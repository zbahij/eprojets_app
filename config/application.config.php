<?php
return array(
    // This should be an array of module namespaces used in the application.
    'modules' => array(    	    	    	   
        'DoctrineModule',
        'DoctrineORMModule',
    	'ZfcBase',
    	'ZfcUser',
    	'ScnSocialAuth',
        'Application',    	
    ),

    // These are various options for the listeners attached to the ModuleManager
    'module_listener_options' => array(        
        'module_paths' => array(
            './module',
            './vendor',
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php',
        ),

        // Whether or not to enable a configuration cache.
        // If enabled, the merged configuration will be cached and used in
        // subsequent requests.
        //'config_cache_enabled' => $booleanValue,
        //'config_cache_key' => $stringKey,
        //'module_map_cache_enabled' => $booleanValue,
        //'module_map_cache_key' => $stringKey,
        //'cache_dir' => $stringPath,
        // 'check_dependencies' => true,
    ),

    // Used to create an own service manager. May contain one or more child arrays.
    //'service_listener_options' => array(
    //     array(
    //         'service_manager' => $stringServiceManagerName,
    //         'config_key'      => $stringConfigKey,
    //         'interface'       => $stringOptionalInterface,
    //         'method'          => $stringRequiredMethodName,
    //     ),
    // )

   // Initial configuration with which to seed the ServiceManager.
   // Should be compatible with Zend\ServiceManager\Config.
   // 'service_manager' => array(),
);