-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 17 Juillet 2013 à 06:34
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `eprojets_app`
--

-- --------------------------------------------------------

--
-- Structure de la table `doc`
--

DROP TABLE IF EXISTS `doc`;
CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `extension` varchar(16) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `user_created_id` int(11) NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `doc`
--

INSERT INTO `doc` (`id`, `name`, `description`, `extension`, `project_id`, `created`, `updated`, `user_created_id`, `task_id`) VALUES
(1, 'video.pptx', 'hahahahaha', 'application/vnd.', 30, '2013-07-17 05:00:09', '2013-07-17 06:22:34', 5, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `jalon`
--

DROP TABLE IF EXISTS `jalon`;
CREATE TABLE IF NOT EXISTS `jalon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `date` datetime NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `responsable` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `jalon`
--

INSERT INTO `jalon` (`id`, `user_id`, `project_id`, `created`, `updated`, `date`, `name`, `description`, `status`, `responsable`) VALUES
(1, 5, 30, '2013-06-09 00:00:00', '2013-06-10 00:00:00', '2013-06-12 04:29:32', 'usma', 'usma', 0, -1),
(2, 5, 30, '2013-07-10 00:00:00', '0000-00-00 00:00:00', '2013-07-17 00:00:00', 'Test - 1', 'For Test', 0, -1),
(3, 5, 30, '2013-07-01 00:00:00', '2013-07-09 00:00:00', '2013-07-12 00:55:46', 'List Anis ==> usma', 'Anis tâches ', 0, -1),
(4, 5, 30, '2013-07-10 00:00:00', '0000-00-00 00:00:00', '2013-07-17 00:00:00', 'Test - 3', 'For Test', 0, -1),
(5, 5, 30, '2013-07-01 00:00:00', '2013-07-09 00:00:00', '2013-07-30 00:00:00', 'Test - 4', 'For Test', 0, -1),
(6, 5, 30, '2013-07-02 02:54:39', NULL, '2013-07-02 02:54:39', 'azerty', 'azerty', 0, -1),
(8, 5, 30, '2013-07-02 03:12:01', NULL, '2013-07-02 16:57:01', 'usma', 'usma', 1, -1),
(9, 5, 30, '2013-07-02 03:27:18', NULL, '2013-07-02 04:31:45', 'test-9', 'test9', 0, -1),
(12, 5, 30, '2013-07-02 03:29:27', NULL, '2013-07-02 04:39:12', 'hamza laterem', 'hamza laterem nnnnnnnn', 0, -1),
(13, 5, 30, '2013-07-02 13:52:19', NULL, '2013-07-02 13:57:32', 'zouhir', 'zouhhh', 0, -1),
(14, 5, 30, '2013-07-02 14:20:21', NULL, '2013-07-02 14:20:21', 'aaaaa', 'aaaaaaaaaaaaa', 0, -1),
(15, 5, 30, '2013-07-02 14:59:37', NULL, '2013-07-02 15:16:58', 'Test-Responsable', '', 1, -1),
(18, 5, 30, '2013-07-02 15:41:12', NULL, '2013-07-02 15:41:12', 'zzz', 'zzz', 0, 24),
(19, 5, 30, '2013-07-02 15:42:00', NULL, '2013-07-02 15:42:00', 'zzz', 'zzz', 0, 24),
(20, 5, 45, '2013-07-03 10:45:14', NULL, '2013-07-03 10:45:14', 'mami', 'mami', 0, 5),
(21, 5, 45, '2013-07-03 10:46:08', NULL, '2013-07-03 10:46:08', 'mami', 'mami', 0, 5),
(22, 5, 45, '2013-07-03 10:46:33', NULL, '2013-07-03 10:46:33', 'mami', 'mami', 0, 5),
(24, 5, 45, '2013-07-03 13:56:54', NULL, '2013-07-03 14:24:01', 'test - jalons -', 'timeaa', 0, -1);

-- --------------------------------------------------------

--
-- Structure de la table `listtasks`
--

DROP TABLE IF EXISTS `listtasks`;
CREATE TABLE IF NOT EXISTS `listtasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created` date NOT NULL,
  `updated` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `progression` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `jalon_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_created_id` int(11) NOT NULL,
  `user_updated_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `listtasks`
--

INSERT INTO `listtasks` (`id`, `name`, `description`, `created`, `updated`, `start_date`, `end_date`, `charge`, `progression`, `status`, `jalon_id`, `project_id`, `user_created_id`, `user_updated_id`) VALUES
(11, 'The first list **', '', '2013-07-15', '2013-07-17', NULL, NULL, NULL, NULL, 0, 1, 30, 5, 5);

-- --------------------------------------------------------

--
-- Structure de la table `pensebetes`
--

DROP TABLE IF EXISTS `pensebetes`;
CREATE TABLE IF NOT EXISTS `pensebetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `test` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Contenu de la table `pensebetes`
--

INSERT INTO `pensebetes` (`id`, `note`, `created`, `user_id`, `test`) VALUES
(53, NULL, '2013-07-02 16:49:23', 5, ''),
(50, 'zaeaz', '2013-05-22 01:42:50', 6, ''),
(49, '					hamz			', '2013-05-22 01:42:38', 6, ''),
(48, NULL, '2013-05-22 01:34:30', 6, ''),
(47, 'qqqqqqqqq', '2013-05-22 01:25:10', 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `project`
--

INSERT INTO `project` (`id`, `name`, `description`, `created`, `updated`) VALUES
(1, 'project 1', 'desc', '2013-05-09 00:00:00', '0000-00-00 00:00:00'),
(2, 'project n° 2', 'desc', '2013-05-29 00:00:00', '0000-00-00 00:00:00'),
(10, 'project n° 10', 'desc', '2013-05-09 00:00:00', '0000-00-00 00:00:00'),
(20, 'project n° 20', 'desc', '2013-05-29 00:00:00', '0000-00-00 00:00:00'),
(21, 'Project 2', 'description projet 2', '2013-05-22 04:27:42', '2013-05-22 04:27:42'),
(22, 'project 3', 'project 3 description', '2013-05-22 14:48:17', '2013-05-22 14:48:17'),
(23, 'hamza', 'hamza', '2013-05-22 14:52:29', '2013-05-22 14:52:29'),
(24, 'azerty', 'azerty', '2013-05-22 14:52:47', '2013-05-22 14:52:47'),
(25, 'aze', 'eee', '2013-05-22 14:53:03', '2013-05-22 14:53:03'),
(26, 'aaaa', 'aaaa', '2013-05-23 15:44:30', '2013-05-23 15:44:30'),
(27, 'ssss', 'sss', '2013-05-23 15:45:14', '2013-05-23 15:45:14'),
(28, 'usma', 'usma', '2013-05-23 15:46:28', '2013-05-23 15:46:28'),
(29, 'usma', 'usma', '2013-05-23 16:42:34', '2013-05-23 16:42:34'),
(30, 'azerty', 'azerty', '2013-05-28 11:04:09', '2013-05-28 11:04:09'),
(31, 'hhhh', 'ffff', '2013-06-10 00:35:45', '2013-06-10 00:35:45'),
(32, 'project_1', 'aaaaaaaaaaaaaa', '2013-06-10 15:57:18', '2013-06-10 15:57:18'),
(33, 'project_1', 'aaaaaaaaaaaaaa', '2013-06-10 15:59:11', '2013-06-10 15:59:11'),
(34, 'Project_1222', 'aaaaa', '2013-06-10 16:02:00', '2013-06-10 16:02:00'),
(35, 'usma', 'usma', '2013-06-10 16:03:14', '2013-06-10 16:03:14'),
(36, 'aa', 'aa', '2013-06-11 16:03:30', '2013-06-11 16:03:30'),
(37, 'Test - TimeLine', 'Test - TimeLine', '2013-07-02 17:33:29', '2013-07-02 17:33:29'),
(38, 'Test - TimeLine - 2', 'Test - TimeLine - 2', '2013-07-02 18:14:02', '2013-07-02 18:14:02'),
(39, 'Test - TimeLine - 3', 'Test - TimeLine - 3', '2013-07-02 18:16:55', '2013-07-02 18:16:55'),
(40, 'Test - TimeLine - 4', 'Test - TimeLine - 4', '2013-07-02 18:18:20', '2013-07-02 18:18:20'),
(41, 'Test - TimeLine - 5', 'Test - TimeLine - 5', '2013-07-02 20:05:42', '2013-07-02 20:05:42'),
(42, 'azeeeee', 'aaa', '2013-07-02 20:08:20', '2013-07-02 20:08:20'),
(43, 'dddddddddd', 'dddd', '2013-07-02 20:17:29', '2013-07-02 20:17:29'),
(44, 'dddddddddd', 'dddd', '2013-07-02 20:17:48', '2013-07-02 20:17:48'),
(45, 'zzzz', 'zzz', '2013-07-02 20:18:48', '2013-07-02 20:18:48');

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

DROP TABLE IF EXISTS `task`;
CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_created_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `listtasks_id` int(11) NOT NULL,
  `user_updated_id` int(11) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `progression` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `rappel` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `user_created_id`, `created`, `updated`, `name`, `description`, `start_date`, `end_date`, `listtasks_id`, `user_updated_id`, `charge`, `progression`, `status`, `rappel`) VALUES
(12, 5, '2013-07-15 04:53:07', NULL, 'The first tache', 'usma milano', '2013-07-16 01:00:00', '2013-07-18 01:00:00', 11, 5, NULL, 0, 0, 0),
(13, 5, '2013-07-15 23:16:20', NULL, 'ddddddddddddddd', '', '2013-07-15 23:16:00', NULL, 11, 5, NULL, 0, 0, 0),
(14, 5, '2013-07-15 23:18:46', NULL, 'khra', '', '2013-07-15 23:18:46', NULL, 11, NULL, NULL, 0, 0, 0),
(15, 5, '2013-07-15 23:24:33', NULL, 'aaaaaaaaaaa', '', '2013-07-15 23:24:33', NULL, 11, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `timeline`
--

DROP TABLE IF EXISTS `timeline`;
CREATE TABLE IF NOT EXISTS `timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `project_id` int(11) NOT NULL,
  `authors` text NOT NULL,
  `activite` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `timeline`
--

INSERT INTO `timeline` (`id`, `description`, `created`, `project_id`, `authors`, `activite`) VALUES
(9, 'Nouveau projet cr&eacute;&eacute; <strong>Test - TimeLine - 2</strong>', '2013-07-02 00:00:00', 38, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(10, 'Nouveau projet cr&eacute;&eacute; <strong></strong>', '2013-07-02 00:00:00', 39, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(11, 'Nouveau projet cr&eacute;&eacute; <strong>Test - TimeLine - 4</strong>', '2013-07-02 00:00:00', 40, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(12, '<strong>Zouhir BAHIJ</strong> a rejoint le projet.', '2013-07-02 00:00:00', 40, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(13, 'Nouveau projet cr&eacute;&eacute; <strong>Test - TimeLine - 5</strong>', '2013-06-30 23:36:00', 41, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(14, '<strong>Zouhir BAHIJ</strong> a rejoint le projet.', '2013-07-01 00:00:00', 41, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(15, 'Nouveau projet cr&eacute;&eacute; <strong>azeeeee</strong>', '2013-07-02 00:00:00', 42, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(16, '<strong>aaaaaaaa</strong> a rejoint le projet.', '2013-07-02 00:00:00', 42, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(17, 'Nouveau projet cr&eacute;&eacute; <strong>dddddddddd</strong>', '2013-07-02 00:00:00', 44, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(18, 'Nouveau projet cr&eacute;&eacute; <strong>zzzz</strong>', '2013-07-02 20:18:48', 45, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(19, '<strong>tttt</strong> a rejoint le projet.', '2013-07-02 20:23:18', 41, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(20, '<strong>111111111</strong> a rejoint le projet.', '2013-07-02 20:24:07', 41, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(21, '<strong>111</strong> a rejoint le projet', '2013-07-02 23:01:38', 41, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'user'),
(22, 'Nouveau jalon cr&eacute;&eacute; <strong>mami</strong>\r\n    				[Responsable: <strong>hamza</strong>]', '2013-07-03 10:47:08', 45, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'project'),
(23, 'Nouveau jalon cr&eacute;&eacute; <strong>test - jalons</strong>\r\n    				[Responsable: <strong>hamza</strong>]', '2013-07-03 13:56:55', 45, 'Cr&eacute;&eacute; par <strong>hamza</strong>', 'jalon'),
(24, 'Jalon supprim&eacute; <strong>mami</strong>', '2013-07-03 14:03:49', 45, 'Supprim&eacute; par <strong>hamza</strong>', 'jalon'),
(25, 'Jalon modifier <strong>test - jalons</strong>\r\n    				[Responsable: <strong>Aucun</strong>]', '2013-07-03 14:20:51', 45, 'Mise ? jour par <strong>hamza</strong>', 'jalon'),
(26, 'Jalon modifier <strong>test - jalons</strong>\r\n    				[Responsable: <strong>Aucun</strong>]', '2013-07-03 14:22:07', 45, 'Mise ? jour par <strong>hamza</strong>', 'jalon'),
(27, 'Jalon modifier <strong>test - jalons -</strong>\r\n    				[Responsable: <strong>Aucun</strong>]', '2013-07-03 14:24:01', 45, 'Mise à jour par <strong>hamza</strong>', 'jalon');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `logged` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `display_name`, `state`, `created`, `updated`, `logged`) VALUES
(1, NULL, 'kimokose@hotmail.com', 'facebookToLocalUser', 'Karim Benabdejlil', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, NULL, 'ato__mix@hotmail.com', 'facebookToLocalUser', 'Vanessa Jones', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, NULL, 'zouhir@bahij.org', 'facebookToLocalUser', 'Zouhir Bahij', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, NULL, 'toma1024@gmail.com', 'facebookToLocalUser', 'Toma Corp', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'latere_h', 'lateremhamza@gmail.com', '$2y$08$3oV1J.RdeRReSURSQUQTleu9Jrzxu0yq98HS9jblVnXcxHyfAiRpW', 'hamza', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, NULL, 'banlieu35@hotmail.com', 'facebookToLocalUser', 'Hamza LaSmoum', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(17, 'latere_h', 'latemhamza@gmail.com', '$2y$14$aFdX60cvoD4fS8h9FDO6WeXp05KFhHR/pjtjHiQoUXRvfubLxld0a', 'Hamza Laterem', NULL, '2013-05-28 11:29:15', '2013-05-28 11:29:15', 0),
(19, 'Bahij', 'omar.bahij@epita.fr', '123456', 'Omar Bahij', NULL, '2013-05-29 00:23:40', '2013-05-29 00:23:40', 0),
(20, 'Bahij', 'omar.bahij@epita.fr', '123456', 'Omar Bahij', NULL, '2013-05-29 00:24:15', '2013-05-29 00:24:15', 0),
(21, 'kamel', 'kamel@laterem', '123456', 'kamel laterem', NULL, '2013-05-30 00:43:33', '2013-05-30 00:43:33', 0),
(22, 'aaa', 'aaaa', 'aaaa', 'aaaa', NULL, '2013-06-09 00:51:08', '2013-06-09 00:51:08', 0),
(23, 'azert', 'azert', 'azert', 'azzzz', NULL, '2013-06-09 22:01:57', '2013-06-09 22:01:57', 0),
(24, 'zouhir', 'zouhir@bahij.org', '123456', 'Zouhir Bahij', NULL, '2013-06-09 22:09:46', '2013-06-09 22:09:46', 0),
(25, 'zouhir', 'zouhir@bahij.org', '123456', 'Zouhir Bahij', NULL, '2013-06-09 22:09:52', '2013-06-09 22:09:52', 0),
(26, 'aaa', 'aaa', 'aaaa', 'aaaa', NULL, '2013-06-09 23:35:11', '2013-06-09 23:35:11', 0),
(27, 'aaaaaaaaaaaa', 'aaaaaaaaaaaaaa', 'a', 'aaa', NULL, '2013-06-09 23:51:48', '2013-06-09 23:51:48', 0),
(28, 'zzzzzzzzzzzzz', 'eeeeeeeeeeee', 'rrrrrrrrrrrrr', 'tttttttttttttt', NULL, '2013-06-09 23:52:09', '2013-06-09 23:52:09', 0),
(29, 'zzzzzzzzzzzzz', 'eeeeeeeeeeee', 'rrrrrrrrrrrrr', 'tttttttttttttt', NULL, '2013-06-09 23:52:15', '2013-06-09 23:52:15', 0),
(30, '11111', '11111111', '1111111111', '11111111', NULL, '2013-06-10 00:02:49', '2013-06-10 00:02:49', 0),
(42, 'aaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaa', NULL, '2013-06-10 00:20:45', '2013-06-10 00:20:45', 0),
(43, 'aaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaa', 'aaaaaaaa', NULL, '2013-06-10 00:20:48', '2013-06-10 00:20:48', 0),
(44, 'aaaaaaaaaaaa', 'aaaa', 'aaa', 'aaa', NULL, '2013-06-11 16:53:35', '2013-06-11 16:53:35', 0),
(45, '', '', '', '', NULL, '2013-07-02 02:31:04', '2013-07-02 02:31:04', 0),
(46, 'Bahij', 'zouhir@bahij.org', '123456', 'Zouhir BAHIJ', NULL, '2013-07-02 18:27:47', '2013-07-02 18:27:47', 0),
(47, 'Zouhir', 'zouhir@bahij.org', '123456', 'Zouhir BAHIJ', NULL, '2013-07-02 20:07:16', '2013-07-02 20:07:16', 0),
(48, 'aaaaa', 'aaaaaa@aaa.com', 'aaaaaa', 'aaaaaaaa', NULL, '2013-07-02 20:08:53', '2013-07-02 20:08:53', 0),
(49, 'tttttt', 'tttt', 'ttttt', 'tttt', NULL, '2013-07-02 20:23:18', '2013-07-02 20:23:18', 0),
(50, '11111111111', '11111111111', '1111111111111', '111111111', NULL, '2013-07-02 20:24:05', '2013-07-02 20:24:05', 0),
(51, '111111111111', '111111111111111', '11111', '111', NULL, '2013-07-02 23:01:38', '2013-07-02 23:01:38', 0);

-- --------------------------------------------------------

--
-- Structure de la table `users_projects_linker`
--

DROP TABLE IF EXISTS `users_projects_linker`;
CREATE TABLE IF NOT EXISTS `users_projects_linker` (
  `project_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `User_user_id` int(11) NOT NULL,
  PRIMARY KEY (`User_user_id`,`project_id`),
  KEY `IDX_19AD33D8E7521667` (`User_user_id`),
  KEY `IDX_19AD33D8166D1F9C` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users_projects_linker`
--

INSERT INTO `users_projects_linker` (`project_id`, `created`, `updated`, `User_user_id`) VALUES
(30, '2013-05-28 11:04:09', '2013-05-28 11:04:09', 5),
(32, '2013-06-10 15:57:18', '2013-06-10 15:57:18', 5),
(33, '2013-06-10 15:59:11', '2013-06-10 15:59:11', 5),
(34, '2013-06-10 16:02:00', '2013-06-10 16:02:00', 5),
(35, '2013-06-10 16:03:14', '2013-06-10 16:03:14', 5),
(37, '2013-07-02 17:33:29', '2013-07-02 17:33:29', 5),
(38, '2013-07-02 18:14:02', '2013-07-02 18:14:02', 5),
(39, '2013-07-02 18:16:55', '2013-07-02 18:16:55', 5),
(40, '2013-07-02 18:18:21', '2013-07-02 18:18:21', 5),
(41, '2013-07-02 20:05:42', '2013-07-02 20:05:42', 5),
(42, '2013-07-02 20:08:20', '2013-07-02 20:08:20', 5),
(43, '2013-07-02 20:17:29', '2013-07-02 20:17:29', 5),
(44, '2013-07-02 20:17:48', '2013-07-02 20:17:48', 5),
(1, '2013-05-08 00:00:00', '2013-05-21 00:00:00', 6),
(21, '2013-05-22 04:27:42', '2013-05-22 04:27:42', 6),
(22, '2013-05-22 14:48:17', '2013-05-22 14:48:17', 6),
(23, '2013-05-22 14:52:29', '2013-05-22 14:52:29', 6),
(24, '2013-05-22 14:52:47', '2013-05-22 14:52:47', 6),
(25, '2013-05-22 14:53:03', '2013-05-22 14:53:03', 6),
(26, '2013-05-23 15:44:30', '2013-05-23 15:44:30', 6),
(27, '2013-05-23 15:45:14', '2013-05-23 15:45:14', 6),
(28, '2013-05-23 15:46:28', '2013-05-23 15:46:28', 6),
(29, '2013-05-23 16:42:34', '2013-05-23 16:42:34', 6),
(30, '2013-06-09 22:09:46', '2013-06-09 22:09:46', 24),
(40, '2013-07-02 18:27:48', '2013-07-02 18:27:48', 46),
(41, '2013-07-02 20:07:16', '2013-07-02 20:07:16', 47),
(42, '2013-07-02 20:08:54', '2013-07-02 20:08:54', 48),
(41, '2013-07-02 20:23:18', '2013-07-02 20:23:18', 49);

-- --------------------------------------------------------

--
-- Structure de la table `users_task_linker`
--

DROP TABLE IF EXISTS `users_task_linker`;
CREATE TABLE IF NOT EXISTS `users_task_linker` (
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_project`
--

DROP TABLE IF EXISTS `user_project`;
CREATE TABLE IF NOT EXISTS `user_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `user_project`
--

INSERT INTO `user_project` (`id`, `id_user`, `id_project`, `date`) VALUES
(1, 5, 1, '2013-05-02 00:00:00'),
(2, 5, 2, '2013-05-09 00:00:00'),
(5, 1, 2, '2013-05-16 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user_provider`
--

DROP TABLE IF EXISTS `user_provider`;
CREATE TABLE IF NOT EXISTS `user_provider` (
  `user_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_provider`
--

INSERT INTO `user_provider` (`user_id`, `provider_id`, `provider`) VALUES
(1, 618996253, 'facebook'),
(2, 722515398, 'facebook'),
(4, 2147483647, 'facebook'),
(3, 536947578, 'facebook'),
(6, 718931876, 'facebook');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `users_projects_linker`
--
ALTER TABLE `users_projects_linker`
  ADD CONSTRAINT `FK_19AD33D8166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `FK_19AD33D8E7521667` FOREIGN KEY (`User_user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
