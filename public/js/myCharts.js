//chart with points
	
	if ($('canvas#canvasPie').length){
		pieChart();	
	}
	if ($('canvas#canvasBar').length){
		barChart()
	}
	
	if($("#sincos").length)
	{				
		$.post("/app/statistique/getValueForChartPoints", {},
	    function(data){
			if (data.response == false)
				alert("Error server non reconu");
			else{
				var $charge = [], $prog = [], tabName = [];
				for(var k in data.ret) {
					$charge.push([k, data.ret[k].charge]);
					$prog.push([k, data.ret[k].prog]);
					//$tabName[k] = ""+data.ret[k].task_name.toString();
					tabName.push([k, ""+data.ret[k].task_name.toString()]);
					//$tabName.push(data.ret[k].task_name.toString());								
				}
				var plot = $.plot($("#sincos"),
						[{ data: $charge, label: "Charge"}, { data: $prog, label: "Progression" }],
						{   series: {
							   lines: { show: true  },
							   points: { show: true },								   
							},
							grid: { hoverable: true, clickable: true, backgroundColor: { colors: ["#fff", "#eee"] } },
							yaxis: { min: 0, max: 1500 },
							xaxis: {ticks:tabName,},
							colors: ["#539F2E", "#3C67A5"]
						});

				function showTooltip(x, y, contents) {					
					$('<div id="tooltip">' + contents + '</div>').css( {
						position: 'absolute',
						display: 'none',
						top: y + 5,
						left: x + 5,
						border: '1px solid #fdd',
						padding: '2px',
						'background-color': '#dfeffc',
						opacity: 0.80
					}).appendTo("body").fadeIn(200);
				}

				var previousPoint = null;
				$("#sincos").bind("plothover", function (event, pos, item) {
					$("#x").text(pos.x.toFixed(2));
					$("#y").text(pos.y.toFixed(2));
					if (item) {
						if (previousPoint != item.dataIndex) {
							previousPoint = item.dataIndex;
							$("#tooltip").remove();
							var x = tabName[item.dataIndex],
							y = item.datapoint[1].toFixed(2);							
							showTooltip(item.pageX, item.pageY,
										item.series.label + " of " + x[1] + " = " + y);
						}
					}
					else {
						$("#tooltip").remove();
						previousPoint = null;
					}
				});
					
				$("#sincos").bind("plotclick", function (event, pos, item) {					
					if (item) {
						$("#clickdata").text("You clicked task \"" + tabName[item.dataIndex][1] + "\" in " + item.series.label + ".");
						plot.highlight(item.series, item.datapoint);						}
				})
			}
	    }, 'json');				
	}
	
	//T�ches : finies / en-cours / en-retard
	if($("#piechart").length)
	{
		var idProject = $("input.idProject").val();
		$.post("/app/statistique/getData/"+idProject, {},
		    function(data){
				if (data.response == false)
					alert("Error server non reconu");
				else{
					var charge = [], prog = [], tabName = [], prog_tmp = [];
					for(var k in data.prog) {
						charge.push([k, data.charge[k]]);						
						prog.push([k, data.prog[k]]);						
						tabName.push([k, ""+data.taskName[k]]);								
					}

					//pie chart
					var dataTask = [
					{ label: "Tâches finies",  data: data.nbTaskF},
					{ label: "Tâches en cours",  data: data.nbTaskC},
					{ label: "Tâches en retard ",  data: data.nbTaskR}
					];
					if($("#piechart").length){
						$.plot($("#piechart"), dataTask,
						{
							series: {
									pie: {
										show: true
									}
							},
							grid: {
								hoverable: true,
								clickable: true
							},
							legend: {
								show: false
							}
						});
						function pieHover(event, pos, obj)
						{
							if (!obj)
								return;
							percent = parseFloat(obj.series.percent).toFixed(2);
							$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
						}
						$("#piechart").bind("plothover", pieHover);																		
					}
				
					var dataJalon = [
							{ label: "Jalons finies",  data: data.nbJalonF},
							{ label: "Jalons en cours",  data: data.nbJalonC},
							{ label: "Jalons en retard ",  data: data.nbJalonR}
					];
					//donut chart
					if($("#donutchart").length)
					{
						$.plot($("#donutchart"), dataJalon,
						{
								series: {
										pie: {
												innerRadius: 0.5,
												show: true
										}
								},
								legend: {
									show: false
								}
						});
					}
					
					//stack chart
					if($("#stackchart").length)
					{						
						var stack = 0, bars = true, lines = false, steps = false;

						function plotWithOptions() {
							$.plot($("#stackchart"), [ charge, prog ], {
								series: {
									stack: stack,
									lines: { show: lines, fill: true, steps: steps },
									bars: { show: bars, barWidth: 0.5 }
								}
							});
						}

						plotWithOptions();

						$(".stackControls input").click(function (e) {
							e.preventDefault();
							stack = $(this).val() == "With stacking" ? true : null;
							plotWithOptions();
						});
						$(".graphControls input").click(function (e) {
							e.preventDefault();
							bars = $(this).val().indexOf("Bars") != -1;
							lines = $(this).val().indexOf("Lines") != -1;
							steps = $(this).val().indexOf("steps") != -1;
							plotWithOptions();
						});
					}
					
					
				}
				
				// chart				
				var plot = $.plot($("#chargProg"),
						[{ data: charge, label: "Charge"}, { data: prog, label: "Progression" }],
						{   series: {
							   lines: { show: true  },
							   points: { show: true },								   
							},
							grid: { hoverable: true, clickable: true, backgroundColor: { colors: ["#fff", "#eee"] } },
							yaxis: { min: 0, max: 1000 },
							xaxis: {ticks:tabName,},
							colors: ["#539F2E", "#3C67A5"]
						});

				function showTooltip(x, y, contents) {					
					$('<div id="tooltip">' + contents + '</div>').css( {
						position: 'absolute',
						display: 'none',
						top: y + 5,
						left: x + 5,
						border: '1px solid #fdd',
						padding: '2px',
						'background-color': '#dfeffc',
						opacity: 0.80
					}).appendTo("body").fadeIn(200);
				}

				var previousPoint = null;
				$("#chargProg").bind("plothover", function (event, pos, item) {
					$("#x").text(pos.x.toFixed(2));
					$("#y").text(pos.y.toFixed(2));
					if (item) {
						if (previousPoint != item.dataIndex) {
							previousPoint = item.dataIndex;
							$("#tooltip").remove();
							var x = tabName[item.dataIndex],
							y = item.datapoint[1].toFixed(2);							
							showTooltip(item.pageX, item.pageY,
										item.series.label + " of " + x[1] + " = " + y);
						}
					}
					else {
						$("#tooltip").remove();
						previousPoint = null;
					}
				});
					
				$("#chargProg").bind("plotclick", function (event, pos, item) {					
					if (item) {
						$("#clickdata").text("You clicked task \"" + tabName[item.dataIndex][1] + "\" in " + item.series.label + ".");
						plot.highlight(item.series, item.datapoint);						}
				})
			}, 
			'json');		
	}