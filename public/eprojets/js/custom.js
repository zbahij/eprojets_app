var One = true;
var Ine;
var Ide;

  


function Init(){
	$('input#time').val("00:00:00:00");
}

function SetInt(){
	var str = $('input#demArr').val();
	
	if (str == "Démarrer"){
		One = true;
		$('input#demArr').val("Pause");
		
	}
	else{
		One = false;
		$('input#demArr').val("Démarrer");
	}
	if(One){
		Int=setInterval("Chrono()",10);
		Ide=setInterval("SetStop(false)",60000);
		One=false;
	}
	else{
		console.log("Pause");
		clearInterval(Int);
		clearInterval(Ide);
		One=true;
	}
}

function GetEle(s,i){
	var Ele=new Array();
	var s_ = s + ":";
	var j;
	var m = 0;
	var s__ = "";
	for (j=0; j < s_.length; j++){
		if (s_.charAt(j) != ":"){
			s__ = s__ + s_.charAt(j);
		}
		else{
			Ele[m] = s__;
			s__ = "";
			m++;
		}
	}
	return Ele[i];
}

function Trans(i){
	if(i < 10){
		return "0"+i;
	}else{
		return i;
	}	   //setInterval("Chrono()",10)
}

function Ext(s){
	var s_ = parseInt(s.substring(1,s.length));
	if(parseInt(s) < 10){
		return parseInt(s_);
	}else{
		return s;
	}
}

function SetStop(v){
	if (v == true){
		One = false;
		SetInt();
	}
	
	var $elem = $('input#time').val().split(':');
    var $ret = 0;
    var idListTask = $('input#idListTask').val();
    var idTask = $('input#idTask').val();
    var idProject = $('input#idProject').val();
    
    $ret = $ret + (parseInt($elem[0]) * 60) + parseInt($elem[1]);
    
    // envyer $ret sous ajax
    $.post("/app/tasks/progression/"+idListTask+"/"+idTask+"/project/"+idProject, {
    	progress: $ret,
    },
    function(data){
        if(data.response == true)
        	console.log("Good : " + $ret);
        else
        	console.log("error : " + $ret);
    }, 'json');
}

function Chrono(){
	var MSec = parseInt(Ext(GetEle($('input#time').val(), 3)));
    var Sec = parseInt(Ext(GetEle($('input#time').val(), 2)));
	var Min = parseInt(Ext(GetEle($('input#time').val(), 1)));
	var He = parseInt(Ext(GetEle($('input#time').val(), 0)));
	if(MSec < 100){
		MSec++;
	}
	else{
		Sec++;
		MSec = 0;
		if(Sec > 60){
			Min++;
			Sec = 0;
			if(Min > 60){
				He++;
				Min = 0;
			}  
		}
	}
	$('input#time').val(Trans(He)+":"+Trans(Min)+":"+Trans(Sec)+":"+Trans(MSec));	
}
function checkLogger1(){
	var $id = $('tr.enLign-1 td.elemEnLign').attr('id');
	var $currentProject = $('input#currentProject').val();
	
	$.post("/app/users/getLogger/"+$id +"/project/"+$currentProject, {    	
    	id : $id
    },
    function(data){        	
        if(data.response == true){
        	if (data['logged'] == 1){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-success\">En ligne </span>");        		 
        	}   
        	else if (data['logged'] == 0){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-failed\">Hors ligne </span>");
        	}
        }
        else
        	console.log("usma");
    }, 'json');
}

function checkLogger2(){
	var $id = $('tr.enLign-2 td.elemEnLign').attr('id');
	var $currentProject = $('input#currentProject').val();
	
	$.post("/app/users/getLogger/"+$id +"/project/"+$currentProject, {    	
    	id : $id
    },
    function(data){        	
        if(data.response == true){
        	if (data['logged'] == 1){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-success\">En ligne </span>");        		 
        	}   
        	else if (data['logged'] == 0){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-failed\">Hors ligne </span>");
        	}
        }
        else
        	console.log("usma");
    }, 'json');
}

function checkLogger3(){
	var $id = $('tr.enLign-3 td.elemEnLign').attr('id');
	var $currentProject = $('input#currentProject').val();
	
	$.post("/app/users/getLogger/"+$id +"/project/"+$currentProject, {    	
    	id : $id
    },
    function(data){        	
        if(data.response == true){
        	if (data['logged'] == 1){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-success\">En ligne </span>");        		 
        	}   
        	else if (data['logged'] == 0){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-failed\">Hors ligne </span>");
        	}
        }
        else
        	console.log("usma");
    }, 'json');
}

function checkLogger4(){
	var $id = $('tr.enLign-4 td.elemEnLign').attr('id');
	var $currentProject = $('input#currentProject').val();
	
	$.post("/app/users/getLogger/"+$id +"/project/"+$currentProject, {    	
    	id : $id
    },
    function(data){        	
        if(data.response == true){
        	if (data['logged'] == 1){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-success\">En ligne </span>");        		 
        	}   
        	else if (data['logged'] == 0){
        		$('td.elemEnLign#'+$id+' span').remove();        		
        		$('td.elemEnLign#'+$id).prepend("<span class=\"label label-failed\">Hors ligne </span>");
        	}
        }
        else
        	console.log("usma");
    }, 'json');
}

jQuery(function($) {		
	if($('.enLign-1').length)
	{		
		var $id = $('tr.enLign-1 td.elemEnLign').attr('id');
		Ide=setInterval('checkLogger1()',6000);
	}
	if($('.enLign-2').length)
	{
		Ide=setInterval('checkLogger2()',6000);
	}
	if($('.enLign-3').length)
	{
		Ide=setInterval('checkLogger3()',6000);	
	}
	if($('.enLign-4').length)
	{
		Ide=setInterval('checkLogger4()',6000);
	}
	/*
	 * Profil functions
	 */
	
	$('a#updInfo').on('click', function(event){
		event.preventDefault();
		var $u = $(this);
		console.log("updInfoMail");
       // var $updInfoMail = $(this);
        //var $display_name = $('input.display_name').val();
        //var $username = $('input.username').val();
        
        console.log("display_name : " + $display_name);
        console.log("username : " + $username);
        
        /*
        var update_id = $stickynote.attr('id'),
        update_content = $stickynote.val();
        update_id = update_id.replace("stickynote-","");

        $.post("pensebetes/update", {
            id: update_id,
            content: update_content
        },function(data){
            if(data.response == false){
                // print error message
                console.log('could not update');
            }
        }, 'json');*/

    });
	
	
	/*
	 * Fin Profil functions
	 */
	
/*	$('div#addDocForm').on('click', 'button#envoyer', function(event){
        var file = $('#file').val();
        var description = $('#description').val();
        var idProject = $('#idProject').val();
        
        console.log(file);
        if (file == ''){            	
        	$('div#divFile').before('<div id="\alertFile\" class=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button><strong>Attention!</strong>Aucun document n\'est choisi</div>');
        }
        $.post("/app/docs/addAjax/project/"+idProject, {
        	fileupload: file,
        	description : description
        },
        function(data){
            if(data.response == true)
            	alert("-- ok --");
            else
            	alert("-- Ko --");
        }, 'json');
        
        console.log("============>");
    });*/
	
	/*
	 * Start add piece jointe
	 */
	
	$('div#addDocForm').on('click', 'a.add-doc', function(event){
        console.log("hamza");        
        event.preventDefault();
        var $u = $(this);
        var id = $(this).attr('id');        
        var $elem = id.split('|');                
        $('div.addDocs').prepend("<p class=remove-"+$elem[0]+" id=\""+$elem[1]+"\"><a class=\"doc\" id=\""+$elem[0]+"\" href=\"#\">"+$elem[1]+"</a><input type=\"hidden\" name=\"docs[]\" value=\""+$elem[0]+"\"><a id=\"remove-"+$elem[0]+"\" class=\"delete-docTask\" href=\"#\">  x</a><br><br></p>");
        $('div.doc-'+$elem[0]).remove();//+$elem[0]+"|"+$elem[1]).remove();
        console.log(id);
        console.log('a.'+$elem[0]+'|'+$elem[1]);
    });	

	$('div.addDocs').on('click', 'a.delete-docTask', function(event){
        console.log("delete doc tasks");        
        event.preventDefault();
        var $u = $(this);
        var fileName = $u.parent().attr('id');
        var id = $(this).attr('id');
        var $elem = id.split('-'); 
        $('p.'+id).remove();
        $('div#addDocForm').prepend("<div class=\"doc-"+$elem[1]+"\"> <a class=\"add-doc\" id=\""+$elem[1]+"|"+fileName+"\" href=\"#\" data-dismiss=\"modal\">"+fileName+" <\/a><br><\/div>");
        
        console.log(id);    
    });
	/*
	 * Fin add piece jointe
	 */
	
	/*
	 * Start add user in task
	 */
	
	$('div#addUserForm').on('click', 'a.add-user', function(event){        
        event.preventDefault();
        var $u = $(this);
        var id = $(this).attr('id');        
        var $elem = id.split('|');                
        $('div.addUsers').prepend("<div style=\"fload:left;\" class=remove-"+$elem[0]+" id=\""+$elem[1]+"\"><a class=\"user\" id=\""+$elem[0]+"\" href=\"#\">"+$elem[1]+"</a>&nbsp;&nbsp;&nbsp;<input type=\"hidden\" name=\"choixUser[]\" value=\""+$elem[0]+"\"><div class=\"input-append remove-"+$elem[0]+"\" id=\""+$elem[1]+"\"> <input name=\"chargeUsers[]\" value=\"0\" class=\"span2\" id=\"appendedInput\" type=\"text\"> <span class=\"add-on\">:Min</span>&nbsp;<a id=\"remove-"+$elem[0]+"\" class=\"delete-userTask\" href=\"#\">x</a> </div><br><br>");      
        $('div.user-'+$elem[0]).remove();//+$elem[0]+"|"+$elem[1]).remove();
        console.log(id);        
    });	

	$('div.addUsers').on('click', 'a.delete-userTask', function(event){
        console.log("delete doc tasks");        
        event.preventDefault();
        var $u = $(this);
        var fileName = $u.parent().attr('id');
        var id = $(this).attr('id');
        var $elem = id.split('-'); 
        $('div.'+id).remove();
        $('div#addUserForm').prepend("<div class=\"user-"+$elem[1]+"\"> <a class=\"add-user\" id=\""+$elem[1]+"|"+fileName+"\" href=\"#\" data-dismiss=\"modal\">"+fileName+" <\/a><br><\/div>");
        
        console.log(id);    
    });
	/*
	 * Fin add user in task
	 */
	
	$('.listUsersProject').on('click', 'a.delete-user',function(event){
		console.log("Delete");
        event.preventDefault();
        var $u = $(this);
        var id = $(this).attr('id');
        var $elem = id.split('|');
        $.post("/app/users/remove/project", {
        	id_user: $elem[0],
        	id : $elem[1]
        },
        function(data){        	
            if(data.response == true){
            	$('.listUsersProject' + " tr#" + $elem[0]).remove();
            }
            else
            	console.log("usma");
        }, 'json');
    });
	
	
	
    $("#create").on('click', function(event){    	
    	event.preventDefault();
        var $stickynote = $(this);
        console.log($stickynote);
        $.post("pensebetes/add", null,
            function(data){        		
                if(data.response == true){
                    $stickynote.before("<div class=\"sticky-note\"><textarea class=\"pensebete\" id=\"stickynote-"+data.new_note_id+"\"></textarea><a href=\"#\" id=\"remove-"+data.new_note_id+"\"class=\"delete-sticky\">X</a></div>");
                    console.log("user_id : "+data.user_id);
                // print success message
                } else {
                    // print error message
                    console.log('could not add');
                }
            }, 'json');
    });

    $('#sticky-notes').on('click', 'a.delete-sticky',function(event){
        event.preventDefault();
        var $stickynote = $(this);
        var remove_id = $(this).attr('id');
        remove_id = remove_id.replace("remove-","");

        $.post("/pensebetes/remove", {
            id: remove_id
        },
        function(data){
            if(data.response == true){
            	console.log('could not update')
                $stickynote.parent().remove();
        }
            else{
                // print error message
                console.log('could not remove ');
            }
        }, 'json');
    });

    $('#sticky-notes').on('keyup', 'textarea', function(event){
        var $stickynote = $(this);
        var update_id = $stickynote.attr('id'),
        update_content = $stickynote.val();
        update_id = update_id.replace("stickynote-","");

        $.post("pensebetes/update", {
            id: update_id,
            content: update_content
        },function(data){
            if(data.response == false){
                // print error message
                console.log('could not update');
            }
        }, 'json');

    });
});