<?php

namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;


class UsersProjectsLinkerTable extends AbstractTableGateway {
	protected $table = 'users_projects_linker';
	
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	public function getUsersProjectsLinker($id_project, $id_user) {
		$row = $this->select(array(
				'project_id' => (int) $id_project,
				'User_user_id' => (int) $id_user,
		))->current();
		if (!$row)
			return false;
	
		$usersProjectsLinker = new UsersProjectsLinker();
		$usersProjectsLinker->exchangeArray($row);
		
		return $usersProjectsLinker;
	}
	
	public function removeUsersProjectsLinker($id_project, $id_user) {
		return $this->delete(array(
				'project_id' => (int) $id_project,
				'User_user_id' => (int) $id_user,				
		));
	}
}