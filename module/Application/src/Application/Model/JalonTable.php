<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;

/**
 *
 * @author lasmoum
 *        
 */
class JalonTable extends AbstractTableGateway {
	
	protected $table = 'jalon';
	
	/**
	 */
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}

	public function fetchAllActiveByUser($user_id, $em)
	{
		$resultSet = $this->select(function (Select $select) use ($user_id) {
			$select->where->equalTo('user_id', $user_id);
			$select->where->equalTo('status', 1);					
		});
		
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\Jalon();
			$entity->exchangeArray($row);
			$project = $em->find('Application\Entity\Project', $row->project_id);
			$user = $em->find('Application\Entity\User', $row->user_id);
			$entity->setUser($user);
			$entity->setProject($project);
			if ($entity->getReponsable() != -1)
				$entity->setResponsableUser($em->find('Application\Entity\User', $entity->getReponsable()));
			$entities[] = $entity;
		}
		return $entities;
	}
	public function fetchAll($project_id = null, $em) {
		 
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\Jalon();
			$entity->exchangeArray($row);			 
			$user = $em->find('Application\Entity\User', $row->user_id);
			$entity->setUser($user);
			if ($entity->getReponsable() != -1)	
				$entity->setResponsableUser($em->find('Application\Entity\User', $entity->getReponsable()));						
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function countJalonOfProject($project_id){
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);			
		});
		
		return $resultSet->count();
	}
	
	public function getJalon($id, $em) {
		$row = $this->select(array('id' => (int) $id))->current();
		if (!$row)
			return false;
		$jalon = new Entity\Jalon();
		$jalon->exchangeArray($row);		
		$jalon->setUser($em->find('Application\Entity\User', $row->user_id));
		if ($jalon->getReponsable() != -1)
			$jalon->setResponsableUser($em->find('Application\Entity\User', $jalon->getReponsable()));
		
		return $jalon;
	}
	
	public function saveJalon(Entity\Jalon $jalon, $em) {		
		$data = $jalon->getArray();	
		$id = (int) $jalon->getId();
		
		if ($id == 0) {			
			if (!$this->insert($data))
				return false;
			return $this->getLastInsertValue();
		}
		elseif ($this->getJalon($id, $em)) {
			if (!$this->update($data, array('id' => $id)))
				return false;
							
			return true;
		}
		else
			return false;
	}
	
	public function removeJalon($id) {
		return $this->delete(array('id' => (int) $id));
	}	
}