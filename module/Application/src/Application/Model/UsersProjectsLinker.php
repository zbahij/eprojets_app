<?php

namespace Application\Model;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UsersProjectsLinker implements InputFilterAwareInterface{
	public $user_id;
	public $project_id;
	public $created;
	public $updated;
	
	protected $inputFilter;
	
	/**
	 * Used by ResultSet to pass each database row to the entity
	 */
	public function exchangeArray($data)
	{
		$this->user_id     = (isset($data['User_user_id'])) ? $data['User_user_id'] : null;
		$this->project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
		$this->created  = (isset($data['created'])) ? $data['created'] : null;
		$this->updated  = (isset($data['updated'])) ? $data['updated'] : null;
	}
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$factory = new InputFactory();
			$inputFilter->add($factory->createInput(array(
					'name'     => 'user_id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));
	
			$inputFilter->add($factory->createInput(array(
					'name'     => 'project_id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));
	
			$inputFilter->add($factory->createInput(array(
					'name'     => 'created',
					'required' => true,
					'filters'  => array(
							array('name' => 'Date'),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'updated',
					'required' => true,
					'filters'  => array(
							array('name' => 'Date'),
					),
			)));
	
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}
}