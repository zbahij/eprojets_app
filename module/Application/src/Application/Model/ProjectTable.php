<?php

namespace MyApplication\Model\app;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class ProjectTable extends AbstractTableGateway
{
    protected $table = 'project';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Project());

        $this->initialize();
    }
	
	public function getProjectArray($user_project)
	{
		$return = array();
		foreach ($user_project as $up){
			//print_r('aaa'  . $up->id_project);			
			$return[] = $this->getProject((int) $up->id);
		}
		return $return;
	}

    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet;
    }

    public function getProject($id)
    {
        $id  = (int) $id;
        $rowset = $this->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveProject(Project $project)
    {
        $data = array(
            'name' => $project->name,
            'description'  => $project->description,
        );

        $id = (int)$project->id;
        if ($id == 0) {
            $this->insert($data);
        } else {
            if ($this->getProject($id)) {
                $this->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteProject($id)
    {
        $this->delete(array('id' => $id));
    }
}
