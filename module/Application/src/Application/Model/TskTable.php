<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Zend\Db\Sql\Delete;
use Application\Entity\ListTasks;
use Zend\Form\Element\Date;

class TskTable extends AbstractTableGateway {

    protected $table = 'task';
    protected $_usersTaskLinkerTable;
    protected $_listTasksTable;

    public function __construct(Adapter $adapter, UsersTaskLinkerTable $usersTaskLinkerTable,
    		ListTasksTable $listTasksTable) {    	
    	$this->adapter = $adapter;
    	$this->_usersTaskLinkerTable = $usersTaskLinkerTable;
    	
    	$this->_listTasksTable = $listTasksTable;
    }
    
    public function fetchAllActiveByUser($user_id, $em)
    {    	
    	$entities = array();
    	$entities = $this->_usersTaskLinkerTable->getTasksByUser($user_id);
    	
    	$tasks = array();
    	foreach ($entities as $ent){
    		$task = new Entity\Task();
    		$task = $this->getCritiqTask($ent->getTaskId());
    		if ($task != false && $task->getStatus() == 0){
    			$date = new date("Y-m-d H:i:s");
    			// verifier la date pour les date critique **************************************     			   			    			    			    			    			
    			$tasks[] = $task;
    		}
    	}
    	
    	return $tasks;    	    	
    }
    
    public function getProject($project_id, $em)
    {    	
    	return ($em->find('Application\Entity\Project', $project_id));
    	
    }
    
    public function getListTaskById($listTask_id)
    {
    	return $this->_listTasksTable->getListTaskForUpdate($listTask_id);
    }
    
    public function fetchAllByUser($user_id)
    {
    	$entities = array();
    	// on recupere toute les usertasklinker d'un user_id
    	$entities = $this->_usersTaskLinkerTable->getTasksByUser($user_id);
    	// on parcour le tableau
    	$tasks = array();
    	foreach ($entities as $ent){
    		$task = new Entity\Task();
			$task = $this->getTask($ent->getTaskId());
			$tasks[] = $task;    		    		
    	}
    	return $tasks;
    	
    }
    public function fetchAll($listtasks_id = null, $em) {    	
        $resultSet = $this->select(function (Select $select) use ($listtasks_id) {
        			$select->where->equalTo('listtasks_id', $listtasks_id);
                    $select->order('created ASC');
                });      
        $entities = array();
        foreach ($resultSet as $row) {
            $entity = new Entity\Task();
            $entity->exchangeArray($row);
            $user = $em->find('Application\Entity\User', $row->user_created_id	);            
            $entity->setUserCreated($user);            
            $entities[] = $entity;
        }
        return $entities;
    }
    
    public function getAllWithIdList($listtasks_id = null) {
    	$resultSet = $this->select(function (Select $select) use ($listtasks_id) {
    		$select->where->equalTo('listtasks_id', $listtasks_id);
    	});
    	$entities = array();
    	foreach ($resultSet as $row) {    		    		
    		$entities[] = $row->id;
    	}
    	return $entities;
    }
    
    public function removeAllTasksWithList($id_listTask){
    	foreach ($this->getAllWithIdList($id_listTask) as $idT){
    		$this->_usersTaskLinkerTable->removeByIdTask($idT);
    	}
    	$resultSet = $this->delete(function (Delete $delete) use ($id_listTask) {
    		$delete->where->equalTo('listtasks_id', $id_listTask);
    	});
    	return $resultSet;    	
    }
    
    public function removeTask($id_task){
    	$this->_usersTaskLinkerTable->removeByIdTask($id_task);    	
    	return $this->delete(array('id' => (int) $id_task));
    }
    
    public function saveTask(Entity\Task $task, $userTable){
    	$data = $task->getArray();
    	$id = (int) $task->getId();
    	// add task
    	if ($id == 0) {
    		if (!$this->insert($data))
    			return false;
    		$id_task = $this->getLastInsertValue();
    		// add les users de la task 
    		foreach ($task->getUserTable() as $u){
    			$this->_usersTaskLinkerTable->saveParams($id_task, $u);
    		}
    		return $id_task;
    	}
    	// update task
    	else{    		
    		// update les users de la task
    		// Si on enleve les users : on supprime les users de tasks de la table userstasklinker     		
    		if ($userTable == null){    			
    			if ($task->getUserTable() != null){    				
    				$this->_usersTaskLinkerTable->removeByIdTask($id);// remove Les user;
    			}
    		}
    		else
    		 {
    		 	
    			// Si la task n'avais pas des users : On ajout nouveau users dans la table userstasklinker
    			if ($task->getUserTable() == null){    				
    				foreach ($userTable as $u){
    					$this->_usersTaskLinkerTable->saveParams($id, $u);
    				}
    			}
    			// Si on a enlever quelque users ou on a ajouter : alors on update la table userstasklinker
    			else{    				
    				// recuperation d'un tableau pour les nouveau user ki n'existe pas dans la task
    				// l'ajout des users qui n'existe pas dans la task � updater
    				foreach ($userTable as $u){
    					if (!$task->verifUser($u)){    					
    						$this->_usersTaskLinkerTable->saveParams($id, $u);
    					}
    				}    				
    				// suppression des users qui existe dans la task et pas dans dans la son update
    				$verif = false;    			
    				foreach ($task->getUserTable() as $u){
    					foreach ($userTable as $tu){
    						if ($u == $tu){
    							$verif = true;
    						}
    					}
    					if (!$verif){
    						$this->_usersTaskLinkerTable->removeParams($id, $u);
    					}
    					else{
    						$verif = false;
    					}
    				}
    			}
    		}
    		    		
    		if (!$this->update($data, array('id' => $id)))
    			return false;
    		
    		return true;
    	}
    }        

    public function getTask($id) {
        $row = $this->select(array('id' => (int) $id))->current();
    	
    	if (!$row)
    		return false;
    	$task = new Entity\Task();
    	$task->exchangeArray($row);
    	$task->setUserTable($this->_usersTaskLinkerTable->getUsersByTaskTable($id));
    	
    	return $task;
    }    

    public function getCritiqTask($id) {    	    	
    	$row = $this->select(function  (Select $select) use ($id){    		
    		$select->where->equalTo('status', 0);
    		$select->where->equalTo('id', $id);
    	})->current();    	
    	if (!$row)
    		return false;
    	$task = new Entity\Task();
    	$task->exchangeArray($row);
    	return $task;
    }
}