<?php

namespace MyApplication\Model\app;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class UserProjectTable extends AbstractTableGateway
{
    protected $table = 'user_project';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new UserProject());

        $this->initialize();
    }
	
	public function getUserProject(){
	}

    public function fetchAll()
    {
        $resultSet = $this->select();
        return $resultSet;
    }
	public function getProjectId($id)
    {
        $id  = (int) $id;
        $resultSet = $this->select(array('id_user' => $id));
 
        return $resultSet;
	}

}
