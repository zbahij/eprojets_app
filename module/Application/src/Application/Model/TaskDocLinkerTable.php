<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Zend\Db\Sql\Delete;
/**
 *
 * @author lasmoum
 *        
 */
class TaskDocLinkerTable  extends AbstractTableGateway
{
	protected $table = 'task_doc_linker';
	
	/**
	 */
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	public function removeAllDocWithListOfTask(array $task = null){
		foreach ($task as $t){
			$this->removeAllByTask($t);
		}
		return true;
	}
	
	public function fetchAllByTask($task_id = null) {			
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->where->equalTo('task_id', $task_id);
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\TaskDocLinker();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function fetchAllByDoc($doc_id = null) {
		$resultSet = $this->select(function (Select $select) use ($doc_id) {
			$select->where->equalTo('doc_id', $doc_id);
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\TaskDocLinker();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function saveAllByTask($task_id = null, array $doc_id){		
		if ($doc_id != null){
			foreach ($doc_id as $dc) {			
				$data['doc_id'] = $dc;
				$data['task_id'] = $task_id;
				$this->insert($data);				
			}
			return true;
		}
		return false;
	}
	public function saveDocByTask($task_id = null, $doc_id = null){
		if ($doc_id != null && $task_id != null){			
			$data['doc_id'] = $doc_id;
			$data['task_id'] = $task_id;
			$this->insert($data);	
			return true;
		}
		return false;
	}
	
	private function verifDoc($taskDocLinker, $id_doc){
		
		foreach ($taskDocLinker as $tdl){				
			if ($tdl->getIdDoc() == $id_doc){				
				return true;
			}
		}		
		return false;
	}
	public function updateAllByTask($task_id = null, array $doc_id = null){
		// update les docs de la task
		// Si on enleve les docs : on supprime les docs de tasks de la table taskDoclinker
		if ($doc_id == null){
			$this->removeAllByTask($task_id);
			return true;
		}
		else
		{
		
			// Si la task n'avais pas des docs : On ajout nouveau doc dans la table taskDoclinker
			$taskDocLinker = $this->fetchAllByTask($task_id);
			if ($taskDocLinker == null){
				$this->saveAllByTask($task_id, $doc_id);
				return true;
			}
			// Si on a enlever quelque doc ou on a ajouter : alors on update la table taskdoclinker
			else{
				// recuperation d'un tableau pour les nouveau doc ki n'existe pas dans la task
				// l'ajout des doc qui n'existe pas dans la task � updater
				foreach ($doc_id as $d){					
					if (!$this->verifDoc($taskDocLinker, $d)){
						$this->saveDocByTask($task_id, $d);
					}
				}
				// suppression des docs qui existe dans la task et pas dans l'update
				$verif = false;
				foreach ($taskDocLinker as $u){
					foreach ($doc_id as $tu){
						if ($u->getIdDoc() == $tu){
							$verif = true;
						}
					}
					if (!$verif){
						$this->removeDocTask($task_id, $u->getIdDoc());
					}
					else{
						$verif = false;
					}
				}
			}
		}						
		return true;		
	}
	
	public function removeByTaskAndDocs($task_id = null, array $doc_id){
		if ($doc_id != null){
			foreach ($doc_id as $dc) {
				$data['doc_id'] = $dc;
				$data['task_id'] = $task_id;
				$this->delete($data);
			}
		}
		return true;
	}
	
	public function removeAllByTask($task_id = null){
		if ($task_id != null){			
				return $this->delete(array('task_id' => (int) $task_id));
			}		
		return false;
	}
	
	public function removeDocTask($task_id = null, $doc_id = null){
		if ($task_id != null && $doc_id != null){
			return $this->delete(array('task_id' => (int) $task_id, 'doc_id' => (int) $doc_id,));
		}
		return false;
	}
}