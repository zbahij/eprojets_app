<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Zend\Db\Sql\Delete;
use Application\Entity\ListTasks;
use DoctrineORMModuleTest\Assets\Entity\Date;

class TaskTable extends AbstractTableGateway {

    protected $table = 'task';
    protected $_usersTaskLinkerTable;

    public function __construct(Adapter $adapter, UsersTaskLinkerTable $usersTaskLinkerTable) {    	
    	$this->adapter = $adapter;
    	$this->_usersTaskLinkerTable = $usersTaskLinkerTable;    	    	
    }
    
    public function setDateStartEnd($task_id = null, $date_start = null, $date_end = null){
    	if($task_id == null || $date_end == null)
    		return false;
    	if ($date_start != null)    	
    		$data['start_date'] = $date_start;
    	$data['end_date'] = $date_end;
    	$data['updated'] = date("Y-m-d H:i:s");
    	if (!$this->update($data, array('id' => $task_id)))
    		return false;
    	return true;
    }
    public function fetchAllActiveByUser($user_id, $em)
    {
    	$entities = array();
    	$entities = $this->_usersTaskLinkerTable->getTasksByUser($user_id);
    	
    	$tasks = array();
    	foreach ($entities as $ent){
    		$task = new Entity\Task();
    		$task = $this->getTask($ent->getTaskId());
    		$task->setCharge($this->_usersTaskLinkerTable->getAllChargeByTask($ent->getTaskId()));
    		$task->setProgression($this->_usersTaskLinkerTable->getAllProgressionByTask($ent->getTaskId()));
    		if ($task->getCritique() == 1)
    			$tasks[] = $task;
    	}
    	return $tasks;    	    	
    }
        
    
    public function fetchAllByUser($user_id)
    {
    	$entities = array();
    	// on recupere toute les usertasklinker d'un user_id
    	$entities = $this->_usersTaskLinkerTable->getTasksByUser($user_id);
    	// on parcour le tableau
    	$tasks = array();
    	foreach ($entities as $ent){
    		$task = new Entity\Task();
			$task = $this->getTask($ent->getTaskId());
			$task->setCharge($this->_usersTaskLinkerTable->getAllChargeByTask($ent->getTaskId()));
			$task->setProgression($this->_usersTaskLinkerTable->getAllProgressionByTask($ent->getTaskId()));
			$tasks[] = $task;    		    		
    	}
    	return $tasks;
    	
    }
    public function fetchAll($listtasks_id = null, $em) {    	
        $resultSet = $this->select(function (Select $select) use ($listtasks_id) {
        			$select->where->equalTo('listtasks_id', $listtasks_id);
                    $select->order('created ASC');
                });      
        $entities = array();
        foreach ($resultSet as $row) {
            $entity = new Entity\Task();            
            $entity->exchangeArray($row);
            $user = $em->find('Application\Entity\User', $row->user_created_id	);            
            $entity->setUserCreated($user);
            $entity->setCharge($this->_usersTaskLinkerTable->getAllChargeByTask($entity->getId()));           
            $entity->setProgression($this->_usersTaskLinkerTable->getAllProgressionByTask($entity->getId()));
            $entities[] = $entity;
        }
        return $entities;
    }
    
    public function getAllTaskOfProject($listTasks){
    	$entities = array();
    	foreach ($listTasks as $listTask){
    		$resultSet = $this->select(function (Select $select) use ($listTask) {
    			$select->where->equalTo('listtasks_id', $listTask);
    		});
    		foreach ($resultSet as $row) {
    			$entity = new Entity\Task();
    			$entity->exchangeArray($row);
   				//$user = $em->find('Application\Entity\User', $row->user_created_id	);
   				//$entity->setUserCreated($user);
   				$entity->setCharge($this->_usersTaskLinkerTable->getAllChargeByTask($entity->getId()));
   				$entity->setProgression($this->_usersTaskLinkerTable->getAllProgressionByTask($entity->getId()));
   				$entities[] = $entity;
   			}    			
    	}
    	return $entities;
    }
    
    public function countTaskOfProject($listTasks){
    	$nbr = 0;
    	foreach ($listTasks as $listTask){
    		$resultSet = $this->select(function (Select $select) use ($listTask) {
    			$select->where->equalTo('listtasks_id', $listTask);
    		});
    		
    		$nbr += $resultSet->count();    		
    	}
    	return $nbr;
    }
    
    public function getAllWithIdList($listtasks_id = null) {
    	$resultSet = $this->select(function (Select $select) use ($listtasks_id) {
    		$select->where->equalTo('listtasks_id', $listtasks_id);
    	});
    	$entities = array();
    	foreach ($resultSet as $row) {    		    		
    		$entities[] = $row->id;
    	}
    	return $entities;
    }
    
    public function removeAllTasksWithList($id_listTask){
    	foreach ($this->getAllWithIdList($id_listTask) as $idT){
    		$this->_usersTaskLinkerTable->removeByIdTask($idT);
    	}
    	$resultSet = $this->delete(function (Delete $delete) use ($id_listTask) {
    		$delete->where->equalTo('listtasks_id', $id_listTask);
    	});
    	return $resultSet;    	
    }
    
    public function removeTask($id_task){
    	$this->_usersTaskLinkerTable->removeByIdTask($id_task);    	
    	return $this->delete(array('id' => (int) $id_task));
    }
    
    public function updateTask(Entity\Task $task){
    	$data = $task->getArray();
    	$idTask = (int) $task->getId();
    	if (!$this->update($data, array('id' => $idTask)))
    		return false;
    	return true;
    }
    
    public function saveTask(Entity\Task $task, $userTable, $chargeUsers){
    	$data = $task->getArray();
    	$id = (int) $task->getId();
    	// add task
    	if ($id == 0) {
    		if (!$this->insert($data))
    			return false;
    		$id_task = $this->getLastInsertValue();
    		// add les users de la task 
    		$i = 0;
    		foreach ($task->getUserTable() as $u){
    			$this->_usersTaskLinkerTable->saveParams($id_task, $u, $chargeUsers[$i]);
    			$i++;
    		}
    		return $id_task;
    	}
    	//filetype.sql "insert into" (login OR Password)
    	// intext (login Or Password) filetype.xls
    	// intitle:"index of" inurl:free.fr
    	// update task
    	else{    		
    		// update les users de la task
    		// Si on enleve les users : on supprime les users de tasks de la table userstasklinker     		
    		if ($userTable == null){    			
    			if ($task->getUserTable() != null){    				
    				$this->_usersTaskLinkerTable->removeByIdTask($id);// remove Les user;
    			}
    		}
    		else
    		 {    		 	
    			// Si la task n'avais pas des users : On ajout nouveau users dans la table userstasklinker
    			if ($task->getUserTable() == null){ 
    				$i = 0;
    				foreach ($userTable as $u){
    					$this->_usersTaskLinkerTable->saveParams($id, $u, $chargeUsers[$i]);
    					$i++;
    				}
    			}
    			// Si on a enlever quelque users ou on a ajouter : alors on update la table userstasklinker
    			else{    				
    				// recuperation d'un tableau pour les nouveau user ki n'existe pas dans la task
    				// l'ajout des users qui n'existe pas dans la task � updater
    				$i = 0;
    				foreach ($userTable as $u){    					
    					if (!$task->verifUser($u)){    					
    						$this->_usersTaskLinkerTable->saveParams($id, $u, $chargeUsers[$i]);
    					}
    					// verifier la modification de la charge et pas de l'utilisateur    					
    					else{
    						if (!$task->verifChargeUser($u, $chargeUsers[$i]))
    							$this->_usersTaskLinkerTable->updateParamsForCharge($id, $u, $chargeUsers[$i]);
    					}    						
    					$i++;
    				}    				
    				// suppression des users qui existe dans la task et pas dans dans la son update
    				$verif = false;    			
    				foreach ($task->getUserTable() as $u){
    					foreach ($userTable as $tu){
    						if ($u->getUserId() == $tu){
    							$verif = true;
    						}
    					}
    					if (!$verif){
    						$this->_usersTaskLinkerTable->removeParams($id, $u->getUserId());
    					}
    					else{
    						$verif = false;
    					}
    				}
    			}
    		}
    		    		
    		if (!$this->update($data, array('id' => $id)))
    			return false;
    		
    		return true;
    	}
    }        

    public function getTask($id) {
        $row = $this->select(array('id' => (int) $id))->current();
    	if (!$row)
    		return false;
    	$task = new Entity\Task();
    	$task->exchangeArray($row);
    	$task->setUserTable($this->_usersTaskLinkerTable->getUsersByTask($id));
    	$task->setCharge($this->_usersTaskLinkerTable->getAllChargeByTask($id));
    	$task->setProgression($this->_usersTaskLinkerTable->getAllProgressionByTask($id));
    	
    	return $task;
    }
    
    public function getTaskByOnlyUser($id, $id_user){    	
    	$row = $this->select(array('id' => (int) $id))->current();
    	if (!$row)
    		return false;
    	$task = new Entity\Task();
    	$task->exchangeArray($row);
    	$task->setUserTable($this->_usersTaskLinkerTable->getUsersByTask($id));
    	$task->setCharge($this->_usersTaskLinkerTable->getChargeByTaskAndUser($id, $id_user));
    	$task->setProgression($this->_usersTaskLinkerTable->getProgressionByTaskAndUser($id, $id_user));
    	 
    	return $task;
    }

}