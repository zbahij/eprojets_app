<?php

namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
/**
 *
 * @author lasmoum
 *        
 */
class ListTasksTable extends AbstractTableGateway {
	protected $table = 'listtasks';
	protected $_taskTable;
	protected $_jalonTable;
	
	public function __construct(Adapter $adapter, TaskTable $taskTable, JalonTable $jalonTable) {
		$this->adapter = $adapter;
		$this->_taskTable = $taskTable;
		$this->_jalonTable = $jalonTable;
	}	
	
	public function getIdProjectByIdList($list_id){
		$resultSet = $this->select(function (Select $select) use ($list_id) {
			$select->where->equalTo('id', $list_id);
		});
		foreach ($resultSet as $row) {
			return $row->project_id;
		}
	}
	
	public function getAllListTaskByProject($project_id){
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);			
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entities[] = $row->id;
		}
		
		return $entities;
	}
		
	public function fetchAll($project_id = null, $em) {
			
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\ListTasks();
			$entity->exchangeArray($row);
			// recuperer le jalon de la list $row->user_created_id
			$user = $em->find('Application\Entity\User', $row->user_created_id);
			$entity->setUserCreated($user);
			// recuperer le jalon de la list $entity->getId()
			if ($entity->getJalonId() != NULL){
				$entity->setJalon($this->_jalonTable->getJalon($entity->getJalonId(), $em));
			}
			// recuperer un tableau de toutes les tasks de la list $entity->getId()
			$entity->setTasks($this->_taskTable->fetchAll($entity->getId(), $em));			
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function getListTaskForUpdate($id_listTask){
		$row = $this->select(array('id' => (int) $id_listTask))->current();
		if (!$row)
			return false;
		$listTasks = new Entity\ListTasks();
		$listTasks->exchangeArray($row);		
		
		return $listTasks;
	}
	
	
	
	public function removeListTasks($id_listTask){		
		$this->_taskTable->removeAllTasksWithList($id_listTask);
		return $this->delete(array('id' => (int) $id_listTask));
	}
	
	public function saveListTasks(Entity\ListTasks $listTasks) {
		$data = $listTasks->getArray();
		$id = (int) $listTasks->getId();


		if ($id == 0) {
			if (!$this->insert($data))
				return false;
			return $this->getLastInsertValue();
		}
		else{
			if (!$this->update($data, array('id' => $id)))
				return false;
				
			return true;
		}
	}
}