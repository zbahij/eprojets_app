<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;

class PenseBetesTable extends AbstractTableGateway {

    protected $table = 'pensebetes';

    public function __construct(Adapter $adapter) {
    	$this->adapter = $adapter;
    }

    public function fetchAll($user_id = null) {
    	
        $resultSet = $this->select(function (Select $select) use ($user_id) {
        			$select->where->equalTo('user_id', $user_id);
                    $select->order('created ASC');
                });      
        $entities = array();
        foreach ($resultSet as $row) {
            $entity = new Entity\PenseBetes();
            $entity->setId($row->id)
                    ->setNote($row->note)
                    ->setCreated($row->created);
            $entities[] = $entity;
        }
        return $entities;
    }

    public function getPenseBetes($id) {
        $row = $this->select(array('id' => (int) $id))->current();
        if (!$row)
            return false;

        $pensebetes = new Entity\PenseBetes(array(
                    'id' => $row->id,
                    'note' => $row->note,
                    'created' => $row->created,
                ));
        return $pensebetes;
    }

    public function savePenseBetes(Entity\PenseBetes $penseBetes, $user_id = null) {
    	    	
    	$data = array(
            'note' => $penseBetes->getNote(),
            'created' => $penseBetes->getCreated(),    		
        );
    	if ($user_id != null)
    	{    		
    		$data['user_id'] = $user_id;
    	}

        $id = (int) $penseBetes->getId();

        if ($id == 0) {
            $data['created'] = date("Y-m-d H:i:s");
            if (!$this->insert($data))
                return false;
            return $this->getLastInsertValue();
        }
        elseif ($this->getPenseBetes($id)) {
            if (!$this->update($data, array('id' => $id)))
                return false;
            return $id;
        }
        else
            return false;
    }

    public function removePenseBetes($id) {
        return $this->delete(array('id' => (int) $id));
    }

}