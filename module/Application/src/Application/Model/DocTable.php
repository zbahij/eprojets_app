<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Zend\Db\Sql\Delete;
/**
 *
 * @author lasmoum
 *        
 */
class DocTable  extends AbstractTableGateway {
	protected $table = 'doc';
	/**
	 */
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	public function fetchAllByProject($project_id = null) {
		
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);
			$select->order('created ASC');
		});
		
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\Doc();
			$entity->exchangeArray($row);												
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function countDocOfProject($project_id = null){
		$resultSet = $this->select(function (Select $select) use ($project_id) {
			$select->where->equalTo('project_id', $project_id);
		});
		
		return $resultSet->count();
	}
	
	public function fetchAllByTask($task_id = null) {
			
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->where->equalTo('task_id', $task_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\Doc();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function getDoc($id) {
		$row = $this->select(array('id' => (int) $id))->current();
		if (!$row)
			return false;
		$doc = new Entity\Doc();
		$doc->exchangeArray($row);				
		
		return $doc;
	}
	
	public function saveDoc(Entity\Doc $doc) {		
		$data = $doc->getArray();	
		$id = (int) $doc->getId();
		
		if ($id == 0) {			
			if (!$this->insert($data))
				return false;
			return $this->getLastInsertValue();
		}
		else {
			if (!$this->update($data, array('id' => $id)))
				return false;
							
			return true;
		}		
	}
	
	public function removeDoc($id) {
		return $this->delete(array('id' => (int) $id));
	}
	
	public function removeAllDocByProject($project_id){
		$resultSet = $this->delete(function (Delete $delete) use ($project_id) {
			$delete->where->equalTo('$project_id', $project_id);
		});
		return $resultSet;
	}
}

?>