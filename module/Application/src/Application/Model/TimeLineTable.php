<?php

namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;

class TimeLineTable extends AbstractTableGateway{	
	
		protected $table = 'timeline';
	
		public function __construct(Adapter $adapter) {
			$this->adapter = $adapter;
		}
		
		
		public function fetchAllWithID($project_id = null) {			
			$rows = $this->select(function (Select $select) use ($project_id) {
						$select->where->equalTo('project_id', $project_id);
						$select->order('created DESC');
			});
			if (!$rows)
				return false;
			
			$entities = array();
			foreach ($rows as $row) {
				$entity = new Entity\TimeLine();
				$entity->exchangeArray($row);				
				$entities[] = $entity;
			}			
			return $entities;
		}		
	
		public function addTimeLine($data, $project_id = null) {
			$data['created'] = date("Y-m-d H:i:s");
			
			if (!$this->insert($data))
				return false;
			return $this->getLastInsertValue();
		}
	
		public function removeTimeLine($id) {
			return $this->delete(array('id' => (int) $id));
		}
}