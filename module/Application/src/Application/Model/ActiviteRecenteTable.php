<?php

namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
/**
 *
 * @author lasmoum
 *        
 */
class ActiviteRecenteTable  extends AbstractTableGateway {
	protected $table = 'activite_recente';
	
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	
	public function fetchAllWithIdUser($user_id = null) {
		$rows = $this->select(function (Select $select) use ($user_id) {
			$select->where->equalTo('user_id', $user_id);
			$select->order('date DESC');
		});
		if (!$rows)
			return false;
			
		$entities = array();
		foreach ($rows as $row) {
			$entity = new Entity\ActiviteRecente();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function addActiveRecente($data) {
		$data['date'] = date("Y-m-d H:i:s");
			
		if (!$this->insert($data))
			return false;
		return $this->getLastInsertValue();
	}
	
	public function removeActiveRecente($id) {
		return $this->delete(array('id' => (int) $id));
	}
	
	public function removeActiveRecenteByUser($user_id) {
		$resultSet = $this->delete(function (Delete $delete) use ($user_id) {
			$delete->where->equalTo('user_id', $user_id);
		});		
	}
}