<?php

namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Zend\Db\Sql\Delete;
/**
 *
 * @author lasmoum
 *        
 */
class UsersTaskLinkerTable extends AbstractTableGateway{
	protected $table = 'users_task_linker';
	
	/**
	 */
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	public function fetchAll() {	
		$resultSet = $this->select(function (Select $select) {			
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\UsersTaskLinker();
			$entity->exchangeArray($row);									
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function getTasksByUser($user_id) {
		$resultSet = $this->select(function (Select $select) use ($user_id) {
			$select->where->equalTo('user_id', $user_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\UsersTaskLinker();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return  $entities;
	}
	public function getUsersByTask($task_id) {
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->where->equalTo('task_id', $task_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {
			$entity = new Entity\UsersTaskLinker();
			$entity->exchangeArray($row);
			$entities[] = $entity;
		}
		return $entities;
	}
	
	public function getUsersByTaskTable($task_id) {
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->where->equalTo('task_id', $task_id);
			$select->order('created ASC');
		});
		$entities = array();
		foreach ($resultSet as $row) {			
			$entities[] = $row->user_id;
		}
		return $entities;
	}
	
	public function save(Entity\UsersTaskLinker $usersTaskLinker) {
		$data = $usersTaskLinker->getArray();
		if (!$this->insert($data))
				return false;
		return $this->getLastInsertValue();		
	}
	
	public function saveParams($task_id, $user_id, $charge) {
		$data = array();
		$data['task_id'] = (int) $task_id;
		$data['user_id'] = (int) $user_id;
		$data['created'] = date("Y-m-d H:i:s");
		$data['charge'] = $charge;
		if (!$this->insert($data))
			return false;
		return $this->getLastInsertValue();
	}
	
	public function updateParamsForCharge($task_id, $user_id, $charge) {		
		$data = array();		
		$data['charge'] = $charge;
		if (!$this->update($data, array('user_id' => $user_id, 'task_id' => $task_id)))
			return false;
		
		return true;
	}
	
	public function remove(Entity\UsersTaskLinker $usersTaskLinker){		
		return $this->delete(array(
				'task_id' => (int) $usersTaskLinker->getTaskId(),
				'user_id' => (int) $usersTaskLinker->getUserId()
				));		
	}
	
	public function removeParams($task_id, $user_id){
		return $this->delete(array(
				'task_id' => (int) $task_id,
				'user_id' => (int) $user_id
		));
	}
	
	public function removeByIdTask($task_id){		
		$resultSet = $this->delete(function (Delete $delete) use ($task_id) {
			$delete->where->equalTo('task_id', $task_id);
		});		
		return $resultSet;
	}
	
	public function getAllChargeByTask($task_id){		
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->columns(array(new \Zend\Db\Sql\Expression('SUM(charge) as charge')))->where->equalTo('task_id', $task_id);
		});
		$a = $resultSet->toArray();		
		return $a[0]['charge'];					
	}
	
	public function getAllProgressionByTask($task_id){
		$resultSet = $this->select(function (Select $select) use ($task_id) {
			$select->columns(array(new \Zend\Db\Sql\Expression('SUM(prog) as prog')))->where->equalTo('task_id', $task_id);
		});
		$a = $resultSet->toArray();		
		return $a[0]['prog'];
	}
	
	public function getChargeByTaskAndUser($task_id, $user_id){
		$resultSet = $this->select(function (Select $select) use ($task_id, $user_id) {
			$select->columns(array(new \Zend\Db\Sql\Expression('SUM(charge) as charge')));
			$select->where->equalTo('task_id', $task_id);
			$select->where->equalTo('user_id', $user_id);
		});
		
		$a = $resultSet->toArray();
		return $a[0]['charge'];
	}
	
	public function getProgressionByTaskAndUser($task_id, $user_id){
		$resultSet = $this->select(function (Select $select) use ($task_id, $user_id) {
			$select->columns(array(new \Zend\Db\Sql\Expression('SUM(prog) as prog')));
			$select->where->equalTo('task_id', $task_id);
			$select->where->equalTo('user_id', $user_id);
		});
		$a = $resultSet->toArray();
		return $a[0]['prog'];

	}
	
	public function setProgression($id_task, $id_user, $prog){
		$data = array();
		$data['prog'] = $prog;
		if (!$this->update($data, array('user_id' => $id_user, 'task_id' => $id_task)))
			return false;
		
		return true;
	}
}