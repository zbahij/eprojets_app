<?php

namespace Application\Model;


use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Application\Entity;
use Entities;
/**
 *
 * @author lasmoum
 *        
 */
class UserTable extends AbstractTableGateway {
	
	protected $table = 'user';
	
	public function __construct(Adapter $adapter) {
		$this->adapter = $adapter;
	}
	
	public function getUserById($user_id)
	{
		$resultSet = $this->select(function (Select $select) use ($user_id) {
			$select->where->equalTo('user_id', $user_id);
		});
	
			$row = $resultSet->current();
			
			$entity = new Entity\User();
			$entity->exchangeArray($row);
						
			return $entity;
	}
	
	public function upd(\Application\Entity\User $user){
		$id = (int) $user->__get('user_id');		
		if ($id == 0)			
			return false;
		else {
			$user->setUpdated(date("Y-m-d H:i:s"));
			if (!$this->update($user->getArray(), array('user_id' => $id)))
				return false;
							
			return $user;
		}		
	}	
	public function updLog(\Application\Entity\User $user){
		$id = (int) $user->__get('user_id');
		if ($id == 0)
			return false;
		else {
			if (!$this->update($user->getArray(), array('user_id' => $id)))
				return false;
				
			return $user;
		}
	}
}