<?php

namespace Application\Entity;

/**
 *
 * @author lasmoum
 *        
 */
class ActiviteRecente {
	
	public $_id;
	public $_user_id;
	public $_str;
	public $_date;
	public $_action;
	public $_project_id;
	
	public function exchangeArray($data)
	{
		$this->_id     = (isset($data['id'])) ? $data['id'] : null;
		$this->_user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
		$this->_str  = (isset($data['str'])) ? $data['str'] : null;
		$this->_date  = (isset($data['date'])) ? $data['date'] : null;
		$this->_action  = (isset($data['action'])) ? $data['action'] : null;
		$this->_project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
	}
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	public function __set($name, $value) {
		$method = 'set' . $name;
		if (!method_exists($this, $method)) {
			throw new Exception('Invalid Method');
		}
		$this->$method($value);
	}
	
	public function __get($name) {
		$method = 'get' . $name;
		if (!method_exists($this, $method)) {
			throw new Exception('Invalid Method');
		}
		return $this->$method();
	}
	
	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	
	/**
	 * @return the $_project_id
	 */
	public function getProject_id() {
		return $this->_project_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_project_id
	 */
	public function setProject_id($_project_id) {
		$this->_project_id = $_project_id;
	}

	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return the $_user_id
	 */
	public function getUser_id() {
		return $this->_user_id;
	}

	/**
	 * @return the $_str
	 */
	public function getStr() {
		return $this->_str;
	}

	/**
	 * @return the $_date
	 */
	public function getDate() {
		return $this->_date;
	}

	/**
	 * @return the $_action
	 */
	public function getAction() {
		return $this->_action;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_id
	 */
	public function setUser_id($_user_id) {
		$this->_user_id = $_user_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_str
	 */
	public function setStr($_str) {
		$this->_str = $_str;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_date
	 */
	public function setDate($_date) {
		$this->_date = $_date;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_action
	 */
	public function setAction($_action) {
		$this->_action = $_action;
	}
}