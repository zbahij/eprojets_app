<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;


/** @ORM\Entity
 *  @ORM\HasLifecycleCallbacks
 * 	@ORM\Table(name="user")
 * */
class User extends \ZfcUser\Entity\User implements \ZfcUser\Entity\UserInterface
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer", name="user_id")	 
	 */
	protected $user_id;			
	
	/** @ORM\Column(length=255, nullable=true) */
	protected $username;
	
	/** @ORM\Column(length=255) */
	protected $email;
	
    /** @ORM\Column(length=128) */
	protected $password;
	
	/** @ORM\Column(length=50) */
	protected $display_name;
	
	/** @ORM\Column(type="smallint", nullable=true) */
	protected $state;		

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $created;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;
	
	/**
	 *  
	 * @ORM\OneToMany(targetEntity="UsersProjectsLinker", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true) 
	 * @ORM\JoinColumn(name="User_user_id", referencedColumnName="user_id", nullable=true) 
	 * */
	protected $projects;
	
	/**
	 * 
	 * @ORM\Column(type="integer")
	 */
	protected $logged;
	
	public function exchangeArray($data)
	{
		$this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
		$this->username = (isset($data['username'])) ? $data['username'] : null;
		$this->email = (isset($data['email'])) ? $data['email'] : null;
		$this->password = (isset($data['password'])) ? $data['password'] : null;
		$this->display_name = (isset($data['display_name'])) ? $data['display_name'] : null;
		$this->created = (isset($data['created'])) ? $data['created'] : null;
		$this->state = (isset($data['state'])) ? $data['state'] : null;
		$this->updated = (isset($data['updated'])) ? $data['updated'] : null;		
		$this->logged = (isset($data['logged'])) ? $data['logged'] : null;
	}
	
	public function getArray(){
		$data = array(
				'user_id' => $this->getUserId(),
				'username' => $this->getUsername(),
				'email' => $this->getEmail(),
				'password' => $this->getPassword(),
				'display_name' => $this->getDisplayName(),				
				'state' => $this->getState(),
				'updated' => $this->getUpdated(),
				'logged' => $this->getLogged()
		);
		return $data;
	}
	
	
	
	/**
	 * @param Ambigous <NULL, unknown> $logged
	 */
	public function setLogged($logged) {
		$this->logged = $logged;
	}

	/**
	 * @return the $created
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @param Ambigous <\DateTime, NULL, unknown> $created
	 */
	public function setCreated($created) {
		$this->created = $created;
	}

	public function __construct() {
		$this->projects = new \Doctrine\Common\Collections\ArrayCollection();	
	}
	
	/** @ORM\PrePersist */
	function onPrePersist()
	{
		if (empty($this->created))
			$this->created = new \DateTime();
		$this->updated = new \DateTime();
	}
	
	/**
	 * Add project 
	 *
	 * @param Project $project
	 * @return User
	 */
	public function addProject(Project $project) {
    	$relation = new UsersProjectsLinker($this, $project);
    	if (!$this->projects->contains($relation)) {    		
  	    	$this->projects->add($relation);
    	}
    	return $this;
	}
	
	/**
	 * Remove project
	 *
	 * @param Project $project
	 * @return User
	 */
	public function removeProject(Project $project) {
		$relation = new UsersProjectsLinker($this, $project);
		if (!$this->projects->contains($relation)) {
			if ($this->projects->remove($relation))
				return true;			
		}
		return false;
	}
	
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property
	 * @return mixed
	 */
	public function __get($property)
	{
		return $this->$property;
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property
	 * @param mixed $value
	 */
	public function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}
	
	/**
	 * Set id.
	 *
	 * @param int $id
	 * @return UserInterface
	 */
	public function setUserId($id)
	{
		$this->user_id = (int) $id;
		return $this;
	}
	
	/**
	 * Get username.
	 *
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}
	
	/**
	 * Set username.
	 *
	 * @param string $username
	 * @return UserInterface
	 */
	public function setUsername($username)
	{
		$this->username = $username;
		return $this;
	}
	
	/**
	 * Get email.
	 *
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}
	
	/**
	 * Set email.
	 *
	 * @param string $email
	 * @return UserInterface
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}
	
	/**
	 * Get displayName.
	 *
	 * @return string
	 */
	public function getDisplayName()
	{
		return $this->display_name;
	}
	
	/**
	 * Set displayName.
	 *
	 * @param string $displayName
	 * @return UserInterface
	 */
	public function setDisplayName($displayName)
	{
		$this->display_name = $displayName;
		return $this;
	}
	
	/**
	 * Get password.
	 *
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}
	
	/**
	 * Set password.
	 *
	 * @param string $password
	 * @return UserInterface
	 */
	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}
	
	/**
	 * Get state.
	 *
	 * @return int
	 */
	public function getState()
	{
		return $this->state;
	}
	
	/**
	 * Set state.
	 *
	 * @param int $state
	 * @return UserInterface
	 */
	public function setState($state)
	{
		$this->state = $state;
		return $this;
	}
	
	public function getLogged()
	{
		return $this->logged;
	}
	/**
	 * @return the $updated
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * @param Ambigous <\DateTime, NULL, unknown> $updated
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;
	}
}
