<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/** @ORM\Entity 
 *  @ORM\HasLifecycleCallbacks
 * 	@ORM\Table(name="project")
 **/

class Project implements InputFilterAwareInterface {
	protected $inputFilter;
	
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/** @ORM\Column(length=128) */
	protected $name;
	
	/** @ORM\Column(length=500, nullable=true)
	 * */	
	protected $description;	
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $created;
	
	/**
	 * @ORM\Column(type="datetime")	 
	 */
	protected $updated;
	
	/** @ORM\OneToMany(targetEntity="UsersProjectsLinker", mappedBy="project", cascade={"persist", "remove"}, orphanRemoval=true) 
	 *  @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=true)
	 * */
	private $users;
	
	public function __construct() {
		$this->users = new \Doctrine\Common\Collections\ArrayCollection();
	}	
	
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property
	 * @return mixed
	 */
	public function __get($property)
	{
		return $this->$property;
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property
	 * @param mixed $value
	 */
	public function __set($property, $value)
	{
		$this->$property = $value;
	}	
	
	/** @ORM\PrePersist */
	function onPrePersist()
	{
		if (empty($this->created))
			$this->created = new \DateTime();
		$this->updated = new \DateTime();
	}
	
	/**
	 * Convert the object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	/**
	 * Populate from an array.
	 *
	 * @param array $data
	 */
	public function populate($data = array())
	{
		$this->id = $data['id'];
		$this->name = $data['name'];
		$this->description = $data['description'];
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
	
			$factory = new InputFactory();
	
			$inputFilter->add($factory->createInput(array(
					'name'       => 'id',
					'required'   => true,
					'filters' => array(
							array('name'    => 'Int'),
					),
			)));
	
			$inputFilter->add($factory->createInput(array(
					'name'     => 'name',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
	
			$inputFilter->add($factory->createInput(array(
					'name'     => 'description',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
	
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}
	
	/**
	 * Add user 
	 *
	 * @param User $user
	 * @return Project
	 */	 
	public function addUser(User $user) {		
		if ($this->hasUser($user))
		{					
    		$relation = new UsersProjectsLinker($user, $this);
    		if (!$this->users->contains($relation)) { 	
  	    		$this->users->add($user);
    		}
  	    	return true;
		}
		else
			return false;
		    	
	}
	
	/**
	 * Remove user
	 *
	 * @param User $user
	 * @return Project
	 */
	public function removeUser(User $user) {
		if ($this->hasUser($user))
		{	
			$relation = new UsersProjectsLinker($user, $this);
			if (!$this->users->contains($relation)) {
				$this->users->remove($user);
			}
			return $this;
		}
		else
			return false;
		 
	}
	public function hasUser(User $user) {
		foreach ($this->users as $u) {
			if ($u->user->id == $user->id) {
				return true;
			}
		}	
		return false;
	}
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return the $created
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @return the $updated
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * @return the $users
	 */
	public function getUsers() {
		return $this->users;
	}

	/**
	 * @param array $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @param array $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @param array $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated($created) {
		$this->created = $created;
	}

	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated) {
		$this->updated = $updated;
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $users
	 */
	public function setUsers($users) {
		$this->users = $users;
	}
}
