<?php

namespace Application\Entity;
/**
 *
 * @author lasmoum
 *        
 */
class UsersTaskLinker {
	
	protected $_user_id;
	protected $_task_id;
	protected $_created;
	protected $_prog;
	protected $_charge;
	/**
	 */
	public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {    	    	
    	$this->_user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
    	$this->_task_id = (isset($data['task_id'])) ? $data['task_id'] : null;
    	$this->_created = (isset($data['created'])) ? $data['created'] : null;
    	$this->_prog = (isset($data['prog'])) ? $data['prog'] : null;
    	$this->_charge = (isset($data['charge'])) ? $data['charge'] : null;
    }

    public function getArray(){
    	if (empty($this->_created))
    		$this->_created = date("Y-m-d H:i:s");
    	$data = array(    			
    			'created' => $this->_created,
    			'task_id'=> $this->_task_id,
    			'user_id'=> $this->_user_id,
    			'prog'=> $this->_prog,
    			'charge'=> $this->_charge
    	);
    	return $data;
    }
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    
	/**
	 * @return the $_prog
	 */
	public function getProg() {
		return $this->_prog;
	}

	/**
	 * @return the $_charge
	 */
	public function getCharge() {
		return $this->_charge;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_prog
	 */
	public function setProg($_prog) {
		$this->_prog = $_prog;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_charge
	 */
	public function setCharge($_charge) {
		$this->_charge = $_charge;
	}

	/**
	 * @return the $_user_id
	 */
	public function getUserId() {
		return $this->_user_id;
	}

	/**
	 * @return the $_task_id
	 */
	public function getTaskId() {
		return $this->_task_id;
	}

	/**
	 * @return the $_created
	 */
	public function getCreated() {
		return $this->_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_id
	 */
	public function setUserId($_user_id) {
		$this->_user_id = $_user_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_task_id
	 */
	public function setTaskId($_task_id) {
		$this->_task_id = $_task_id;
	}

	/**
	 * @param Ambigous <string, NULL, unknown> $_created
	 */
	public function setCreated($_created) {
		$this->_created = $_created;
	}        	
}