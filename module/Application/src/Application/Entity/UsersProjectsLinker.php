<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity 
 *  @ORM\HasLifecycleCallbacks
 * 	@ORM\Table(name="users_projects_linker") 
 **/

class UsersProjectsLinker {	

	/**
	 * @ORM\Id()
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="projects") 
	 * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="User_user_id", referencedColumnName="user_id")
     * })	 	
	 * */
	protected $user;
	
	/**
	 * @ORM\Id()
	 * @ORM\ManyToOne(targetEntity="Project", inversedBy="users") 
	 * 	   
	 *  */
	protected $project;
		
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $created;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $updated;
			
	public function __construct(User $user, Project $project) {
		$this->user = $user;
		$this->project = $project;		
	}	
	
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property
	 * @return mixed
	 */
	public function __get($property)
	{
		return $this->$property;
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property
	 * @param mixed $value
	 */
	public function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	/** @ORM\PrePersist */
	function onPrePersist()
	{
		if (empty($this->created))
			$this->created = new \DateTime();
		$this->updated = new \DateTime();
	}
}
