<?php

namespace Application\Entity;

/**
 *
 * @author lasmoum
 *        
 */
class Jalon {
	protected $_id;
	protected $_updated;
	protected $_created;
	protected $_user_id;
	protected $_user;
	protected $_project_id;
	protected $_project;
	protected $_name;
	protected $_description;
	protected $_date;
	protected $_responsable;
	protected $_responsable_user;
	protected $_status;	
	
	/**
	 */
   public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {
    	$this->_project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
    	$this->_name = (isset($data['name'])) ? $data['name'] : null;
    	$this->_date = (isset($data['date'])) ? $data['date'] : null;
    	$this->_id = (isset($data['id'])) ? $data['id'] : null;
    	$this->_updated = (isset($data['updated'])) ? $data['updated'] : null;
    	$this->_created = (isset($data['created'])) ? $data['created'] : null;
    	$this->_user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
    	$this->_description = (isset($data['description'])) ? $data['description'] : null;
    	$this->_user = (isset($data['user'])) ? $data['user'] : null;
    	$this->_responsable = (isset($data['responsable'])) ? $data['responsable'] : null;
    	$this->_responsable_user = (isset($data['responsable_user'])) ? $data['responsable_user'] : null;
    	$this->_status = (isset($data['status'])) ? $data['status'] : null;
    }
    
    public function getArray(){
    	if (empty($this->_created))
    		$this->_created = date("Y-m-d H:i:s");
    	$data = array(
    			'project_id' => $this->getProject_id(),
    			'name' => $this->getName(),
    			'date' => $this->getDate(),
    			'updated' => $this->getUpdated(),
    			'created' => $this->getCreated(),
    			'user_id' => $this->getUserId(),
    			'description' => $this->getDescription(),
    			'responsable' => $this->getReponsable(),
    			'status' => $this->getStatus()
    	);
    	return $data; 
    }    
    
   
    
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function getStatus(){
    	return $this->_status;
    }
    
    public function setStatus($status){
    	$this->_status = $status;
    	return $this;
    }
    
    public function setResponsable($responsable){
    	$this->_responsable = $responsable;
    	return $this;
    }
    public function getReponsable(){
    	return $this->_responsable;
    }
    
    public function setResponsableUser(User $user){
    	$this->_responsable_user = $user;    	
    }
    public function getResponsableUser(){
    	return $this->_responsable_user;
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }
    
    public function getUser() {
    	return $this->_user;
    }
    
    public function setUser(User $user) {
    	$this->_user = $user;
    	return $this;
    }

    public function getUserId() {
    	return $this->_user_id;
    }
    
    public function setUserId($user_id) {
    	$this->_user_id = $user_id;
    	return $this;
    }
    
    public function setUser_id($user_id) {
    	$this->_user_id = $user_id;
    	return $this;
    }
    
    public function getProject_id() {
        return $this->_project_id;
    }
    
    public function getProject() {
    	return $this->_project;
    }
    
    public function setProject($project)
    {
    	$this->_project = $project;
    	return $this;
    }

    public function setProject_id($project_id) {
        $this->_project_id = $project_id;
        return $this;
    }

    public function getCreated() {
        return $this->_created;
    }

    public function setCreated($created) {
        $this->_created = $created;
        return $this;
    }
    
    public function getUpdated() {
    	return $this->_updated;
    }
    
    public function setUpdated($updated) {
    	$this->_updated = $updated;
    	return $this;
    }
    
    public function getName() {
    	return $this->_name;
    }
    
    public function setName($name) {
    	$this->_name = $name;
    	return $this;
    }
    
    public function getDescription() {
    	return $this->_description;
    }
    
    public function setDescription($description) {
    	$this->_description = $description;
    	return $this;
    }
    
    public function setDate($date)
    {
    	$this->_date = $date;
    	return $this;
    }

    public function getDate()
    {    	
    	return $this->_date;
    }
}