<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity  
 * 	@ORM\Table(name="user_provider") 
 **/

class UserProvider {	

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * */
	protected $id;
	/**
	 * @ORM\Column(type="integer")	  	 
	 * */
	protected $user_id;
	
	/**
	 * @ORM\Column(type="integer")
	 * */
	protected $provider_id;
	
	/**	 
	 * @ORM\Column(length=255)
	 */
	protected $provider;
		
}
