<?php

namespace Application\Entity;

/**
 *
 * @author lasmoum
 *        
 */
class TaskDocLinker {
	
	protected $_idTask;
    protected $_idDoc;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {    	    	
    	$this->_idTask = (isset($data['task_id'])) ? $data['task_id'] : null;
    	$this->_idDoc = (isset($data['doc_id'])) ? $data['doc_id'] : null;
    }
    
    public function getArray(){    	
    	$data = array(
    			'task_id' => $this->_idTask,
    			'doc_id' => $this->_idDoc,    			
    	);
    	return $data;
    }
    
    public function __set($name, $value) {
    	$method = 'set' . $name;
    	if (!method_exists($this, $method)) {
    		throw new \Exception('Invalid Method');
    	}
    	$this->$method($value);
    }
    
    public function __get($name) {
    	$method = 'get' . $name;
    	if (!method_exists($this, $method)) {
    		throw new \Exception('Invalid Method');
    	}
    	return $this->$method();
    }
    
    public function setOptions(array $options) {
    	$methods = get_class_methods($this);
    	foreach ($options as $key => $value) {
    		$method = 'set' . ucfirst($key);
    		if (in_array($method, $methods)) {
    			$this->$method($value);
    		}
    	}
    	return $this;
    }
	/**
	 * @return the $_idTask
	 */
	public function getIdTask() {
		return $this->_idTask;
	}

	/**
	 * @return the $_idDoc
	 */
	public function getIdDoc() {
		return $this->_idDoc;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_idTask
	 */
	public function setIdTask($_idTask) {
		$this->_idTask = $_idTask;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_idDoc
	 */
	public function setIdDoc($_idDoc) {
		$this->_idDoc = $_idDoc;
	}        
}