<?php

namespace Application\Entity;

class Task {

    protected $_id;
    protected $_updated;
    protected $_created;    
    protected $_name;
    protected $_description;
    protected $_start_date;
    protected $_end_date;
    protected $_listtasks_id;
    protected $_user_updated_id;
    protected $_user_updated;
    protected $_user_created_id;
    protected $_user_created;
    protected $_charge;
    protected $_progression;
    protected $_status;
    protected $_rappel;
    protected $_userTable;
    protected $_critique;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {    	    	
    	$this->_name = (isset($data['name'])) ? $data['name'] : null;
    	$this->_start_date = (isset($data['start_date'])) ? $data['start_date'] : null;
    	$this->_id = (isset($data['id'])) ? $data['id'] : null;
    	$this->_end_date = (isset($data['end_date'])) ? $data['end_date'] : null;
    	$this->_updated = (isset($data['updated'])) ? $data['updated'] : null;
    	$this->_created = (isset($data['created'])) ? $data['created'] : null;    	
    	$this->_description = (isset($data['description'])) ? $data['description'] : null;    	
    	$this->_listtasks_id = (isset($data['listtasks_id'])) ? $data['listtasks_id'] : null;
    	$this->_user_created_id = (isset($data['user_created_id'])) ? $data['user_created_id'] : null;
    	$this->_user_updated_id = (isset($data['user_updated_id'])) ? $data['user_updated_id'] : null;
    	$this->_charge = (isset($data['charge'])) ? $data['charge'] : null;
    	$this->_progression = (isset($data['progression'])) ? $data['progression'] : null;
    	$this->_status = (isset($data['status'])) ? $data['status'] : null;
    	$this->_rappel = (isset($data['rappel'])) ? $data['rappel'] : null;
    	$this->_critique = (isset($data['critique'])) ? $data['critique'] : null;
    }

    public function getArray(){
    	if (empty($this->_created))
    		$this->_created = date("Y-m-d H:i:s");
    	$data = array(
    			'charge' => $this->_charge,
    			'progression' => $this->_progression,
    			'status' => $this->_status,
    			'rappel' => $this->_rappel,
    			'name' => $this->_name,
    			'updated' => $this->_updated,
    			'start_date' => $this->_start_date,
    			'end_date' => $this->_end_date,
    			'description' => $this->_description,
    			'created' => $this->_created,
    			'user_created_id'=> $this->_user_created_id,
    			'user_updated_id'=> $this->_user_updated_id,
    			'listtasks_id' => $this->_listtasks_id,
    			'critique' => $this->_critique,
    	);
    	return $data;
    }
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    
	/**
	 * @return the $_critique
	 */
	public function getCritique() {
		return $this->_critique;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_critique
	 */
	public function setCritique($_critique) {
		$this->_critique = $_critique;
	}

	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return the $_updated
	 */
	public function getUpdated() {
		return $this->_updated;
	}

	/**
	 * @return the $_created
	 */
	public function getCreated() {
		return $this->_created;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @return the $_start_date
	 */
	public function getStartDate() {
		return $this->_start_date;
	}

	/**
	 * @return the $_end_date
	 */
	public function getEndDate() {
		return $this->_end_date;
	}

	/**
	 * @return the $_listtasks_id
	 */
	public function getListtasksId() {
		return $this->_listtasks_id;
	}

	/**
	 * @return the $_user_updated_id
	 */
	public function getUserUpdatedId() {
		return $this->_user_updated_id;
	}

	/**
	 * @return the $_user_updated
	 */
	public function getUserUpdated() {
		return $this->_user_updated;
	}

	/**
	 * @return the $_user_created_id
	 */
	public function getUserCreatedId() {
		return $this->_user_created_id;
	}

	/**
	 * @return the $_user_created
	 */
	public function getUserCreated() {
		return $this->_user_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_updated
	 */
	public function setUpdated($_updated) {
		$this->_updated = $_updated;
	}

	/**
	 * @param Ambigous <string, NULL, unknown> $_created
	 */
	public function setCreated($_created) {
		$this->_created = $_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_start_date
	 */
	public function setStartDate($_start_date) {
		$this->_start_date = $_start_date;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_end_date
	 */
	public function setEndDate($_end_date) {
		$this->_end_date = $_end_date;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_listtasks_id
	 */
	public function setListtasksId($_listtasks_id) {
		$this->_listtasks_id = $_listtasks_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_updated_id
	 */
	public function setUserUpdatedId($_user_updated_id) {
		$this->_user_updated_id = $_user_updated_id;
	}

	/**
	 * @param field_type $_user_updated
	 */
	public function setUserUpdated($_user_updated) {
		$this->_user_updated = $_user_updated;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_created_id
	 */
	public function setUserCreatedId($_user_created_id) {
		$this->_user_created_id = $_user_created_id;
	}

	/**
	 * @param field_type $_user_created
	 */
	public function setUserCreated($_user_created) {
		$this->_user_created = $_user_created;
	}
	/**
	 * @return the $_charge
	 */
	public function getCharge() {
		return $this->_charge;
	}

	/**
	 * @return the $_progression
	 */
	public function getProgression() {
		return $this->_progression;
	}

	/**
	 * @return the $_status
	 */
	public function getStatus() {
		return $this->_status;
	}

	/**
	 * @return the $_rappel
	 */
	public function getRappel() {
		return $this->_rappel;
	}

	/**
	 * @param field_type $_charge
	 */
	public function setCharge($_charge) {
		$this->_charge = $_charge;
	}

	/**
	 * @param field_type $_progression
	 */
	public function setProgression($_progression) {
		$this->_progression = $_progression;
	}

	/**
	 * @param field_type $_status
	 */
	public function setStatus($_status) {
		$this->_status = $_status;
	}

	/**
	 * @param field_type $_rappel
	 */
	public function setRappel($_rappel) {
		$this->_rappel = $_rappel;
	}
	/**
	 * @return the $_userTable
	 */
	public function getUserTable() {
		return $this->_userTable;
	}

	/**
	 * @param field_type $_userTable
	 */
	public function setUserTable(array $_userTable) {
		$this->_userTable = $_userTable;
	}
	
	
	public function getPourcentage()
	{		
		if ($this->_status == 1)
			return 100;
		if ($this->_charge != 0)
			return (($this->_progression / $this->_charge) * 100) ;
		else
			return 0;
	}

	public function verifUser($user_id){
		foreach ($this->_userTable as $u){
			if ($user_id == $u->getUserId())
				return true;
		}
		return false;
	}
	
	public function getChargeByUser($user_id){
		foreach ($this->_userTable as $u){
			if ($user_id == $u->getUserId()){
				return $u->getCharge();
			}			
		}
		return false;
	}

	public function verifChargeUser($user_id, $chargeUser){
		if ($this->getChargeByUser($user_id) == $chargeUser)
			return true;
		return false;
	}
	
}
