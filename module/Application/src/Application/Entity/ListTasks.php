<?php

namespace Application\Entity;


/**
 *
 * @author lasmoum
 *        
 */
class ListTasks {
		
	protected $_id;	
	protected $_name;	
	protected $_description;
	protected $_created;
	protected $_updated;
	protected $_start_date;
	protected $_end_date;
	
	protected $_project;	
	protected $_user_created;	
	protected $_user_updated;
	protected $_jalon;
	
	protected $_project_id;
	protected $_jalon_id;
	protected $_user_created_id;
	protected $_user_updated_id;
	
	protected $_charge;
	protected $_status;
	protected $_progression;
	
	protected $_tasks = array();
	
   public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {    	
    	$this->_jalon_id = (isset($data['jalon_id'])) ? $data['jalon_id'] : null;
    	$this->_jalon = (isset($data['jalon'])) ? $data['jalon'] : null;
    	$this->_charge = (isset($data['charge'])) ? $data['charge'] : null;
    	$this->_status = (isset($data['status'])) ? $data['status'] : null;
    	$this->_progression = (isset($data['progression'])) ? $data['progression'] : null;
    	$this->_name = (isset($data['name'])) ? $data['name'] : null;
    	$this->_start_date = (isset($data['start_date'])) ? $data['start_date'] : null;
    	$this->_id = (isset($data['id'])) ? $data['id'] : null;
    	$this->_end_date = (isset($data['end_date'])) ? $data['end_date'] : null;
    	$this->_updated = (isset($data['updated'])) ? $data['updated'] : null;
    	$this->_created = (isset($data['created'])) ? $data['created'] : null;
    	$this->_user_created_id = (isset($data['user_created_id'])) ? $data['user_created_id'] : null;
    	$this->_user_created = (isset($data['user_created'])) ? $data['user_created'] : null;
    	$this->_description = (isset($data['description'])) ? $data['description'] : null;
    	$this->_user_updated_id = (isset($data['user_updated_id'])) ? $data['user_updated_id'] : null;
    	$this->_user_updated = (isset($data['user_updated'])) ? $data['user_updated'] : null;
    	$this->_project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
    	$this->_project = (isset($data['project'])) ? $data['project'] : null;
    }
    
    public function getArray(){
    	if (empty($this->_created))
    		$this->_created = date("Y-m-d H:i:s");
    	$data = array(
    			'jalon_id' => $this->_jalon_id,
    			'name' => $this->_name,
    			'updated' => $this->_updated,
    			'start_date' => $this->_start_date,
    			'end_date' => $this->_end_date,
    			'charge' => $this->_charge,
    			'description' => $this->_description,
    			'progression' => $this->_progression,
    			'status' => $this->_status,   
    			'created' => $this->_created, 
    			'user_created_id'=> $this->_user_created_id,	
    		 	'user_updated_id' => $this->_user_updated_id,
    			'project_id' => $this->_project_id,    					  		
    	    	);
    	return $data;
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
        
    public function getTasks(){
    	return $this->_tasks;
    }
    public function setTasks($tasks){
    	$this->_tasks = $tasks;
    }
    
	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @return the $_created
	 */
	public function getCreated() {
		return $this->_created;
	}

	/**
	 * @return the $_updated
	 */
	public function getUpdated() {
		return $this->_updated;
	}

	/**
	 * @return the $_start_date
	 */
	public function getStartDate() {
		return $this->_start_date;
	}

	/**
	 * @return the $_end_date
	 */
	public function getEndDate() {
		return $this->_end_date;
	}

	/**
	 * @return the $_project_id
	 */
	public function getProjectId() {
		return $this->_project_id;
	}

	/**
	 * @return the $_user_created_id
	 */
	public function getUserCreatedId() {
		return $this->_user_created_id;
	}

	/**
	 * @return the $_user_updated_id
	 */
	public function getUserUpdatedId() {
		return $this->_user_updated_id;
	}

	/**
	 * @return the $_jalon_id
	 */
	public function getJalonId() {
		return $this->_jalon_id;
	}

	/**
	 * @return the $_charge
	 */
	public function getCharge() {
		return $this->_charge;
	}

	/**
	 * @return the $_status
	 */
	public function getStatus() {
		return $this->_status;
	}

	/**
	 * @return the $_progression
	 */
	public function getProgression() {
		return $this->_progression;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_created
	 */
	public function setCreated($_created) {
		$this->_created = $_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_updated
	 */
	public function setUpdated($_updated) {
		$this->_updated = $_updated;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_start_date
	 */
	public function setStartDate($_start_date) {
		$this->_start_date = $_start_date;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_end_date
	 */
	public function setEndDate($_end_date) {
		$this->_end_date = $_end_date;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_project_id
	 */
	public function setProjectId($_project_id) {
		$this->_project_id = $_project_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_created_id
	 */
	public function setUserCreatedId($_user_created_id) {
		$this->_user_created_id = $_user_created_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_updated_id
	 */
	public function setUserUpdatedId($_user_updated_id) {
		$this->_user_updated_id = $_user_updated_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_jalon_id
	 */
	public function setJalonId($_jalon_id) {
		$this->_jalon_id = $_jalon_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_charge
	 */
	public function setCharge($_charge) {
		$this->_charge = $_charge;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_status
	 */
	public function setStatus($_status) {
		$this->_status = $_status;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_progression
	 */
	public function setProgression($_progression) {
		$this->_progression = $_progression;
	}
	/**
	 * @return the $_project
	 */
	public function getProject() {
		return $this->_project;
	}

	/**
	 * @return the $_user_created
	 */
	public function getUserCreated() {
		return $this->_user_created;
	}

	/**
	 * @return the $_user_updated
	 */
	public function getUserUpdated() {
		return $this->_user_updated;
	}

	/**
	 * @return the $_jalon
	 */
	public function getJalon() {
		return $this->_jalon;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_project
	 */
	public function setProject(Project $_project) {
		$this->_project = $_project;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_created
	 */
	public function setUserCreated(User $_user_created) {
		$this->_user_created = $_user_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_updated
	 */
	public function setUserUpdated(User $_user_updated) {
		$this->_user_updated = $_user_updated;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_jalon
	 */
	public function setJalon(Jalon $_jalon) {
		$this->_jalon = $_jalon;
	}
}