<?php

namespace Application\Entity;

class TimeLine {
	public $_id;
	public $_description;
	public $_authors;
	public $_created;
	public $_project_id;
	public $_activite;
	
/**
	 * Used by ResultSet to pass each database row to the entity
	 */
	public function exchangeArray($data)
	{
		$this->_id     = (isset($data['id'])) ? $data['id'] : null;
		$this->_project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
		$this->_created  = (isset($data['created'])) ? $data['created'] : null;
		$this->_authors  = (isset($data['authors'])) ? $data['authors'] : null;
		$this->_description  = (isset($data['description'])) ? $data['description'] : null;
		$this->_activite  = (isset($data['activite'])) ? $data['activite'] : null;
	}
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	public function __set($name, $value) {
		$method = 'set' . $name;
		if (!method_exists($this, $method)) {
			throw new Exception('Invalid Method');
		}
		$this->$method($value);
	}
	
	public function __get($name) {
		$method = 'get' . $name;
		if (!method_exists($this, $method)) {
			throw new Exception('Invalid Method');
		}
		return $this->$method();
	}
	
	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function getActivite() {
		return $this->_activite;
	}
	
	public function setActivite($activite) {
		$this->_activite = $activite;
		return $this;
	}
	
	public function getAuthors() {
		return $this->_authors;
	}
	
	public function setAuthors($authors) {
		$this->_authors = $authors;
		return $this;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function setId($id) {
		$this->_id = $id;
		return $this;
	}
	
	public function getProjectId() {
		return $this->_project_id;
	}
	
	public function setProjectId($project_id) {
		$this->_project_id = $project_id;
		return $this;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function setDescription($description) {
		$this->_description = $description;
		return $this;
	}
	
	public function getCreated() {
		return $this->_created;
	}
	
	public function setCreated($created) {
		$this->_created = $created;
		return $this;
	}
}