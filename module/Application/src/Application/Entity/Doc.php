<?php 

namespace  Application\Entity;

class Doc {
	
	protected $_id;
	protected $_name;
	protected $_description;
	protected $_extension;
	protected $_project_id;
	protected $_created;
	protected $_updated;	
	protected $_user_created_id;	
	protected $_task_id;
		
	/**
	 */
   public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function exchangeArray($data)
    {
    	$this->_id = (isset($data['id'])) ? $data['id'] : null;
    	$this->_name = (isset($data['name'])) ? $data['name'] : null;
    	$this->_description = (isset($data['description'])) ? $data['description'] : null;
    	$this->_extension = (isset($data['extension'])) ? $data['extension'] : null;
    	$this->_project_id = (isset($data['project_id'])) ? $data['project_id'] : null;
    	$this->_created = (isset($data['created'])) ? $data['created'] : null;
    	$this->_updated = (isset($data['updated'])) ? $data['updated'] : null;    	
    	$this->_user_created_id = (isset($data['user_created_id'])) ? $data['user_created_id'] : null;    	
    	$this->_task_id = (isset($data['task_id'])) ? $data['task_id'] : null;
    }
    
    public function getArray(){
    	if (empty($this->_created))
    		$this->_created = date("Y-m-d H:i:s");
    	$data = array(
    			'id' => $this->_id,
    			'project_id' => $this->getProject_id(),
    			'name' => $this->getName(),    			
    			'updated' => $this->getUpdated(),
    			'created' => $this->getCreated(),
    			'user_created_id' => $this->_user_created_id,
    			'description' => $this->getDescription(),    			
    			'extension' => $this->getExtension(),
    			'task_id' => $this->_task_id,
    	);
    	return $data; 
    }    
    
   
    
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    
	/**
	 * @return the $_task_id
	 */
	public function getTask_id() {
		return $this->_task_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_task_id
	 */
	public function setTask_id($_task_id) {
		$this->_task_id = $_task_id;
	}

	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @return the $_extension
	 */
	public function getExtension() {
		return $this->_extension;
	}

	/**
	 * @return the $_project_id
	 */
	public function getProject_id() {
		return $this->_project_id;
	}

	/**
	 * @return the $_created
	 */
	public function getCreated() {
		return $this->_created;
	}

	/**
	 * @return the $_updated
	 */
	public function getUpdated() {
		return $this->_updated;
	}

	/**
	 * @return the $_user_created_id
	 */
	public function getUser_created_id() {
		return $this->_user_created_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_extension
	 */
	public function setExtension($_extension) {
		$this->_extension = $_extension;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_project_id
	 */
	public function setProject_id($_project_id) {
		$this->_project_id = $_project_id;
	}

	/**
	 * @param Ambigous <string, NULL, unknown> $_created
	 */
	public function setCreated($_created) {
		$this->_created = $_created;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_updated
	 */
	public function setUpdated($_updated) {
		$this->_updated = $_updated;
	}

	/**
	 * @param Ambigous <NULL, unknown> $_user_created_id
	 */
	public function setUser_created_id($_user_created_id) {
		$this->_user_created_id = $_user_created_id;
	}
}