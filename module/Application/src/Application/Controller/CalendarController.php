<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Doctrine\DBAL\Schema\View;

class CalendarController extends AbstractController
{
	protected $_timelineTable;
	
    public function indexAction()
    {     	   	    	    	    	       	    	     
        return new ViewModel($this->setVariables());
    }
    
    public function getcalendarbyprojectAction()
    {
    	$response = $this->getResponse();
    	$this->setCurrentProject();
    	$this->setUser();
    	if ($this->getCurrentProject() != null)
    	{
    		$this->setUsers();
    		$listTasks = $this->getListTaskTable()->getAllListTaskByProject($this->getCurrentProject()->getId());
    		$events = array();
    		$event = null;
    		foreach ($listTasks as $lt)
    		{
    			$tasks = $this->getTaskTable()->fetchAll($lt, $this->getEm());
    			foreach ($tasks as $t)
    			{
    				$event = array(
    						'id' => $t->getId(),
    						'title' => $t->getName(),
    						'start' => \Application\Utility\Helper::dateTimeToDate($t->getStartDate()),
    						'end' => \Application\Utility\Helper::dateTimeToDate($t->getEndDate()),
    						'id_project' => $this->getCurrentProject()->getId(),
    						'id_listtask' => $t->getListtasksId(),
    						'url' => '/app/tasks/view/'.$t->getListtasksId().'/'.$t->getId().'/project/'.$this->getCurrentProject()->getId(),
    						);
    			}
    			if ($event != null)
    				$events[] = $event;
    			$event = null;
    		}
    		$response->setContent(\Zend\Json\Json::encode($events));    		    		
    	}    	
    	return $response;
    }
    
    public function fillMyCalendarAction()
    {
    	$response = $this->getResponse();    	
    	$this->setUser();    	    	    
    	$tasks = $this->getTaskTable()->fetchAllByUser($this->getUser()->user_id);
    	$events = array();
    	foreach ($tasks as $t)
    	{    		
    		$id_project = $this->getListTaskTable()->getIdProjectByIdList($t->getListtasksId());
    		$project = $this->getEM()->find('Application\Entity\Project', $id_project);    		
    		$event = array(
    				'id' => $t->getId(),
    				'title' => "[" . $project->getName() . "] : " . $t->getName(),
    				'start' => \Application\Utility\Helper::dateTimeToDate($t->getStartDate()),
    				'end' => \Application\Utility\Helper::dateTimeToDate($t->getEndDate()),
    				'id_project' => $project->getId(),
    				'id_listtask' => $t->getListtasksId(),
    				'url' => '/app/tasks/view/'.$t->getListtasksId().'/'.$t->getId().'/project/'.$project->getId(),
    		);
    
    		$events[] = $event;
    	}
    
    	$response->setContent(\Zend\Json\Json::encode($events));
    	return $response;
    }

    public function updDateTaskByProjectAction()
    {
    	$response = $this->getResponse();
    	$request = $this->getRequest();
    	$this->setUser();
    	if ($request->isPost()) {
    		$post_data = $request->getPost();
    		$task_id = $post_data['task_id'];
    		if (isset($post_data['date_start']))
    			$date_start =  $post_data['date_start'];
    		else
    			$date_start = null;
    		$date_end = strftime($post_data['date_end']);
    		$project_id = $post_data['project_id'];    		
    		$taskName = $post_data['title'];    		
    		if($this->getTaskTable()->setDateStartEnd($task_id, $date_start, $date_end)){
    			$data = array(
    					'project_id' => $project_id,
    					'authors' => 'Modifi&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    					'description' => 'T�che Modifi&eacute; <strong>' . $taskName . '</strong>',
    					'activite' => 'tache',
    			);
    			$this->getTimeLineTable()->addTimeLine($data, $project_id);
    			$recentActivite = array(
    					'user_id' => $this->getUser()->user_id,
    					'project_id' => $project_id,
    					'action' => 'app/tasks/project/'.$project_id,
    					'str' => $data['description'],
    			);
    			$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    			$response->setContent(\Zend\Json\Json::encode(array('response' => true, 'start' => $date_start, 'end'=>$date_end)));
    		}
    		else
    			$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	}
    	return $response;
    }
    public function updDateTaskByUserAction()
    {
    	$response = $this->getResponse();
    	$request = $this->getRequest();
    	$this->setUser();
    	if ($request->isPost()) {
    		$post_data = $request->getPost();
    		$task_id = $post_data['task_id'];
    		if (isset($post_data['date_start']))    			    			
    			$date_start =  $post_data['date_start'];
    		else 
    			$date_start = null;
    		$date_end = strftime($post_data['date_end']);
    		$project_id = $post_data['project_id'];
    		$title = $post_data['title'];
    		$taskName = explode(']', $title);
    		$taskName = $taskName[1];
			if($this->getTaskTable()->setDateStartEnd($task_id, $date_start, $date_end)){
				$data = array(
						'project_id' => $project_id,
						'authors' => 'Modifi&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
						'description' => 'T�che Modifi&eacute; <strong>' . $taskName . '</strong>',
						'activite' => 'tache',
				);
				$this->getTimeLineTable()->addTimeLine($data, $project_id);
				$recentActivite = array(
						'user_id' => $this->getUser()->user_id,
						'project_id' => $project_id,
						'action' => 'app/tasks/project/'.$project_id,
						'str' => $data['description'],
				);
				$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
				$response->setContent(\Zend\Json\Json::encode(array('response' => true, 'start' => $date_start, 'end'=>$date_end)));
			}
			else
				$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	}
    	return $response;
    }
    
    public function calendarProjectAction(){
    	$this->setUser();
    	$this->setCurrentProject(); 
    	$view = new ViewModel();
    	$view->setTemplate('application/calendar/calendar-project');
    	$view->setVariables(array(
    			'currentProject' => $this->getCurrentProject(), 
    	));
    	return $view;
    }
}
