<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;

class ProjectController extends AbstractController
{
	protected $_timelineTable;
	
    public function indexAction()
    {        	    	
        return new ViewModel($this->setVariables());
    }
    public function getLoggerAction()
    {
    	$request = $this->getRequest();
    	$response = $this->getResponse();
    	if ($request->isPost())
    	{
    		$newUser = new \Application\Entity\User;
    		$newUser = $this->getEm()->find('Application\Entity\User' , $request->getPost('id'));
    		$response->setContent(\Zend\Json\Json::encode(array('response' => true, 'logger' => $newUser->getLogger())));
    			
    	}
    	else
    		$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    
    	return $response;
    }
    public function timelineAction()
    {    	    	   	
 	   	return new ViewModel($this->setVariables()); 	   	
    }  
    
    public function addAction() {
    	$this->setUser();
    	$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    	$user_id = $this->getUser()->user_id;
    	$request = $this->getRequest();

    	if ($request->isPost())
    	{
    		$new_project = new \Application\Entity\Project();
    		$new_project->__set('name', $request->getPost('name'));
    		$new_project->__set('description', $request->getPost('description'));
    		$em->persist($new_project);
    		$em->flush();
    		$user = $this->getUser();
    		$user->addProject($new_project);		 
    		$em->persist($user);
    		$em->flush();
    		// Ajouter dans la table timeLine    		
    		$data = array(
    				'project_id' => $new_project->__get('id'),
    				'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'description' => 'Nouveau projet cr&eacute;&eacute; <strong>' . $new_project->__get('name') . '</strong>',
    				'activite' => 'project',
    		);
    		$recentActivite = array(
    				'user_id' => $user_id,
    				'project_id' => $new_project->__get('id'),
    				'action' => '/app/project/'.$new_project->__get('id').'/timeline',
    				'str' => 'Nouveau projet cr&eacute;&eacute; <strong>' . $new_project->__get('name') . '</strong>',
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->getTimeLineTable()->addTimeLine($data, $new_project->__get('id'));
    		
    		
    		$this->redirect()->toRoute('project', array('id' => $new_project->__get('id'), 'action' => 'timeline'));
    	}
    	else 
    		return;// TODO return une error
    		
    }
    public function removeAction(){
    	$id_project = $this->params()->fromRoute('id', null);
    	if (!$id_project){
    		//TODO
    		// return error
    		//return $this->redirect()->toRoute('album');
    	}
    	
    	$project = $this->getEM()->find('Application\Entity\Project', $id_project);
    	$this->getEM()->remove($project);
    	$this->getEM()->flush();
    	return $this->redirect()->toRoute('app');
    }
    
    public function editAction()
    {
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$post_data = $request->getPost();
    		$note_id = $post_data['id'];
    		$note_content = $post_data['content'];
    		$pensebete = $this->getpensebetesTable()->getPenseBetes($note_id);
    		$pensebete->setNote($note_content);
    		
    		if (!$this->getpensebetesTable()->savePenseBetes($pensebete))
    			$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    		else {
    			$response->setContent(\Zend\Json\Json::encode(array('response' => true)));
    		}
    	}
    	return $response;
    }  
	
	public function fillcalendarAction()
	{
		$this->setUser();
//		var_dump($this->getCurrentProject());die;
		$sm = $this->getServiceLocator();
		
		$tasks = $sm->get('Application\Model\TaskTable')->fetchAllByUser($this->getUser()->user_id);
		$events = array();
		foreach ($tasks as $t)
		{
			$event = array('id' => $t->getId(),
					'title' => $t->getName(),
					'start' => \Application\Utility\Helper::dateTimeToDate($t->getStartDate()),
					'end' => \Application\Utility\Helper::dateTimeToDate($t->getEndDate()),
					'url' => '',
			);
				
			$events[] = $event;
		}
		
		$result = new JsonModel($events);
		
		return $result;
	}
    
    public function calendarAction()
    {    	
    	return new ViewModel($this->setVariables());
    }   
}
