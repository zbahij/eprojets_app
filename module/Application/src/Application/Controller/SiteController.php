<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity;
class SiteController extends AbstractController
{
    public function indexAction()
    {   
    	$this->setUser(); 	   
    	$this->layout('layout/layout_site');
        return new ViewModel(array('user' => $this->getUser()));
    }
    
    public function aboutAction() {
    	$this->setUser();
    	$this->layout('layout/layout_site');
    	return new ViewModel(array('user' => $this->getUser()));
    }
    
    public function contactAction() {
    	
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$post_data = $request->getPost();
    		
    		$firstname = $post_data['firstname'];
    		$lastname = $post_data['lastname'];
    		$to = $post_data['email'];
  			$subject = $post_data['subject'];
  			$msg = $post_data['message'];
    		
    		// message
    		$message = '
     <html>
      <head>
       <title>Contact via la page de contact Eprojets</title>
      </head>
      <body>
       <p>Contact via la page de contact Eprojets</p>
       <p>De la part de : ' . $firstname . ' '.$lastname.'</p>
       <p>Objet : ' . $subject. '</p>
       <p>' . $msg . '</p>
      </body>
     </html>
     ';
    		
    		// Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
    		$headers  = 'MIME-Version: 1.0' . "\r\n";
    		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    		
    		// En-têtes additionnels
    		$headers .= 'To: Zouhir Bahij <toma1024@gmail.com>' . "\r\n";
    		$headers .= 'From: Eprojets <contact@eprojets.com>' . "\r\n";    		
    		
    		// Envoi
    		mail($to, $subject, $message, $headers);
    		$this->flashMessenger()->addMessage("Merci pour votre message");
    			    		
    	}
    	
    	
    	$this->setUser();
    	$this->layout('layout/layout_site');
    	
    	
    	return new ViewModel(array('user' => $this->getUser(), 'flashMessages' => $this->flashMessenger()->getMessages()));
    }
}
