<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
/**
 *
 * @author lasmoum
 *        
 */
class StatistiqueController extends AbstractController{
	
	/**
	 */
	public function indexAction()
    {
    	$this->setUser();
    	$user_id = $this->getUser()->user_id;
    	$u = $this->getEm()->find('Application\Entity\User', $user_id);
    	$p = $u->__get('projects');
    	
    	$projectName = array();
    	$projectId = array();
    	$coutTask = array();
    	$coutMyTask = array();
    	$finishTask = 0;
    	$enCourTask = 0;
    	$retTask = 0;
    	
    	foreach ($p as $proj){
    		$ctks = 0;
    		$cmtks = 0;
    		$projectName[] = $proj->project->name;
    		$projectId[] = $proj->project->id;
    		$listTask = $this->getListTaskTable()->fetchAll($proj->project->id, $this->getEm());
    		foreach ($listTask as $lt){
    			$ctks += sizeof($lt->getTasks());
    			foreach ($lt->getTasks() as $task){    				
    				$task->setUserTable($this->getUsersTaskLinkerTable()->getUsersByTask($task->getId()));
	    			if ($task->verifUser($user_id)){
	    				$cmtks++;
	    				if ($task->getStatus() == 1)
	    					$finishTask++;
	    				else {
	    					$time = date("Y-m-d H:i:s");
	    					if ($task->getEndDate() > $time)
	    						$enCourTask++;
	    					else {
	    						$retTask++;
	    					}
	    				}
	    			}
    			}
    		}
    		
    		$coutTask[] = $ctks;
    		$coutMyTask[] = $cmtks;
    	}    	    	
    		
        return new ViewModel(array(
        		'projectName' => json_encode($projectName),
        		'countTask' => json_encode($coutTask),
        		'countMyTask' => json_encode($coutMyTask),
        		'finishTask' => json_decode($finishTask/($finishTask + $enCourTask + $retTask)),
        		'enCourTask' => json_decode($enCourTask/($finishTask + $enCourTask + $retTask)),
        		'retTask' => json_decode($retTask/($finishTask + $enCourTask + $retTask)),
        ));
    }
    
    public function getValueForChartPointsAction(){
    	$request = $this->getRequest();
    	$response = $this->getResponse();
    	if ($request->isPost())
    	{
    		$this->setUser();
    		$user_id = $this->getUser()->user_id;    	
			// 	recuperation de tous les userTaskLinker d'un user    	
    		$userTaskLinkers = $this->getUsersTaskLinkerTable()->getTasksByUser($user_id);
    		$ret = array();
    		foreach ($userTaskLinkers as $userTaskLinker){
    			$ret[] = array(
	    			'task_name' => $this->getTaskTable()->getTask($userTaskLinker->getTaskId())->getName(),
    				'charge' => $userTaskLinker->getCharge(),
    				'prog' => $userTaskLinker->getProg(),
    			);  		
    		}		    		
    		$response->setContent(\Zend\Json\Json::encode(array(
    				'response' => true,
    				'ret' => $ret
    		)));
    		return $response;
    	}
    	$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	return $response;
    }
    
    public function statProjectAction(){
    	$this->setUser();
    	$this->setCurrentProject();  
    	$this->setUsers();  	    	    	
    	$nbTask = $this->getTaskTable()->countTaskOfProject($this->getListTaskTable()->getAllListTaskByProject($this->getCurrentProject()->getId()));    	
    	$nbJalon = $this->getJalonTable()->countJalonOfProject($this->getCurrentProject()->getId());    	
    	$nbDoc = $this->getDocTable()->countDocOfProject($this->getCurrentProject()->getId());
    	$nbUser = $this->getUsers()->count();
    	$view = new ViewModel();
    	$view->setVariables(array(
    			'em' => $this->getEm(),
    			'currentProject' => $this->getCurrentProject(),
    			'user' => $this->getUser(),
    			'nbTask' => $nbTask,
    			'nbJalon' => $nbJalon,
    			'nbDoc' => $nbDoc,
    			'nbUser' => $nbUser,
    	));
    	$view->setTemplate('application/statistique/chart-project');
    	return $view;    	
    }
    
    public function getDataAction(){
    	$request = $this->getRequest();
    	$response = $this->getResponse();    	
    	    	
    	if ($request->isPost())
    	{
    		$this->setCurrentProject();
    		$tasks = $this->getTaskTable()->getAllTaskOfProject($this->getListTaskTable()->getAllListTaskByProject($this->getCurrentProject()->getId()));
    		 
    		$nbTaskR = 0;
    		$nbTaskC = 0;
    		$nbTaskF = 0;
    		$nbJalonR = 0;
    		$nbJalonC = 0;
    		$nbJalonF = 0;
    		$prog = array();
    		$charge = array();
    		$taskName = array();
    		$myDate = date("Y-m-d H:i:s");
    		foreach ($tasks as $task){
    			if ($task->getProgression() == null){
    				$prog[] = 0;
    			}
    			else
    				$prog[] = $task->getProgression();
    			     			
    			if ($task->getCharge() == null)
    				$charge[] = 0;
    			else
    				$charge[] = $task->getCharge();
    			
    			if ($task->getName() == null)
    				$taskName[] = "Unknown";
    			else
    				$taskName[] = $task->getName();
    			if ($task->getStatus() == 1)
    				$nbTaskF += 1;
    			else {
    				if ($task->getEndDate() >= $myDate)
    					$nbTaskC += 1;
    				else
    					$nbTaskR += 1;    				 
    			}
    		}
    		//Jalons : termin�s / restants / retards
    		$jalons = $this->getJalonTable()->fetchAll($this->getCurrentProject()->getId(), $this->getEm());
    		foreach ($jalons as $jalon){
    			if ($jalon->getStatus() == 1)
    				$nbJalonF += 1;
    			else {
    				if ($jalon->getDate() >= $myDate)
    					$nbJalonC += 1;
    				else
    					$nbJalonR += 1;
    		
    			}
    		}
    		$response->setContent(\Zend\Json\Json::encode(array(
    				'response' => true,
    				'nbTaskR' => $nbTaskR,
    				'nbTaskC' => $nbTaskC,
    				'nbTaskF' => $nbTaskF,
    				'nbJalonR' => $nbJalonR,
    				'nbJalonC' => $nbJalonC,
    				'nbJalonF' => $nbJalonF,
    				'prog' => $prog,
    				'charge' => $charge,
    				'taskName' => $taskName,    				
    		)));
    		return $response;
    	}
    	$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	return $response;
    }
}