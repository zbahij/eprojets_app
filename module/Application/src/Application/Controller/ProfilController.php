<?php

namespace Application\Controller;
use Zend\View\Model\ViewModel;
use Zend\Crypt\Password\Bcrypt;

/**
 *
 * @author lasmoum
 *        
 */
class ProfilController  extends AbstractController {
	
	public function indexAction() {
		$this->setUser();
		return new ViewModel(array(
			'user' => $this->getUserTable()->getUserById($this->getUser()->__get('user_id')), 
		));
	}
	

	public function updateInfoAction(){		
		$request = $this->getRequest();		
		$view = new ViewModel();
		if ($request->isPost())
		{
			$verif = false;
			$updUser = new \Application\Entity\User;
			$updUser = $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId());									
			
			if ($request->getPost('display_name') != null && $request->getPost('display_name') != $updUser->__get('display_name')){
				$updUser->setDisplayName($request->getPost('display_name'));
				$verif = true;					
			}
			if ($request->getPost('username') != null && $request->getPost('username') != $updUser->__get('username')){
				$updUser->__set('username', $request->getPost('username'));				
				$verif = true;				
			}
			
			if ($verif)
				$updUser = $this->getUserTable()->upd($updUser);
						
			if ($updUser == false){
				$view->setVariable(array(
						'user' => $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId()),
						'error' => true,
						'error_str' => 'Error updated'
				));
			}
			else{
				$view->setVariables(array(
					'user' => $updUser,
					'error' => false,
					'error_str' => 'Update succes'
				));
			}			
		}				
		$view->setTemplate('application/profil/index');		
		return $view;
	}
	
	public function updateMailAction(){
		$request = $this->getRequest();
		
		if ($request->isPost())
		{			
			
			$updUser = new \Application\Entity\User;
			$updUser = $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId());
			
			if ($request->getPost('newMail') != null && $request->getPost('confirmMail') != null && $request->getPost('password') != null){				
				if ($request->getPost('newMail') != $updUser->getEmail()){					
					if ($request->getPost('newMail') == $request->getPost('confirmMail')){						
						//if ($request->getPost('password') == $updUser->getPassword()){
							
							$updUser->setEmail($request->getPost('newMail'));
							$updUser = $this->getUserTable()->upd($updUser);
							if ($updUser != false){
								$view = new ViewModel(array(
										'user' => $updUser,
										'error' => false,
										'error_str' => 'Update mail succes',
								));
								
								$view->setTemplate('application/profil/index');
								return $view;
							}				
						//}	
					}		
				}	
			}
		}
		$view = new ViewModel(array(
				'user' => $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId()),
				'error' => true,
				'error_str' => 'Error mail updated',
		));				
		$view->setTemplate('application/profil/index');
		return $view;		
	}
	
	public function updatePasswordAction(){
		$request = $this->getRequest();
		
		if ($request->isPost())
		{				
			$updUser = new \Application\Entity\User;
			$updUser = $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId());
				
			$bcrypt = new Bcrypt();
			if ($request->getPost('oldPassword') != null && $request->getPost('newPassword') != null){				
					//if ($request->getPost('oldPassword') == $updUser->getPassword()){													
						$updUser->setPassword($bcrypt->create($request->getPost('newPassword')));
						$updUser = $this->getUserTable()->upd($updUser);
						if ($updUser != false){
							$view = new ViewModel(array(
									'user' => $updUser,
									'error_pass' => false,
									'error_str_pass' => 'Update password success',
							));
		
							$view->setTemplate('application/profil/index');
							return $view;
						//}
						//}
					}
				}
			
		}
		$view = new ViewModel(array(
				'user' => $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId()),
				'error_pass' => true,
				'error_str_pass' => 'Error password updated',
		));
		$view->setTemplate('application/profil/index');
		return $view;
	}
	
	public function forgotPasswordAction(){
		
	}
}