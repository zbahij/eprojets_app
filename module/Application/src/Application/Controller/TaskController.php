<?php
namespace Application\Controller;
use Zend\View\Model\ViewModel;

class TaskController extends AbstractController {
	   
    public function indexAction() {
    	$data = array();
    	$data = $this->setVariables();
    	    	
		$view = new ViewModel();
		$view->setVariables($data);		
		return $view;
    }
    
    public function getIdListTaskFromUrl(){
    	return $this->params()->fromRoute('id_listTask', null);
    }
    
    public function getIdTaskFromUrl(){
    	return $this->params()->fromRoute('id_task', null);
    }

    public function addListAction() {
    	$request = $this->getRequest();
    	$this->setCurrentProject();
    	if ($request->isPost())
    	{    		
    		$this->setUser();
    		$listTasksUpdate = new \Application\Entity\ListTasks();
    		$listTasksUpdate->setName($request->getPost('name'));
    		$listTasksUpdate->setDescription($request->getPost('description'));
    		$listTasksUpdate->setCreated(date("Y-m-d H:i:s"));
    		$listTasksUpdate->setUserCreatedId($this->getUser()->user_id);
    		$listTasksUpdate->setProjectId($this->getCurrentProject()->__get('id'));
    		$listTasksUpdate->setStatus(0);
    		if ($request->getPost('jalon_id') != -1)
    			$listTasksUpdate->setJalonId($request->getPost('jalon_id'));
    		
    		$this->getListTaskTable()->saveListTasks($listTasksUpdate);
    		 
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'tache',
    				'description' => 'Nouvelle liste des tâches cr&eacute;&eacute; <strong>' . $listTasksUpdate->__get('name') . '</strong>',
    		);    	
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
			$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		
    		$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {
    		$view = new ViewModel();    		
    		$this->setJalon();
    		$view->setVariables(array(
    				'currentProject' => $this->getCurrentProject(),
    				'jalons' => $this->getJalon()
    		));
    		return $view;
    	}
    }
    
    public function addAction() {
    	$request = $this->getRequest();
    	$this->setCurrentProject();
    	$id_listTask = $this->getIdListTaskFromUrl();    	 
    	if ($request->isPost())
    	{
    		$this->setUser();
    		$task = new \Application\Entity\Task();
    				    		
    		$task->setName($request->getPost('name'));
    		$task->setDescription($request->getPost('description'));
    		$task->setCreated(date("Y-m-d H:i:s"));
    		$task->setUserCreatedId($this->getUser()->user_id);
    		$task->setListtasksId($id_listTask);
    		$task->setStatus(0);
    		$task->setCritique(0);
    		$task->setProgression(0);
    		if ($request->getPost('start_date') != '')
    			$task->setStartDate($request->getPost('start_date'));
    		else
    			$task->setStartDate(date("Y-m-d H:i:s"));
    		if ($request->getPost('end_date') != '')
    			$task->setEndDate($request->getPost('end_date'));
    		$var = 0;
    		foreach ($request->getPost('chargeUsers') as $cu){
    			$var += $cu;
    		}
    		$task->setCharge($var);
    		
    		if ($request->getPost('rappel') != null)
    			$task->setRappel(1);
    		else
    			$task->setRappel(0);
    		if ($request->getPost('choixUser') != null)
    			$task->setUserTable($request->getPost('choixUser'));
    		    		
    		$id_task = $this->getTaskTable()->saveTask($task, $request->getPost('choixUser'), $request->getPost('chargeUsers')); 
    		if ($request->getPost('docs') != null)
    			$this->getTaskDocLinkerTable()->saveAllByTask($id_task, $request->getPost('docs'));   		    		
    		
    		// Ajouter dans la table timeLine
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'tache',
    				'description' => 'Nouvelle tâche  cr&eacute;&eacute; <strong>' . $task->__get('name') . '</strong>',
    		);    	
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
			$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {
    		$listTasks = new \Application\Entity\ListTasks();
    		$listTasks = $this->getListTaskTable()->getListTaskForUpdate($id_listTask);
    		$this->setUsers();
    		$listDocs = $this->getDocTable()->fetchAllByProject($this->getCurrentProject()->__get('id'));
    		$view = new ViewModel();
    		$view->setVariables(array(
    				'currentProject' => $this->getCurrentProject(),
    				'users' => $this->getUsers(),
    				'currentListTasks' => $listTasks,
    				'listDocs' => $listDocs
    		));
    		$view->setTemplate('application/task/addTache');
    		return $view;
    	}    	
    }
      
    
    public function removeListAction() {
    	$id_listTask = $this->getIdListTaskFromUrl();    	
    	$this->setCurrentProject();
    	$removeListTasks = $this->getListTaskTable()->getListTaskForUpdate($id_listTask);    	
    	$this->setUser();
    	// Ajouter dans la table timeLine
    	$data = array(
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'authors' => 'Supprim&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    			'description' => 'Liste des tâches supprim&eacute; <strong>' . $removeListTasks->__get('name') . '</strong>',
    			'activite' => 'tache',
    	);
    	$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    	$recentActivite = array(
    			'user_id' => $this->getUser()->user_id,
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
    			'str' => $data['description'],
    	);
    	$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    	$removeTask = $this->getTaskTable()->getAllWithIdList($id_listTask);
    	
   		$this->getTaskDocLinkerTable()->removeAllDocWithListOfTask($removeTask);
    	$this->getListTaskTable()->removeListTasks($id_listTask);
    	$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    }

    public function removeAction() {
    	$id_listTask = $this->getIdListTaskFromUrl();
    	$id_task = $this->getIdTaskFromUrl();
    	$this->setCurrentProject();
    	$removeTasks = $this->getTaskTable()->getTask($id_task);
    	$this->setUser();
    	// Ajouter dans la table timeLine
    	$data = array(
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'authors' => 'Supprim&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    			'description' => 'Tâche supprim&eacute; <strong>' . $removeTasks->__get('name') . '</strong>',
    			'activite' => 'tache',
    	);
    	$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    	$recentActivite = array(
    			'user_id' => $this->getUser()->user_id,
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
    			'str' => $data['description'],
    	);
    	$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    	
    	$this->getTaskDocLinkerTable()->removeAllByTask($id_task);
    	$this->getTaskTable()->removeTask($id_task);
    	$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    }

    public function updateListAction() {
    	$request = $this->getRequest();
    	$this->setCurrentProject();
    	$id_task = $this->getIdTaskFromUrl();
    	$id_listTask = $this->getIdListTaskFromUrl();
    	$listTasksUpdate = new \Application\Entity\ListTasks();
    	$listTasksUpdate = $this->getListTaskTable()->getListTaskForUpdate($id_listTask);
    	
    	if ($request->isPost())
    	{
    		$this->setUser();    		
    		$listTasksUpdate->setName($request->getPost('name'));    		
    		$listTasksUpdate->setDescription($request->getPost('description'));
    		$listTasksUpdate->setUpdated(date("Y-m-d H:i:s"));
    		$listTasksUpdate->setUserUpdatedId($this->getUser()->user_id);
    		if ($request->getPost('jalon_id') != -1)
    			$listTasksUpdate->setJalonId($request->getPost('jalon_id'));
    		else
    			$listTasksUpdate->setJalonId(null);
    	
    		$this->getListTaskTable()->saveListTasks($listTasksUpdate);
    		 
    		// Ajouter dans la table timeLine
    		    $data = array(
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'authors' => 'Modifi&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    			'description' => 'Liste des Tâches Modifi&eacute; <strong>' . $listTasksUpdate->__get('name') . '</strong>',
    			'activite' => 'tache',
		    	);
		    	$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
		    	$recentActivite = array(
		    			'user_id' => $this->getUser()->user_id,
		    			'project_id' => $this->getCurrentProject()->__get('id'),
		    			'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
		    			'str' => $data['description'],
		    	);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {    		    		
    		$view = new ViewModel();
    		$this->setJalon();
    		$view->setVariables(array(
    				'currentProject' => $this->getCurrentProject(),
    				'jalons' => $this->getJalon(),
    				'currentListTasks' => $listTasksUpdate
    		));
    		$view->setTemplate('application/task/updList');
    		return $view;
    	}
    }
    
    public function updateAction() {
    	$request = $this->getRequest();
    	$this->setCurrentProject();
    	$id_task = $this->getIdTaskFromUrl();
    	$id_listTask = $this->getIdListTaskFromUrl();
    	$listTasksUpdate = new \Application\Entity\ListTasks();
    	$listTasksUpdate = $this->getListTaskTable()->getListTaskForUpdate($id_listTask);
    	$taskUpdate = new \Application\Entity\Task();
    	$taskUpdate = $this->getTaskTable()->getTask($id_task);
    	$this->setUser();
    	$this->setUsers();
    	if ($request->isPost())
    	{    		        				    	
    		$taskUpdate->setName($request->getPost('name'));
    		$taskUpdate->setDescription($request->getPost('description'));    		
    		$taskUpdate->setUserUpdatedId($this->getUser()->user_id);
    		if ($request->getPost('status') == null)
    			$taskUpdate->setStatus(0);
    		else
    			$taskUpdate->setStatus(1);

    		if ($request->getPost('start_date') != '')
    			$taskUpdate->setStartDate($request->getPost('start_date'));
    		else
    			$taskUpdate->setStartDate(null);
    		if ($request->getPost('end_date') != '')
    			$taskUpdate->setEndDate($request->getPost('end_date'));
    		else 
    			$taskUpdate->setEndDate(null);
    		
    		$var = 0;
    		foreach ($request->getPost('chargeUsers') as $cu){
    			$var += $cu;
    		}
    		$taskUpdate->setCharge($var);
    		
    		if ($request->getPost('rappel') != null)
    			$taskUpdate->setRappel(1);
    		else
    			$taskUpdate->setRappel(0);
    		
    		//if ($request->getPost('choixUser') != null)
    			//$taskUpdate->setUserTable($request->getPost('choixUser'));    		
    			//var_dump($request->getPost('docs'));
    			
    		$this->getTaskDocLinkerTable()->updateAllByTask($id_task ,$request->getPost('docs'));
    		$this->getTaskTable()->saveTask($taskUpdate, $request->getPost('choixUser'), $request->getPost('chargeUsers'));
    		
    		// Ajouter dans la table timeLine
    		    $data = array(
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'authors' => 'Modifi&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    			'description' => 'Tâche Modifi&eacute; <strong>' . $taskUpdate->__get('name') . '</strong>',
    			'activite' => 'tache',
		    	);
		    	$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
		    	$recentActivite = array(
		    			'user_id' => $this->getUser()->user_id,
		    			'project_id' => $this->getCurrentProject()->__get('id'),
		    			'action' => 'app/tasks/project/'.$this->getCurrentProject()->__get('id'),
		    			'str' => $data['description'],
		    	);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->redirect()->toRoute('task',array('id' => $this->getCurrentProject()->__get('id')));
    		
    	}
    	else {
    		$listIdDocs = $this->getTaskDocLinkerTable()->fetchAllByTask($id_task);
    		 
    		$listDocs = array();
    		foreach($listIdDocs as $idDoc){
    			$listDocs[] = $this->getDocTable()->getDoc($idDoc->getIdDoc());
    		}
    		$view = new ViewModel();
    		$view->setVariables(array(
    				'currentProject' => $this->getCurrentProject(),
    				'currentListTasks' => $listTasksUpdate,
    				'taskUpdate' => $taskUpdate,
    				//'usersTask' => $this->getUsersTaskLinkerTable()->getUsersByTask($id_task),
    				'users' => $this->getUsers(),
    				'listDocs' => $listDocs,
    				'listAllDocs' => $this->getDocTable()->fetchAllByProject($this->getCurrentProject()->__get('id')),
    		));
    		$view->setTemplate('application/task/upd');
    		return $view;
    	}     
    }       
    
    public function viewAction() {
    	$request = $this->getRequest();
    	$this->setCurrentProject();
    	$id_task = $this->getIdTaskFromUrl();
    	$id_listTask = $this->getIdListTaskFromUrl();
    	$listTasksUpdate = new \Application\Entity\ListTasks();
    	$listTasksUpdate = $this->getListTaskTable()->getListTaskForUpdate($id_listTask);
    	$taskUpdate = new \Application\Entity\Task();
    	$this->setUser();
    	$this->setUsers();
    	
    	$taskUpdate = $this->getTaskTable()->getTaskByOnlyUser($id_task, $this->getUser()->__get('user_id'));    	
    	$listIdDocs = $this->getTaskDocLinkerTable()->fetchAllByTask($id_task);
 
    	$listDocs = array();
    	foreach($listIdDocs as $idDoc){    		    		     		    		
    		$listDocs[] = $this->getDocTable()->getDoc($idDoc->getIdDoc());
    	}    	
    	//var_dump($taskUpdate);  	
    	$view = new ViewModel();
    	$view->setVariables(array(
    			'currentProject' => $this->getCurrentProject(),
    			'currentListTasks' => $listTasksUpdate,
    			'taskUpdate' => $taskUpdate,
    			'users' => $this->getUsers(),
    			'listDocs' =>  $listDocs,
    	));
    	$view->setTemplate('application/task/view');
    	return $view;    	
    }
    
    public function progressionAction(){
    	$request = $this->getRequest();
    	$response = $this->getResponse();
    	$id_task = $this->getIdTaskFromUrl();
    	$this->setUser();

    	if ($request->isPost()) {
    		if ($this->getUsersTaskLinkerTable()->setProgression($id_task, $this->getUser()->__get('user_id'), $request->getPost('progress')))
    			$response->setContent(\Zend\Json\Json::encode(array('response' => true)));
    		else
    			$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	}
    	else
    		$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
    	
    	return $response;    	
    }
}