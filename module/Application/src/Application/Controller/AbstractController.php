<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Entity;
class AbstractController extends AbstractActionController
{	
	/**
	 * @var Doctrine\ORM\EntityManager
	 */
	protected $em;
	protected $user;
	protected $currentProject;
	protected $users;
	protected $listTasks;//$listTasks
	protected $jalon;
	protected $dataTimeLine;
	protected $docs;

	/*
	 * Entity Manager
	 */
	
	public function setEntityManager(EntityManager $em)
	{
		$this->em = $em;
				
	}
	
	public function getEm()
	{
		if (null === $this->em) {
			$this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		}
		return $this->em;
	}
	
	/*
	 * Setter & Getter Variables
	 */
	
	public function setVariables()
	{
		$this->setUser();
		$this->setCurrentProject();
		$this->setUsers();
		$this->setListTasks();
		$this->setJalon();
		$this->setDataTimeLine();
		$this->setDoc();
		return array(
				'em' => $this->getEm(),
				'currentProject' => $this->getCurrentProject(),				
				'user' => $this->getUser(),
				'users' => $this->getUsers(),
				'listTasks' => $this->getListTasks(),
				'jalons' => $this->getJalon(),
				'dataTimeLine' => $this->getDataTimeLine(),
				'docs' => $this->getDoc(),				
		);
	}
	
	public  function getVariables()
	{
		return array(
				'em' => $this->getEm(),
				'currentProject' => $this->getCurrentProject(),
				'user' => $this->getUser(),
				'users' => $this->getUsers(),
				'listTasks' => $this->getListTasks(),
				'jalons' => $this->getJalon(),
				'dataTimeLine' => $this->getDataTimeLine(),
				'docs' => $this->getDoc(),
		);
	}
	
	/*
	 * Function for USER
	 */
	
	public function getUsers()
	{
		return $this->users;
	}
	
	public function setUsers()
	{
		if ($this->currentProject != null)
			$this->users = $this->currentProject->users;			
	}
	
	public function setUser()
	{				
		if ($this->zfcUserAuthentication()->hasIdentity()) {			
			$this->user = $this->getEm()->find('Application\Entity\User', $this->zfcUserAuthentication()->getIdentity()->getId());
		}
		else 
			$this->user = null;		 
	}
	public function getUser()
	{
		return $this->user;
	}
	
	/*
	 * Function for CURRENTPROJECT
	 */
	public function getCurrentProject()
	{
		return $this->currentProject;
	}
	
	public function setCurrentProject()
	{		
		$id_project = $this->params()->fromRoute('id', null);
		
		if(!$id_project)
			$this->currentProject = null;
		else
		{
			$this->currentProject = $this->getEM()->find('Application\Entity\Project', $id_project);			
		}		
	}
	
	/*
	 * Function for UsersProjects in LinkerTable
	 */
	public function getUsersProjectsLinkerTable() {		
		$sm = $this->getServiceLocator();
		$usersProjectsLinkerTable = $sm->get('Application\Model\UsersProjectsLinkerTable');		
		return $usersProjectsLinkerTable;
	}
	
	
	
		
	/*
	 * Function for listTASK 
	 */
	
	public function setListTasks()
	{
		if ($this->currentProject)
			$this->listTasks = $this->getListTaskTable()->fetchAll($this->currentProject->id, $this->getEm());
		return $this->listTasks;
	}
	
	public function getListTasks()
	{
		return $this->listTasks;
	}
	
	public function getListTaskTable()
	{
		$sm = $this->getServiceLocator();
		$listTaskTable = $sm->get('Application\Model\ListTaskTable');
		return $listTaskTable;
	}
	
	public function getTaskTable()
	{
		$sm = $this->getServiceLocator();
		$taskTable = $sm->get('Application\Model\TaskTable');
		return $taskTable;
	}
	
	public function getUsersTaskLinkerTable()
	{
		$sm = $this->getServiceLocator();		
		return $sm->get('Application\Model\UsersTaskLinkerTable');
	}
	
	/*
	 * Function for JALON
	 */
	public function getJalonTable() {
		$sm = $this->getServiceLocator();
		$jalonTable = $sm->get('Application\Model\JalonTable');
		return $jalonTable;
	}
	
	public function setJalon()
	{
		if ($this->currentProject)
			$this->jalon = $this->getJalonTable()->fetchAll($this->currentProject->id, $this->getEm());		
	}
	public function getJalon()
	{
		return $this->jalon;
	}
	
	/*
	 * Function for recente activite
	 */
	public function getActiviteRecenteTable(){
		$sm = $this->getServiceLocator();
		$activiteRecenteTable = $sm->get('Application\Model\ActiviteRecenteTable');
		
		return $activiteRecenteTable;
	}
	
	/*
	 * Function for TimeLine
	 */
	public function getTimeLineTable() {		
		$sm = $this->getServiceLocator();
		$timelineTable = $sm->get('Application\Model\TimeLineTable');
		
		return $timelineTable;
	}
	
	public function setDataTimeLine()
	{
		if ($this->currentProject)
			$this->dataTimeLine = $this->getTimeLineTable()->fetchAllWithID($this->getCurrentProject()->__get('id'));
	}
	
	public function getDataTimeLine()
	{
		return $this->dataTimeLine;
	}
	
	/*
	 * Function for Doc
	*/
	public function getDocTable() {
		$sm = $this->getServiceLocator();
		$docTable = $sm->get('Application\Model\DocTable');
		return $docTable;
	}
	
	public function setDoc()
	{
		if ($this->currentProject)
			$this->docs = $this->getDocTable()->fetchAllByProject($this->currentProject->id);
	}
	public function getDoc()
	{
		return $this->docs;
	}
	
	/*
	 * Function for task doc linker
	*/
	public function getTaskDocLinkerTable() {
		$sm = $this->getServiceLocator();
		$docTable = $sm->get('Application\Model\TaskDocLinkerTable');
		return $docTable;
	}	
	
	public function getUserTable() {
		$sm = $this->getServiceLocator();
		$userTable = $sm->get('Application\Model\UserTable');
		return $userTable;
	}
}
