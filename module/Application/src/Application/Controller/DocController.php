<?php

namespace Application\Controller;
use Zend\View\Model\ViewModel;
use Zend\Validator\File\Size;
use Zend\Form\Element;
use Doctrine\DBAL\Types\Type;

/**
 *
 * @author lasmoum
 *        
 */
class DocController extends AbstractController {
	
	public function indexAction() {
		$view = new ViewModel();
        $view->setTemplate('application/doc/index');        
        $this->setCurrentProject();
        $this->setUsers();
        $this->setDoc();        	
        // recuperation des variables
		$view->setVariables(array(
        		'em' => $this->getEm(),
        		'currentProject' => $this->getCurrentProject(),        		
        		'users' => $this->getUsers(),
        		'docs' => $this->getDoc()
		));		
		return $view;
    }    
    
	public function downloadAction(){
		$id_doc = $this->getDocIdFromUrl();
		$this->setCurrentProject();
		$this->setUser(); // recupérer le user qui fait le download
		$doc = new \Application\Entity\Doc();
		$doc = $this->getDocTable()->getDoc($id_doc);
		header('Content-Type: ' . $doc->getExtension());
		header('Content-Disposition: attachment; filename="'. $doc->getName() .'"');
		$destination = './public/upload' . $this->getCurrentProject()->__get('id') . '/';
		readfile($destination . $doc->getName());		
		
		// disable the view ... and perhaps the layout
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);		
	}     
    public function addAction() {
    	$request = $this->getRequest();  
    	$this->setCurrentProject();
    	$this->setUsers();
    	$this->setUser();
    	$destination = '';
    	if ($request->isPost())
    	{    		
    		$nonFile = $request->getPost()->toArray();    		    		
    		$File    = $this->params()->fromFiles('fileupload');    		
    		$data = array_merge(
    				$nonFile, //POST
    				$this->getRequest()->getFiles()->toArray()
    		);
    		$size = new Size(array('min'=>2)); //minimum bytes filesize    		 
    		$adapter = new \Zend\File\Transfer\Adapter\Http();
    		//validator can be more than one...    		
    		$adapter->setValidators(array($size), $File['name']);    				    		
    		if (!$adapter->isValid()){    			  		
    			$dataError = $adapter->getMessages();
    			$error = array();
    			foreach($dataError as $key=>$row)
    			{
    				$error[] = $row;
    			}
    			$view = new ViewModel();
    			$view->setTemplate('application/doc/add');
    			// envoie les donnée k'on a besoin
    			$view->setVariables(array(
    					'em' => $this->getEm(),
    					'currentProject' => $this->getCurrentProject(),
    					'users' => $this->getUsers(),
    					'error' => $error
    			));
    			return $view;
    			
    		} else {    			
    			if (!is_dir('./public/upload/' . $this->getCurrentProject()->__get('id'))){
    				mkdir('./public/upload/' . $this->getCurrentProject()->__get('id'));
    			}    			
    			$destination .= './public/upload/' . $this->getCurrentProject()->__get('id'). '/';
    			$test = $destination . $File['name'];  	
    			
    			if (file_exists($test)){
    				$view = new ViewModel();
    				$view->setTemplate('application/doc/add');
    				// envoie les donnée k'on a besoin
    				$view->setVariables(array(
    						'em' => $this->getEm(),
    						'currentProject' => $this->getCurrentProject(),
    						'users' => $this->getUsers(),
    						'error' => 'Le fichier ' . $File['name'] . ' existe déja'
    				));
    				return $view;
    			}	
    			$adapter->setDestination($destination);    			
    			if (!$adapter->receive($File['name'])) {    				    				    				
	    			$view = new ViewModel();
	    			$view->setTemplate('application/doc/add');
	    			// envoie les donnée k'on a besoin
	    			$view->setVariables(array(
	    					'em' => $this->getEm(),
	    					'currentProject' => $this->getCurrentProject(),
	    					'users' => $this->getUsers(),
	    					'error' => 'Fichier no upload'
	    			));
    				return $view;
    			}
    		}    		    		
    		$newDoc = new \Application\Entity\Doc();
    		$newDoc->setName($File['name']);
    		
    		$newDoc->setDescription($request->getPost('description'));    		
    		$newDoc->setCreated(date("Y-m-d H:i:s")); 
    		$newDoc->setUser_created_id($this->getUser()->user_id);
    		$newDoc->setProject_id($this->getCurrentProject()->__get('id'));
    		$newDoc->setExtension($File['type']);
    		$doc_id = $this->getDocTable()->saveDoc($newDoc);
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'doc',
    				'description' => 'Nouveau document  cr&eacute;&eacute; <strong>' . $newDoc->__get('name') . '</strong>',
    		);
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    		$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/docs/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		
    		$this->redirect()->toRoute('doc',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {
    		
    		$view = new ViewModel();
    		$view->setTemplate('application/doc/add');
    		// envoie les donnée k'on a besoin
    		$view->setVariables(array(
    				'em' => $this->getEm(),
    				'currentProject' => $this->getCurrentProject(),
    				'users' => $this->getUsers()
    		));
    		return $view;
    	}
    }
    
    public function removeAction() {
    	$id_doc = $this->getDocIdFromUrl();
    	$doc = $this->getDocTable()->getDoc($id_doc);
    	$this->setCurrentProject();
    	$this->setUser();    	
       	// supprission du fichier
    	$sup = './public/upload/' . $this->getCurrentProject()->__get('id'). '/' . $doc->getName();
    	unlink($sup);
    	$this->getDocTable()->removeDoc($id_doc);
    	// Ajouter dans la table timeLine
    	$data = array(
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'authors' => 'Supression par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    			'activite' => 'doc',
    			'description' => 'Document  supprim&eacute; <strong>' . $doc->__get('name') . '</strong>',
    	);
    	$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    	$recentActivite = array(
    			'user_id' => $this->getUser()->user_id,
    			'project_id' => $this->getCurrentProject()->__get('id'),
    			'action' => 'app/docs/project/'.$this->getCurrentProject()->__get('id'),
    			'str' => $data['description'],
    	);
    	$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    	$this->redirect()->toRoute('doc',array('id' => $this->getCurrentProject()->__get('id')));
    }
    
    public function updateAction() {
    	$id_doc = $this->getDocIdFromUrl();
    	$this->setCurrentProject();
    	$this->setUsers();
    	$this->setUser();
    	$request = $this->getRequest();
    	$newDoc = new \Application\Entity\Doc();
    	$newDoc = $this->getDocTable()->getDoc($id_doc);
    	if ($request->isPost())
    	{    		    		
    		$name = $request->getPost('name');
    		$destination = './public/upload/' . $this->getCurrentProject()->__get('id'). '/';
    		if (file_exists($destination . $name)){
    			$view = new ViewModel();
    			$view->setTemplate('application/doc/upd');
    			// envoie les donnée k'on a besoin
    			$view->setVariables(array(
    					'em' => $this->getEm(),
    					'currentProject' => $this->getCurrentProject(),
    					'users' => $this->getUsers(),
    					'doc' => $newDoc,
    					'error' => 'Le fichier ' . $name . ' existe déja, renomé le fichier svp'
    			));
    			return $view;
    		}    		
    		rename($destination . $newDoc->getName(), $destination . $name);
    		$n = $newDoc->getName();
    		$newDoc->setName($name);
    		$newDoc->setDescription($request->getPost('description'));
    		$newDoc->setUpdated(date("Y-m-d H:i:s"));
    		    		    		
    		$this->getDocTable()->saveDoc($newDoc);
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Renom&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'doc',
    				'description' => 'Document  renom&eacute; <strong>' . $n . ' to ' .$newDoc->getName() . '</strong>',
    		);
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    		$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/docs/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->redirect()->toRoute('doc',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {    		    		  	
    		$view = new ViewModel();
    		$view->setTemplate('application/doc/upd');
    		// envoie les donnée k'on a besoin
    		$view->setVariables(array(
    				'em' => $this->getEm(),
    				'currentProject' => $this->getCurrentProject(),
    				'users' => $this->getUsers(),
    				'doc' => $newDoc,
    		));
    		return $view;
    	}
    }
    
    public function newversionAction() {
    	$id_doc = $this->getDocIdFromUrl();
    	$this->setCurrentProject();
    	$this->setUsers();
    	$this->setUser();
    	$request = $this->getRequest();
    	$newDoc = new \Application\Entity\Doc();
    	$newDoc = $this->getDocTable()->getDoc($id_doc);
    	if ($request->isPost())
    	{    		    		
    		// ajout du nouveau fichier
    		$nonFile = $request->getPost()->toArray();
    		$File    = $this->params()->fromFiles('fileupload');
    	
    		$data = array_merge(
    				$nonFile, //POST
    				$this->getRequest()->getFiles()->toArray()
    		);
    		$size = new Size(array('min'=>2)); //minimum bytes filesize
    		$adapter = new \Zend\File\Transfer\Adapter\Http();
    		//validator can be more than one...
    		$adapter->setValidators(array($size), $File['name']);
    		if (!$adapter->isValid()){
    			$dataError = $adapter->getMessages();
    			$error = array();
    			foreach($dataError as $key=>$row)
    			{
    				$error[] = $row;
    			}
    			$view = new ViewModel();
    			$view->setTemplate('application/doc/version');
    			// envoie les donnée k'on a besoin
    			$view->setVariables(array(
    					'em' => $this->getEm(),
    					'currentProject' => $this->getCurrentProject(),
    					'users' => $this->getUsers(),
    					'doc' => $newDoc,
    					'error' => $error
    			));
    			return $view;
    			 
    		} else {
    			if (!is_dir('./public/upload/' . $this->getCurrentProject()->__get('id'))){
    				mkdir('./public/upload/' . $this->getCurrentProject()->__get('id'));
    			}
    			$destination = './public/upload/' . $this->getCurrentProject()->__get('id'). '/';
    			$test = $destination . $File['name'];
    			 
    			if (file_exists($test)){
    				$view = new ViewModel();
    				$view->setTemplate('application/doc/version');
    				// envoie les donnée k'on a besoin
    				$view->setVariables(array(
    						'em' => $this->getEm(),
    						'currentProject' => $this->getCurrentProject(),
    						'users' => $this->getUsers(),
    						'doc' => $newDoc,
    						'error' => 'Le fichier ' . $File['name'] . ' existe déja'
    				));
    				return $view;
    			}
    			// supprission du fichier
    			$sup = './public/upload/' . $this->getCurrentProject()->__get('id'). '/' . $newDoc->getName();
    			$n = $newDoc->getName();
    			unlink($sup);
    			$adapter->setDestination($destination);
    			if (!$adapter->receive($File['name'])) {
    				$view = new ViewModel();
    				$view->setTemplate('application/doc/version');
    				// envoie les donnée k'on a besoin
    				$view->setVariables(array(
    						'em' => $this->getEm(),
    						'currentProject' => $this->getCurrentProject(),
    						'users' => $this->getUsers(),
    						'doc' => $newDoc,
    						'error' => 'Fichier no upload'
    				));
    				return $view;
    			}
    		}    		
    		$newDoc->setName($File['name']);
    		$newDoc->setDescription($request->getPost('description'));
    		$newDoc->setUpdated(date("Y-m-d H:i:s"));    		    	
    		$newDoc->setExtension($File['type']);
    		
    		$doc_id = $this->getDocTable()->saveDoc($newDoc);
    		 
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Changer par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'doc',
    				'description' => 'Nouvelle version du document <strong>'.$n.'</strong> to <strong>'.$newDoc->getName() . '</strong>',
    		);
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    		$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/docs/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		$this->redirect()->toRoute('doc',array('id' => $this->getCurrentProject()->__get('id')));
    	}
    	else {
    	
    		$view = new ViewModel();
    		$view->setTemplate('application/doc/version');
    		// envoie les donnée k'on a besoin
    		$view->setVariables(array(
    				'em' => $this->getEm(),
    				'currentProject' => $this->getCurrentProject(),
    				'users' => $this->getUsers(),
    				'doc' => $newDoc,
    		));
    		return $view;
    	}
    }
    
    public function getDocIdFromUrl(){
    	return $this->params()->fromRoute('id_doc', null);
    }    
}