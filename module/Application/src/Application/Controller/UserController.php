<?php

namespace Application\Controller;
use Zend\View\Model\ViewModel;
use Application\Entity\UsersProjectsLinker;
use Zend\Crypt\Password\Bcrypt;

class UserController extends AbstractController {
	
	public function indexAction()
	{		
		return new ViewModel($this->setVariables());		
	}
		
	public function getIdUser()
	{
		$id_user = $this->params()->fromRoute('id_user', null);
		if(!$id_user)
			return null;
		else
			return $this->getEM()->find('Application\Entity\User', $id_user);
	}
	
	public function getLoggerAction()
	{
		$request = $this->getRequest();
		$response = $this->getResponse();
		if ($request->isPost())
		{
			$newUser = new \Application\Entity\User;
			$newUser = $this->getEm()->find('Application\Entity\User' , $request->getPost('id'));
			
			$response->setContent(\Zend\Json\Json::encode(array('response' => true, 'logged' => $newUser->getLogged())));
				
		}
		else
			$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
	
		return $response;
	}
	// ajouter un utilisateur
	// le champ a remplir MAIL
	// verifier s'il existe le user
	// si oui :
	//		Afficher les infos public de user
	//		Oui ou non
	// 
	public function addAction()
	{		
		$this->setUser();
		$request = $this->getRequest();
		if ($request->isPost())
		{
                        $bcrypt = new Bcrypt();
			$newUser = new \Application\Entity\User;
			$newUser->__set('username', $request->getPost('username'));
			$newUser->__set('email', $request->getPost('email'));
			$newUser->__set('password', $bcrypt->create($request->getPost('password'))); 
			$newUser->__set('display_name', $request->getPost('display_name'));
			$newUser->__set('logged', 0);
			
			$this->getEm()->persist($newUser);
			$this->getEm()->flush();
			$this->setCurrentProject();
			$newUser->addProject($this->getCurrentProject());
			$this->getEm()->persist($newUser);
			$this->getEm()->flush();
			// Ajouter dans la table timeLine
			
			$data = array(
					'project_id' => $this->getCurrentProject()->__get('id'),
					'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->getDisplayName() . '</strong>',
					'description' =>'<strong>' . $newUser->__get('display_name') . '</strong> a rejoint le projet',
					'activite' => 'user',
			);
			$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
			
			$this->redirect()->toRoute('users',array('id' => $this->getCurrentProject()->__get('id')));
		}
		else
			return; // TODO return error
	}
				
	// suppression d'un utilisateur
	public function removeAction()
	{		
		$request = $this->getRequest();
		$response = $this->getResponse();
		if ($request->isPost())
		{
			$post_data = $request->getPost();
			$user_id = $post_data['id_user'];
			$project_id = $post_data["id"];
			$name = $this->getEm()->find('Application\Entity\User', $user_id);
			$pro = $this->getEm()->find('Application\Entity\Project', $project_id);
			$this->setUser();
			//$em->find('Application\Entity\User', $entity->getReponsable())
			if ($this->getUsersProjectsLinkerTable()->getUsersProjectsLinker($project_id, $user_id) != false)
			{
				if (!$this->getUsersProjectsLinkerTable()->removeUsersProjectsLinker($project_id, $user_id))
					$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
				else {					
					// Ajouter dans la table timeLine					
					$data = array(
							'project_id' => $pro->getId(),
							'authors' => 'Supprimé par <strong>' .  $this->getUser()->getDisplayName() . '</strong>',
							'description' =>'<strong>' . $name->getDisplayName() . '</strong> a quiter le projet',
							'activite' => 'user',
					);
					$this->getTimeLineTable()->addTimeLine($data, $pro->__get('id'));
					$response->setContent(\Zend\Json\Json::encode(array('response' => true)));
				}
			}
			else
				$response->setContent(\Zend\Json\Json::encode(array('response' => false)));
			return $response;
		}
	}
}

?>