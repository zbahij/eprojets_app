<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Entity;
class IndexController extends AbstractController
{
	public function mycalendarAction()
	{
		return new ViewModel($this->setVariables());
	}
	
	public function fillmycalendarAction()
	{
		$this->setUser();
		
		$sm = $this->getServiceLocator();
		
		$tasks = $sm->get('Application\Model\TaskTable')->fetchAllByUser($this->getUser()->user_id);
		$events = array();		
		foreach ($tasks as $t)
		{
			$event = array('id' => $t->getId(),
					'title' => $t->getName(),
					'start' => \Application\Utility\Helper::dateTimeToDate($t->getStartDate()),
					'end' => \Application\Utility\Helper::dateTimeToDate($t->getEndDate()),
					'url' => '',
			 );
			
			 $events[] = $event;
		}
		
		$result = new JsonModel($events);
		
		return $result;
	}
	public function indexAction()
	{		
		$this->setUser();
		$user = $this->getUser();		
		$user->setLogged(1);
		$this->getUserTable()->updLog($user);
		return new ViewModel(array(
				'user' => $this->getUser()
		));
	}
}
