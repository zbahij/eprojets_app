<?php
namespace Application\Controller;
use Zend\View\Model\ViewModel;

class PenseBetesController extends AbstractController {

    protected $_pensebetesTable;

    public function indexAction() {
    	$this->setUser();
		$view = new ViewModel();
        $view->setTemplate('pense-betes/index');
		$view->setVariables(array('pensebetes' => 
				$this->getpensebetesTable()->fetchAll($this->getUser()->user_id)));
		return $view;
    }

    public function addAction() {
    	$this->setUser();
    	$user_id = $this->getUser()->user_id;
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {            
			$new_note = new \Application\Entity\PenseBetes();  
			
			if (!$note_id = $this->getpensebetesTable()->savePenseBetes($new_note, $user_id))
                $response->setContent(\Zend\Json\Json::encode(array('response' => false)));
            else {
                $response->setContent(\Zend\Json\Json::encode(array('response' => true, 'new_note_id' => $note_id, 'user_id' => $user_id)));
            }
        }
        return $response;
    }

    public function removeAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $post_data = $request->getPost();
            $note_id = $post_data['id'];
            if (!$this->getpensebetesTable()->removePenseBetes($note_id))
                $response->setContent(\Zend\Json\Json::encode(array('response' => false)));
            else {
                $response->setContent(\Zend\Json\Json::encode(array('response' => true)));
            }
        }
        return $response;
    }

    public function updateAction() {
        // update post
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $post_data = $request->getPost();
            $note_id = $post_data['id'];
            $note_content = $post_data['content'];
            $pensebete = $this->getpensebetesTable()->getPenseBetes($note_id);
            $pensebete->setNote($note_content);
            if (!$this->getpensebetesTable()->savePenseBetes($pensebete))
                $response->setContent(\Zend\Json\Json::encode(array('response' => false)));
            else {
                $response->setContent(\Zend\Json\Json::encode(array('response' => true)));
            }
        }
        return $response;
    }

    public function getpensebetesTable() {
        if (!$this->_pensebetesTable) {
            $sm = $this->getServiceLocator();
            $this->_pensebetesTable = $sm->get('Application\Model\PenseBetesTable');
        }
        return $this->_pensebetesTable;
    }
}