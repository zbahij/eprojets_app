<?php

namespace Application\Controller;
use Zend\View\Model\ViewModel;

class JalonController extends AbstractController {
	
	protected $_jalonTable;	
   	
   	public function indexAction() {
		$view = new ViewModel();
        $view->setTemplate('application/jalon/index');
		$view->setVariables($this->setVariables());
		return $view;
    }

    public function addAction() {    	
    	$request = $this->getRequest();
    	if ($request->isPost())
    	{
    		$this->setUser();
    		$this->setCurrentProject();
    		$newJalon = new \Application\Entity\Jalon();    		
    		$newJalon->__set('name', $request->getPost('name'));
    		$newJalon->__set('description', $request->getPost('description'));
    		$newJalon->__set('date', $request->getPost('date'));
    		
    		$newJalon->__set('user_id', $this->getUser()->user_id);
    		$newJalon->__set('project_id', $this->getCurrentProject()->__get('id'));
    		$newJalon->__set('status', 0);
    		$newJalon->__set('responsable', $request->getPost('responsable'));
    		$jalon_id = $this->getJalonTable()->saveJalon($newJalon, $this->getEm());
    		 
    		// Ajouter dans la table timeLine  
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Cr&eacute;&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',    				
    				'activite' => 'jalon',
    		);
    		
    		if ($newJalon->getReponsable() == '-1')
    			$data['description'] = 'Nouveau jalon cr&eacute;&eacute; <strong>' . $newJalon->__get('name') . '</strong>
    				[Responsable: <strong>Aucun</strong>]';
    		else {    			
    			$reponsableUser = $this->getEm()->find('Application\Entity\User', $newJalon->getReponsable());
    			$data['description'] = 'Nouveau jalon cr&eacute;&eacute; <strong>' . $newJalon->__get('name') . '</strong>
    				[Responsable: <strong>' . $reponsableUser->__get('display_name') . '</strong>]';
    		}
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    		$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/jalon/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'], 
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		
    		$this->redirect()->toRoute('jalon',array('id' => $this->getCurrentProject()->__get('id'))); 		
    	}
    	else {
    		$view = new ViewModel();
    		$view->setTemplate('application/jalon/add');
    		$view->setVariables($this->setVariables());
    		return $view;
    	}
    }

    public function removeAction() {    	
    	$id_jalon = $this->getJalonIdFromUrl();    	
   		$jalon = $this->getJalonTable()->getJalon($id_jalon, $this->getEm());
   		$this->setCurrentProject();
   		$this->setUser();
   		// Ajouter dans la table timeLine
   		$data = array(
   				'project_id' => $this->getCurrentProject()->__get('id'),
   				'authors' => 'Supprim&eacute; par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
   				'description' => 'Jalon supprim&eacute; <strong>' . $jalon->__get('name') . '</strong>',
   				'activite' => 'jalon',
   		);
   		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
   		$recentActivite = array(
   				'user_id' => $this->getUser()->user_id,
   				'project_id' => $this->getCurrentProject()->__get('id'),
   				'action' => 'app/jalon/project/'.$this->getCurrentProject()->__get('id'),
   				'str' => 'Jalon supprim&eacute; <strong>' . $jalon->__get('name') . '</strong>',
   		);
   		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
   		$this->getJalonTable()->removeJalon($id_jalon);
    	$this->redirect()->toRoute('jalon',array('id' => $this->getCurrentProject()->__get('id')));
    }

    public function updateAction() {
    	$id_jalon = $this->getJalonIdFromUrl();
    	$this->setCurrentProject();
    	$request = $this->getRequest();
    	if ($request->isPost())
    	{   		
    		$this->setUser(); // pour modifier la valuer de celui qui modifier  @TODO
    		$newJalon = $this->getJalonTable()->getJalon($id_jalon, $this->getEm());    		
    		$newJalon->__set('name', $request->getPost('name'));
    		$newJalon->__set('description', $request->getPost('description'));
    		$newJalon->__set('date', $request->getPost('date'));
   		    		
    		//$newJalon->setDate(date("Y-m-d H:i:s")); // � enlever
    		$newJalon->__set('responsable', $request->getPost('responsable'));
    		if ($request->getPost('status') == 'on')
    			$newJalon->__set('status', 1);
    		else
    			$newJalon->__set('status', 0);

    		$this->getJalonTable()->saveJalon($newJalon, $this->getEm());
    		// Ajouter dans la table timeLine
    		$data = array(
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'authors' => 'Mise à jour par <strong>' .  $this->getUser()->__get('display_name') . '</strong>',
    				'activite' => 'jalon',
    		);
    		
    		if ($newJalon->getReponsable() == '-1')
    			$data['description'] = 'Jalon modifié <strong>' . $newJalon->__get('name') . '</strong>
    				[Responsable: <strong>Aucun</strong>]';
    		else {
    			$reponsableUser = $this->getEm()->find('Application\Entity\User', $newJalon->getReponsable());
    			$data['description'] = 'Jalon modifié <strong>' . $newJalon->__get('name') . '</strong>
    				[Responsable: <strong>' . $reponsableUser->__get('display_name') . '</strong>]';
    		}
    		$this->getTimeLineTable()->addTimeLine($data, $this->getCurrentProject()->__get('id'));
    		$recentActivite = array(
    				'user_id' => $this->getUser()->user_id,
    				'project_id' => $this->getCurrentProject()->__get('id'),
    				'action' => 'app/jalon/project/'.$this->getCurrentProject()->__get('id'),
    				'str' => $data['description'],
    		);
    		$this->getActiviteRecenteTable()->addActiveRecente($recentActivite);
    		
    		
    		$this->redirect()->toRoute('jalon',array('id' => $this->getCurrentProject()->__get('id')));   	    		
    	}
    	else {
    		$jalon = new \Application\Entity\Jalon();    		
    		$newJalon = new \Application\Entity\Jalon();
    		$newJalon = $this->getJalonTable()->getJalon($id_jalon, $this->getEm());
    		$ret = $this->setVariables();
    		$ret ['jalon'] = $newJalon;
    		$view = new ViewModel();
    		$view->setTemplate('application/jalon/upd');
    		$view->setVariables($ret);
    		return $view;
    	}   	 
    }
	
    public function getJalonIdFromUrl(){
    	return $this->params()->fromRoute('id_jalon', null);
    }	
}