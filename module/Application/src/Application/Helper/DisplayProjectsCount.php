<?php 
namespace Application\Helper;

 
class DisplayProjectsCount  extends AbstractMyHelper
{
    public function __invoke($user = null, $params= array() )
    {   
    	$result = 0;     	    	    	       	
    	if ($user)
    	{   
    		$result = count($user->projects); 		   				
    	}
    	
    	return $result;
    }
}