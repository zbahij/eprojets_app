<?php

/**
 * Zend_View_Helper
 * 
 * @author
 * @version 
 */
namespace Application\Helper;

use Application\Entity\ListTasks;
use Application\Entity\Task;
use Application\Entity\Jalon;
use Application\Entity\Docs;
/**
 * View Helper
 */
class DisplayDocs extends AbstractMyHelper {
	public function __invoke($var = null, $params= array() )
    {    
    	if ($var->docs)
    	{	    	    
    		$result = '';
    		
	        foreach ($var->docs as $lt)
        	{        	
        		$result .= '
        		<tr>
        			<td>
        				<a href="/app/docs/download/' . $lt->getId() . '/project/' . $var->currentProject->id . '">'
        				 	. $lt->getName() . '
        				</a>
        			</td>' .         		
        			'<td>
        				'. $lt->getExtension().'
        			</td>' . 
        			'<td>
	        			'. $lt->getCreated().'
	        		</td>' .
        			'<td>
        					'. $lt->getUpdated().'
        			</td>' .
        		'<td class="center ">
								<a class="btn btn-success" href="/app/docs/newversion/' . $lt->getId() . '/project/' . $var->currentProject->id . '">
									<i class="icon-zoom-in icon-white"></i>  
											Nouvelle version                                            
								</a>
								<a class="btn btn-info" href="/app/docs/update/' . $lt->getId() . '/project/' . $var->currentProject->id . '">
									<i class="icon-edit icon-white"></i>  
											Renommer                                            
								</a>
								<a class="btn btn-danger" href="/app/docs/remove/' . $lt->getId() . '/project/' . $var->currentProject->id . '">
									<i class="icon-trash icon-white"></i> 
									Supprimer
								</a>
							</td>
        				
        		
        		</tr>';
        		
        		
        		
        		
        	}
        	$result .= '';
            return $result;
    	}
    	else 
    		return 'Aucun document : <a href="/app/docs/add/project/' . $var->currentProject->getId() . ' ">Ajouter un document</a>';

    }
}
