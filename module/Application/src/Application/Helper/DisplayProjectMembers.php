<?php 
namespace Application\Helper;

 
class DisplayProjectMembers  extends AbstractMyHelper
{
    public function __invoke($project = null, $params= array() )
    {    
    	$result = '
    	<input type="hidden" id="currentProject" value="'.$project->id.'"/>
    	<table class="table">
    	<tbody>';
    	    	    	    	
    	if ($project)
    	{   	
    		$i = 1;
			foreach ($project->users as $u)
			{
				
				$result .= ' 
				<tr class="enLign-'.$i.'">
				<td id="enLign">' . $u->user->__get('display_name') . '</td>';
				
				if ($u->user->getLogged())
					$result .= '<td class="elemEnLign" id="'.$u->user->__get('user_id').'"><span class="label label-success">En ligne</span></td>';
				else
					$result .= '<td class="elemEnLign" id="'.$u->user->__get('user_id').'"><span class="label label-failed">Hors ligne</span></td>';
				
				
				$result .= '</tr>';
				
				$i = $i + 1;
			}	
    	}
    	$result .= '</tbody></table>
    	';
    	return $result;
    }
}