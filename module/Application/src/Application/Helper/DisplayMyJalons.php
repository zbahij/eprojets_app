<?php 
namespace Application\Helper;

 
use Application\Model\PenseBetesTable;
class DisplayMyJalons extends AbstractMyHelper
{
    public function __invoke($user = null, $params= array() )
    {  
    	
    	
    	$result = "";
    	if ($user)
    	{
    		$em = $this->sm->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    		$sm = $this->getServiceLocator();
    		if (isset($params['config']))
    			if ($params['config'] == 'count')    				
    				return count($sm->getServiceLocator()->get('Application\Model\JalonTable')->fetchAllActiveByUser($user->user_id, $em));
    			if ($params['config'] == 'active')
    			{
    				$result .= 
    				'
    				<table class="table bootstrap-datatable datatable dataTable">
						<thead>
							<tr>
								<th>Nom du jalon</th>
								<th>Date</th>
								<th>Projet</th>
								<th>Responsable</th>                                          
							  </tr>
						  </thead>   
						  <tbody>
    				';
    				$jalons = $sm->getServiceLocator()->get('Application\Model\JalonTable')->fetchAllActiveByUser($user->user_id, $em);
					
    				foreach ($jalons as $j)
    				{
    					//<!-- http://eprojets.dev/app/project/51/timeline-->
    					//'/app/project[/:id][/:action]
    					$result .=
    					'
    					<tr>
							<td><a href="/app/jalon/project/'. $j->getProject()->id .'">'.$j->getName().'</a></td>
							<td class="center">'.$j->getDate().'</td>
							
							<td class="center"><a href="/app/project/'. $j->getProject()->id .'/timeline">'.$j->getProject()->name.'</td></a>
							<td class="center">';    					
							if ($j->getReponsable() != -1)
							{
								$result .= '<a href="/app/users/project/' . $j->getProject()->id .'">';								
								$result .= $j->getResponsableUser()->display_name;
								$result .= '</a>';
							}
							
							else
								$result .= '-';
							$result .= '</td></tr>';                                        
						    					
    				}
    				$result .= 
    				'
    					</tbody>
					</table>
    				';
    				return $result;
    			}    		
    	}    	
    	return "Error";    	
    }
}