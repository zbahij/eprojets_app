<?php

namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\Url;

use Application\Entity\User;

class AbstractMyHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{
	protected 	$sm;
	protected 	$em;	
	private		$translator = null;
	
	public function translate($str = '')
	{
		if (!$this->translator)
		{
			$this->translator = $this->getServiceLocator()->get('translator');
		}
		return $this->translator->translate($str);
	}
	
	public function setEntityManager(EntityManager $em)
	{
		$this->em = $em;
	}
	
	public function getEm()
	{
		if (null === $this->em) {
			$this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		}
		return $this->em;
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
	{
		$this->sm = $serviceLocator;
		return $this;
	}
	
	public function getServiceLocator()
	{
		return $this->sm;
	}
}