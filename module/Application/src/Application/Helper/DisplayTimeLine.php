<?php

namespace Application\Helper;
use Application\Model\TimeLine;


class DisplayTimeLine  extends AbstractMyHelper
{
	private $jalon;
	private $tache;
	private $entities = array();
	
	private function verif($variable)
	{
		foreach ($variable as $data)
		{
			if ($data->__get('activite') == 'jalon')
				$this->jalon = true;
			if ($data->__get('activite') == 'tache')
				$this->tache = true;
		}	
	}
	
	private function myDate($date)
	{
		$today = date("Y-m-d");
		$dTab = explode(' ', $date);
		$DateHier=1;
		$DateAvantHier=2;
		$Date_hier = date('Y-m-d', strtotime($today.' -'.$DateHier.' days'));		
		$Date_avantHier = date('Y-m-d', strtotime($today.' -'.$DateAvantHier.' days'));
		
		if ($today == $dTab[0])
			return "Aujourd'hui";
		if ($Date_hier == $dTab[0])
			return "Hier";
		if ($Date_avantHier == $dTab[0])
			return "Avant hier";			
		return $date;		
	}
	
	private function dateEqual($dateFirst, $dateLast){				
		$tday = explode(' ', $dateFirst);
		$dTab = explode(' ', $dateLast);
		if ($tday[0] == $dTab[0])
			return true;
		
		return false;
	}
	
	
	public function __invoke($variable = null, $params= array() )
	{
		$result = '';
		$count = count($variable->dataTimeLine);
		$this->verif($variable->dataTimeLine);		
		$error;
		
		
		$result .= '<table class="table">';
		$result .= '<tbody>';
		for ($i = 0; $i < $count; $i++){						
			if ($i == 0)
			{
				
				$result .='<tr><td>&nbsp;&nbsp;<strong>' . $this->myDate($variable->dataTimeLine[$i]->__get('created')) . '</strong></td><td></td></tr>';//Aujourd\'hui
				$result .='<tr>														
							<td class=""><i class="icon-info-sign"></i> ' . $variable->dataTimeLine[$i]->__get('description') . '.</td>
							<td class="by">' . $variable->dataTimeLine[$i]->__get('authors') .'</td>
						  </tr>';
				if (($i+1) == $count)
					$result .= '<tr><td> </td><td> </td></tr>';
			}
			else
			{
				if ($this->dateEqual($variable->dataTimeLine[$i]->__get('created'),$variable->dataTimeLine[$i - 1]->__get('created')))
				{
					$result .='
							<tr>																
								<td class=""><i class="icon-info-sign"></i> ' . $variable->dataTimeLine[$i]->__get('description') . '</td>
								<td class="by">' . $variable->dataTimeLine[$i]->__get('authors') .'</td>
							</tr>';
					if (($i+1) == $count)
						$result .= '<tr><td> </td><td> </td></tr>';
				}
				else
				{															
					$result .='<tr><td>&nbsp;&nbsp;<strong>' . $this->myDate($variable->dataTimeLine[$i]->__get('created')) . '</strong></td><td></td></tr>';//Aujourd\'hui
					$result .='<tr>
								<td class="width_70">' . $variable->dataTimeLine[$i]->__get('description') . '.</td>
								<td class="by">' . $variable->dataTimeLine[$i]->__get('authors') .'</td>
							  </tr>';
					if (($i+1) == $count)
						$result .= '<tr><td> </td><td> </td></tr>';
				}
			}
		}	
		$result .= '</table>';
		return $result;
	}	
}