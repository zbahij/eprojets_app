<?php

namespace Application\Helper;

class DisplayUser extends AbstractMyHelper{
	public function __invoke($user_id = null, $params= array())
	{
		$em = $this->sm->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$u = $em->find('Application\Entity\User', $user_id);
		echo $u->getDisplayName();
	}
}