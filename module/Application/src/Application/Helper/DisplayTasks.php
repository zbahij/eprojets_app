<?php

/**
 * Zend_View_Helper
 * 
 * @author
 * @version 
 */
namespace Application\Helper;

use Application\Entity\ListTasks;
use Application\Entity\Task;
use Application\Entity\Jalon;


class DisplayTasks extends AbstractMyHelper {


	public function __invoke($var = null, $params= array() ){

   		$result = "";
    	if ($var->listTasks)
    	{    		  	    	    
    		$result = '';
	        foreach ($var->listTasks as $lt){
        		$result .= '
        		<div class="row-fluid sortable ui-sortable">
        		<div class="box span12">
					<div class="box-header well" data-original-title="">
						<h2><i class="icon-list"></i> '. $lt->getName() .'</h2>
						<div class="box-icon">
							<a class="btn btn-round" href="/app/tasks/add/' . $lt->getId() . '/project/' . $var->currentProject->id . '"><i class="icon-plus"></i></a>        		     		
        		     		<a title="Editer" class="btn btn-round" href="/app/tasks/updateList/' . $lt->getId() . '/project/' . $var->currentProject->id . '"><i class="icon-edit"></i></a>
   		     				<a class="btn btn-round" href="/app/tasks/removeList/' . $lt->getId() . '/project/' . $var->currentProject->id . '"><i class="icon-remove-sign"></i></a>					
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>					
						</div>
					</div>	
					<div class="box-content">';

        		$result .= 
    				'
    				<table class="table">
						<thead>
							<tr>								
								<th>Nom de la tâche</th>
    							<th>Début</th>
    							<th>Fin</th>
								<th>% Achevé</th>
								<th>Action</th>                                          
							  </tr>
						  </thead>   
						  <tbody>
    				';
														
        		
        		if ($lt->getTasks() != null){
        			foreach ($lt->getTasks() as $t)
        			{
        				$result .= '
							<tr>        						 						        					
        						<td width="30%">' . $t->getName() . '</td>
        						<td>' . $t->getStartDate() . '</td>
        						<td>'. $t->getEndDate().'</td>
        						<td width="20%">
        							<div class="progress progress-striped progress-success active">
										<div style="width: '. $t->getPourcentage().'%;" class="bar"></div>
									</div>
        						</td>
        						<td>
        							<a href="/app/tasks/view/' . $lt->getId() . '/' . $t->getId() . '/project/' . $var->currentProject->id . '" class="btn btn-success">
        								<i class="icon-zoom-in"></i>
        								Voir
        							</a>         							
        							<a href="/app/tasks/update/' . $lt->getId() . '/' . $t->getId() . '/project/' . $var->currentProject->id . '" class="btn btn-info">
										<i class="icon-edit icon-white"></i>  
										Editer                                            
									</a>
        							<a href="/app/tasks/remove/' . $lt->getId() . '/' . $t->getId() . '/project/' . $var->currentProject->id . '" class="btn btn-danger">
										<i class="icon-trash icon-white"></i> 
										Supprimer
									</a>        							        				
        						</td>
        					</tr>
        					';
        			}
        		} 
        		if ($lt->getJalon() != NULL)
        			$result .= '<p> Jalon : ' . $lt->getJalon()->getName() . '</p>';
        		$result .= '</tbody></table>';
        		$result .= '
        			</div>
        		</div>
        	</div>';
        	}        	        	            
    	}
    	else 
    	{
    		$result .= '
    				
    				<div class="row-fluid sortable ui-sortable">
        		<div class="box span12">
					<div class="box-header well" data-original-title="">
						<h2><i class="icon-list"></i></h2>						
					</div>	
					<div class="box-content">
						<h3>Aucune liste de tâches</h3>								
					</div>
								';    				
    	}
    	return $result;

    }
}
