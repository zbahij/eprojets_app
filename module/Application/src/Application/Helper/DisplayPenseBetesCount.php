<?php 
namespace Application\Helper;

 
use Application\Model\PenseBetesTable;
class DisplayPenseBetesCount  extends AbstractMyHelper
{
    public function __invoke($user = null, $params= array() )
    {   
    	if ($user)
    	{        		
    		$sm = $this->getServiceLocator();    	
    		return count($sm->getServiceLocator()->get('Application\Model\PenseBetesTable')->fetchAll($user->user_id));
    	}
    	return "Error";    	
    }
}