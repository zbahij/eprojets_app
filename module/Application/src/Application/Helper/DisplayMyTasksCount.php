<?php 
namespace Application\Helper;

 
use Application\Model\TaskTable;
class DisplayMyTasksCount  extends AbstractMyHelper
{
    public function __invoke($user = null, $params= array() )
    {   
    	if ($user)
    	{        		
    		$sm = $this->getServiceLocator();    	
    		return count($sm->getServiceLocator()->get('Application\Model\TaskTable')->fetchAllByUser($user->user_id));
    	}
    	return 0;    	
    }
}