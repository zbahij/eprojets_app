<?php 
namespace Application\Helper;

class DisplayMyProjects extends AbstractMyHelper
{	
    public function __invoke($user_id = null, $params= array() )
    {    
    	if ($user_id)
    	{   	
    		$em = $this->sm->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    		$u = $em->find('Application\Entity\User', $user_id);
    		$p = $u->__get('projects');

    		$result = ""; 
        	$before = '<li>';
        	$after = '</li>';
	        
    	     
        
	        foreach ($p as $proj)
        	{
        		$result .= $before;
	        	$id_project = $proj->project->id;        	
        		$href = "/app/project/$id_project/timeline";
        		$result .= '<a href="'.$href.'"><i class="icon-book"></i> '.$proj->project->name.'</a>';
        		$result .= $after;
        	}
                        
        	$result .= $before;
        	return $result;
    	}
    	else 
    		return "Error";

    }
}