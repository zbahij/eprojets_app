<?php 
namespace Application\Helper;

 
use Application\Model\PenseBetesTable;
class DisplayMyTasks extends AbstractMyHelper
{
    public function __invoke($user = null, $params= array() )
    {      	
    	$result = "";
    	if ($user)
    	{
    		$em = $this->sm->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    		$sm = $this->getServiceLocator();
    		if (isset($params['config']))
    			if ($params['config'] == 'count')    				
    				return count($sm->getServiceLocator()->get('Application\Model\TaskTable')->fetchAllActiveByUser($user->user_id, $em));
    			if ($params['config'] == 'active')
    			{
    				$result .= 
    				'
    				<table class="table bootstrap-datatable datatable dataTable">
						<thead>
							<tr>
								<th>Nom du jalon</th>
								<th>Date fin</th>
								<th>Projet</th>
								<th>Liste</th>                                          
							  </tr>
						  </thead>
						  <tbody>
    				';
    				$tasks = $sm->getServiceLocator()->get('Application\Model\TskTable')->fetchAllActiveByUser($user->user_id, $em);
    				
					if (!empty($tasks))
					{
	    				foreach ($tasks as $t)
	    				{
	    					$listTask = $sm->getServiceLocator()->get('Application\Model\TskTable')->getListTaskById($t->getListtasksId());    					
	    					$project = $sm->getServiceLocator()->get('Application\Model\TskTable')->getProject($listTask->getProjectId(), $em);
	    					//http://eprojets.dev/app/tasks/view/2/4/project/51
	    					$result .=
	    					'
	    					<tr>
								<td><a href="/app/tasks/view/'.$t->getListtasksId() .'/'. $t->getId().'/project/'. $project->id .'">'.$t->getName().'</a></td>
								<td class="center">'.$t->getEndDate().'</td>
								<td class="center"><a href="/app/project/'. $project->id .'/timeline">'.$project->name.'</td></a>
								<td class="center"><a href="/app/tasks/project/'. $project->id .'">'; 
									$result .= $listTask->getName();
								$result .= '</a></td></tr>';                                        
							    					
	    				}
					}
					else
					{
						$result .=
						'
	    					<tr>
								<td>Pas de tâches</td>
							<tr>';																	
					}
    				$result .= 
    				'
    					</tbody>
					</table>
    				';
    				return $result;
    			}    		
    	}    	
    	return "Error";    	
    }
}