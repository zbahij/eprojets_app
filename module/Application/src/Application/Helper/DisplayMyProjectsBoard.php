<?php 
namespace Application\Helper;

class DisplayMyProjectsBoard extends AbstractMyHelper
{	
    public function __invoke($user_id = null, $params= array() )
    {   
    	$result = ""; 
    	if ($user_id)
    	{   	
    		$em = $this->sm->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    		$u = $em->find('Application\Entity\User', $user_id);
    		$p = $u->__get('projects');

    		
    		foreach ($p as $proj)
    		{	
    			$result .= '
    			<div id="accordion">';
				$result .= '
					<div class="accordion-group">						
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion'.$proj->project->id.'" href="#collapse'.$proj->project->id.'">
								'.$proj->project->name.'
							</a>
						</div>									
						<div id="collapse'.$proj->project->id.'" class="accordion-body collapse in">
							<p style="padding-top:15px;">
							'.$proj->project->description.'
							</p>	
							<div class="accordion-inner">
								<div class="navbar navbar-static" id="navbar-example">
									<div class="navbar-inner">
        							<div style="width: auto;" class="container">            						
                						<a href="/app/project/'.$proj->project->id.'/timeline"><button class="btn" type="button"><i class="icon-road"></i> Timeline</button></a>
                						<a href="/app/project/'.$proj->project->id.'/calendar"><button class="btn" type="button"><i class="icon-calendar"></i> Calendrier</button></a>
                						<a href="/app/project/'.$proj->project->id.'>/tasks"><button class="btn" type="button"><i class="icon-list"></i> Tâches</button></a>                  
                						<a href="/app/project/'.$proj->project->id.'/jalons"><button class="btn" type="button"><i class="icon-time"></i> Jalons</button></a>
                						<button class="btn" type="button"><i class="icon-time"></i> Discussions</button>
                						<button class="btn" type="button"><i class="icon-book"></i> Documents</button>                 
                						<div class="nav pull-right">	            											              
									</div>
								</div>
							</div>							
						</div>
					</div>';
				$result .= '</div>';
    		}			
    		
    		return $result;         	
        	
    	}
    	else 
    		return "Error";

    }
}