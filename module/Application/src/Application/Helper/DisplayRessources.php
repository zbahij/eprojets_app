<?php

namespace Application\Helper;

class DisplayRessources  extends AbstractMyHelper{
	public function __invoke($var = null, $params= array() )
	{
		if ($var->users)
		{
			$result = '';
	
			foreach ($var->users as $lt)
			{
				$result .= '
        		<tr id="'. $lt->user->user_id . '">
        			<td>
        				'. $lt->user->display_name .'
        			</td>' .
        			'<td>
        				'. $lt->user->email .'
        			</td>' .
        			'<td class="center">
								<a class="delete-user btn btn-success" href="#" id="'.$lt->user->user_id .'|'. $var->currentProject->id . '">
									<i class="icon-zoom-in icon-white"></i>
											Delete
								</a>
					</td>
	
	
        		</tr>';
	
	
	
	
			}
			$result .= '';
			return $result;
		}
		else
			return "Error";
	
	}
}

