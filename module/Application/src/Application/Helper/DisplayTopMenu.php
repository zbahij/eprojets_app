<?php 
namespace Application\Helper;
 
class DisplayTopMenu extends AbstractMyHelper
{	
    public function __invoke($glob = null, $params= array() )
    {   
    	$uri = $_SERVER['REQUEST_URI'];    
    	$topmenu = '<ul class="nav">';
    	$topmenu .= '<li ';    	    	    							
		if ( $uri == '/') $topmenu .=  'class="active"';
		
		if (strpos($uri, 'app')){
			$topmenu .= '><a href="'. $glob->url('app') . '">Accueil</a></li>';
		}
		else
			$topmenu .= '><a href="'. $glob->url('site') . '">Accueil</a></li>';
			
		
//		$topmenu .= '><a href="'. $glob->url('site') . '">Accueil</a></li>';
    							                                		
    	 	    	    	
    	
    	$topmenu .= '
    		<li><a href="./about">A propos</a></li>
    		<li><a href="./contact">Contact</a></li>
    	';
    	//if ($glob->zfcUserIdentity())
    		//$topmenu .= '<li><a href="/user/logout">Se déconnecter</a></li>';
    	$topmenu .= '</ul>';
    	return $topmenu;
    }
}