<?php

namespace Application\Helper;

use Application\Model\ActiviteRecenteTable;

/**
 *
 * @author lasmoum
 *        
 */
class DisplayActiviteRecente extends AbstractMyHelper {
	public function __invoke($user = null, $params = array()) {
		$result = "";
		if ($user) {
			$em = $this->sm->getServiceLocator ()->get ( 'Doctrine\ORM\EntityManager' );
			$sm = $this->getServiceLocator ();			
			if ($params ['config'] == 'active') {				
				$activiteRecente = $sm->getServiceLocator()->get('Application\Model\ActiviteRecenteTable')->fetchAllWithIdUser($user->user_id, $em);
				
				$result .= '<table class="table bootstrap-datatable datatable dataTable">';
				if (!empty($activiteRecente)) {
					$result .= '
	    				<thead>
		    				<tr>
			    				<th>Description de l\'activité</th>
			    				<th>Date</th>
								<th>Projet</th>			    				
			    				<th>Action</th>
		    				</tr>
	    				</thead>
	    				<tbody>
    				';
					foreach ($activiteRecente as $t) {
												
						$result .= '
	    					<tr>
	    						<td class="center">'.$t->getStr().'</td>
	    						<td class="center">'.$t->getDate().'</td>
	    						<td class="center">'.$em->find('Application\Entity\Project', $t->getProject_id())->name.'</td>	    						
	    						<td class="center"><a class="btn btn-success" href="'.$t->getAction().'"><i class="icon-zoom-in icon-white"></i>Voir</a></td>
	    					</tr>';
					}
					$result .= '
    					</tbody>
					</table>
    				';
				} else {
					$result .= '
	    				<thead>
		    				<tr>
			    				<th>Aucune activité récente</th>			    				
		    				</tr>
	    				</thead>
	    				<tbody>';
					$result .= '
    					</tbody>
					</table>
    				';
				}
				
				return $result;
			}
		}
		return "Error";
	}
}