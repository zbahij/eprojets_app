<?php

namespace Application;
return array(
	// Ces aides de vues permettent de cr�er des widgets 
	'view_helpers' => array(
		'invokables' => array(
			'displayMyProjects' => 'Application\Helper\DisplayMyProjects',
			'displayMyProjectsBoard' => 'Application\Helper\DisplayMyProjectsBoard',				
			'displayTopMenu' => 'Application\Helper\DisplayTopMenu',
			'displayProjectMembers' => 'Application\Helper\DisplayProjectMembers',
			'displayProjectsCount' => 'Application\Helper\DisplayProjectsCount',
			'displayTimeLine' => 'Application\Helper\DisplayTimeLine',
			'displayPenseBetesCount' => 'Application\Helper\DisplayPenseBetesCount',
			'displayMyTasksCount' => 'Application\Helper\DisplayMyTasksCount',
			'displayTasks' => 'Application\Helper\DisplayTasks',
			'displayDocs' => 'Application\Helper\DisplayDocs',
			'displayMyJalons' => 'Application\Helper\DisplayMyJalons',
			'displayMyTasks' => 'Application\Helper\DisplayMyTasks',
			'displayActiviteRecente' => 'Application\Helper\DisplayActiviteRecente',
			'displayUser' => 'Application\Helper\DisplayUser',
			'displayRessources' => 'Application\Helper\DisplayRessources'
		)
	),
	
    'router' => array(
        'routes' => array(
            'site' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',                	
                    'defaults' => array(                    	
                        'controller' => 'Application\Controller\Site',
                        'action'     => 'index',
                    ),
                ),
            ),
        	'site_about' => array(
        			'type' => 'Zend\Mvc\Router\Http\Literal',
        			'options' => array(
        					'route'    => '/about',
        					'defaults' => array(
        							'controller' => 'Application\Controller\Site',
        							'action'     => 'about',
        					),
        			),
        	),
        	'site_contact' => array(
        		'type' => 'Zend\Mvc\Router\Http\Literal',
        			'options' => array(
        				'route'    => '/contact',
        				'defaults' => array(
        					'controller' => 'Application\Controller\Site',
        					'action'     => 'contact',
        				),
       				),
       		),
		
            'pensebetes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pensebetes[/:action][/:id]',
					'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\PenseBetes',
                        'action' => 'index',
                    ),
                ),
            ),
        	'stat' => array(
        				'type' => 'segment',
        				'options' => array(
        						'route' => '/app/statistique[/:action][/:id]',
        						'constraints' => array(
        							'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        							'id' => '[0-9]*',
        						),
        						'defaults' => array(
        								'controller' => 'Application\Controller\Statistique',
        								'action' => 'index',
        						),
        				),
        		),        		
        		'calendar' => array(
        				'type' => 'segment',
        				'options' => array(
        						'route' => '/app/calendar[/:action][/:id]',
        						'constraints' => array(
        								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        								'id' => '[0-9]*',
        						),
        						'defaults' => array(
        								'controller' => 'Application\Controller\Calendar',
        								'action' => 'index',
        						),
        				),
        		),
        		'test' => array(
        				'type' => 'segment',
        				'options' => array(
        						'route' => '/test[/:action][/:id]',
        						'constraints' => array(
        								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        								'id' => '[0-9]+',
        						),
        						'defaults' => array(
        								'controller' => 'Application\Controller\Index',
        								'action' => 'index',
        						),
        				),
        		),

        	'users' => array(
        			'type' => 'Segment',
        			'options' => array(
        					'route' => '/app/users[/:action][/:id_user]/project[/:id]',
        					'constraints' => array(
        							'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        							'id' => '[0-9]*',
        							'id_user' => '[0-9]*'
        					),
        					'defaults' => array(
        							'controller' => 'Application\Controller\UserController',
        							'action' => 'index',
        					),
        			),
        	),
        	'profil' => array(
        			'type' => 'Segment',
        			'options' => array(
        					'route' => '/app/profil[/:action][/:id_user]',
        					'constraints' => array(
        							'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        							'id_user' => '[0-9]*'
        					),
        					'defaults' => array(
        							'controller' => 'Application\Controller\Profil',
        							'action' => 'index',
        					),
        			),
        	),
        	'task' => array(
        			'type' => 'Segment',
        			'options' => array(
        					'route' => '/app/tasks[/:action][/:id_listTask][/:id_task]/project[/:id]',
        					'constraints' => array(
        							'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        							'id' => '[0-9]*',
        							'id_task' => '[0-9]*',
        							'id_listTask' => '[0-9]*'
        					),
        					'defaults' => array(
        							'controller' => 'Application\Controller\Task',
        							'action' => 'index',
        					),
        			),
        	),
        		
        		'doc' => array(
        				'type' => 'Segment',
        				'options' => array(
        						'route' => '/app/docs[/:action][/:id_doc]/project[/:id]',
        						'constraints' => array(
        								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        								'id' => '[0-9]*',
        								'id_doc' => '[0-9]*'
        						),
        						'defaults' => array(
        								'controller' => 'Application\Controller\Doc',
        								'action' => 'index',
        						),
        				),
        		),
        	'jalon' => array(
        		'type' => 'Segment',
        		'options' => array(
        			'route' => '/app/jalon[/:action][/:id_jalon]/project[/:id]',
        			'constraints' => array(
        				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        				'id' => '[0-9]*',
        				'id_jalon' => '[0-9]*'
        			),
	        		'defaults' => array(
	        			'controller' => 'Application\Controller\Jalon',
	        			'action' => 'index',
	        		),
        		),
        	),
        	'project' => array(
        		'type' => 'Segment',
        		'options' => array(
        			'route' => '/app/project[/:id][/:action]',
        			'constraints' => array(
        				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        				'id' => '[0-9]+',
        			),
        			'defaults' => array(
        				'controller' => 'Application\Controller\Project',
        				'action' => 'index',
        			),
        		),
        	),
        	
            'app' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/app',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),        		
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            	'text_domain' => __NAMESPACE__
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
        	'Application\Controller\Site' => 'Application\Controller\SiteController',
        	'Application\Controller\Project' => 'Application\Controller\ProjectController',
        	'Application\Controller\Task' => 'Application\Controller\TaskController',
        	'Application\Controller\Doc' => 'Application\Controller\DocController',
        	'Application\Controller\Jalon' => 'Application\Controller\JalonController',        		
			'Application\Controller\PenseBetes' => 'Application\Controller\PenseBetesController',
        	'Application\Controller\UserController' => 'Application\Controller\UserController',
        	'Application\Controller\Statistique' => 'Application\Controller\StatistiqueController', 
        	'Application\Controller\Profil' => 'Application\Controller\ProfilController',
        	'Application\Controller\Calendar' => 'Application\Controller\CalendarController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
          //  'application/index/index' => __DIR__ . '/../view/application/project/index.phtml',
        	'application/index/user' => __DIR__ . '/../view/application/user/index.phtml',
			'pensebetes' 			  => __DIR__ . '/../view/pensebetes/index.phtml',
        	'application/site/index' => __DIR__ . '/../view/application/site/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
    		'driver' => array(
    				__NAMESPACE__ . '_driver' => array(
    						'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
    						'cache' => 'array',
    						'paths' => array(
    							__DIR__ . '\\..\\src\\Application\\Entity' 
    						)
    				),
    				'orm_default' => array(
    						'drivers' => array(
    								__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
    						)
    				)
    		)
   	),
	'session' => array(
		'remember_me_seconds'  => 1200,
		'use_cookies'          => true,
		'cookie_httponly'      => true,
		'cookie_domain'        => 'eprojets.dev',
	),
);
