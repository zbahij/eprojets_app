<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Application\View\Helper\DisplayMyProjects;


class Module
{	
    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        //$translator->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))->setFallbackLocale('fr_FR');                       
    }

    public function getConfig()
    {    	
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {    	
        return array(
			
            'Zend\Loader\StandardAutoloader' => array(            		
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,                	
                ),
            ),
        );
    }     
	
	public function getServiceConfig() {
        return array(
            'factories' => array(
                'Application\Model\PenseBetesTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new Model\PenseBetesTable($dbAdapter);
                    return $table;
                },
                'Application\Model\TimeLineTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\TimelineTable($dbAdapter);
                	return $table;
                },
                'Application\Model\ActiviteRecenteTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\ActiviteRecenteTable($dbAdapter);
                	return $table;
                },
                'Application\Model\UsersProjectsLinkerTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\UsersProjectsLinkerTable($dbAdapter);
                	return $table;
                	
                },
                'Application\Model\UsersTaskLinkerTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\UsersTaskLinkerTable($dbAdapter);
                	return $table;
                
                },
                'Application\Model\TaskTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$usersTaskLinkerTable = $sm->get('Application\Model\UsersTaskLinkerTable');                	
                	$table = new Model\TaskTable($dbAdapter, $usersTaskLinkerTable);
                	return $table;
                	 
                },
                'Application\Model\TskTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$usersTaskLinkerTable = $sm->get('Application\Model\UsersTaskLinkerTable');
                	$listTasksTable = $sm->get('Application\Model\ListTaskTable');
                	$table = new Model\TskTable($dbAdapter, $usersTaskLinkerTable, $listTasksTable);
                	return $table;                
                },
                'Application\Model\JalonTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\JalonTable($dbAdapter);
                	return $table;
                
                },
                'Application\Model\UserTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\UserTable($dbAdapter);
                	return $table;                
                },
                'Application\Model\DocTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\DocTable($dbAdapter);
                	return $table;
                },
                'Application\Model\TaskDocLinkerTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$table = new Model\TaskDocLinkerTable($dbAdapter);
                	return $table;
                },
                'Application\Model\ListTaskTable' => function($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$taskTable = $sm->get('Application\Model\TaskTable');
                	$jalonTable = $sm->get('Application\Model\JalonTable');
                	$table = new Model\ListTasksTable($dbAdapter, $taskTable, $jalonTable);
                	return $table;
                
                },                               
            ),
        );
    }
    
    public function initializeSession($em)
    {
    	$config = $em->getApplication()->getServiceManager()->get('Config');
    
    	$sessionConfig = new SessionConfig();
    	$sessionConfig->setOptions($config['session']);
    
    	$sessionManager = new SessionManager($sessionConfig);
    	$sessionManager->start();
    
    	Container::setDefaultManager($sessionManager); //au cas ou on utilise plusieurs SessionManagers
    }
}
